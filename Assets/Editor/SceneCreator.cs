using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SceneCreator : EditorWindow
{
    public Object sourceObj;
    List<string> usedMeshNames = new List<string>();

    [MenuItem("Window/Scene Creator")]
    public static void ShowWindow()
    {
        GetWindow(typeof(SceneCreator));
    }

    void OnGUI()
    {
        GUILayout.Label("Source FBX:");
        EditorGUILayout.BeginHorizontal();
        sourceObj = EditorGUILayout.ObjectField(sourceObj, typeof(GameObject), true); // true -> allowSceneObjects - why?
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Make/Update Prefabs"))
        {
            AssetDatabase.Refresh();
            CreatePrefabs();
        }
        if (GUILayout.Button("Make/Update Prefabs and Instantiate FBX hierarchy"))
        {
            AssetDatabase.Refresh();
            CreatePrefabs();
            InstantiateFBXHierarchyAsPrefabs();
        }
    }

    void CreatePrefabs()
    {
        usedMeshNames.Clear();
        var instanceRoot = (GameObject)PrefabUtility.InstantiatePrefab(sourceObj); // instantiate fbx
        PrefabUtility.UnpackPrefabInstance(instanceRoot, PrefabUnpackMode.OutermostRoot, InteractionMode.AutomatedAction); // make normal GO
        int childCount = instanceRoot.transform.childCount;
        List<Transform> children = new List<Transform>(childCount); // use child count as first approximation of necessary capacity
        GetAllChildren(instanceRoot.transform, ref children);
        string assetPath = $"Assets/_Prefabs/{instanceRoot.name}/"; // the instanceRoot.name should allow for identical names in different folders
        if (!Directory.Exists(assetPath))
        {
            Directory.CreateDirectory(assetPath);
            Debug.Log($"Created Directory: {assetPath}");
        }
        for (int i = 0; i < children.Count; i++)
        {
            var child = children[i];
            string meshName = child.GetComponent<MeshFilter>().sharedMesh.name; // get mesh name to find out which objects are identical meshes
            //Debug.Log(meshName);
            if (meshName.Contains("BoxCollider") || meshName.Contains("SphereCollider") || meshName.Contains("CapsuleCollider") || meshName.Contains("MeshCollider"))
            {
                child.GetComponent<Renderer>().enabled = false;
            }
            var savedPrefab = (GameObject)AssetDatabase.LoadAssetAtPath($"{assetPath}{meshName}.prefab", typeof(GameObject)); // try to load existing prefab with same name
            // prefab doesn't exist and not already in list... is this correct?
            // it does not update existing prefabs I think
            // so it updates the mesh obviously because of the new data in the fbx
            //Debug.Log($"Saved Prefab is null: {savedPrefab == null}");
            //if (savedPrefab == null && !usedMeshNames.Contains(meshName))
            if (!usedMeshNames.Contains(meshName))
            {
                usedMeshNames.Add(meshName); // update list
                //var childPrefab = PrefabUtility.SaveAsPrefabAssetAndConnect(child.gameObject, $"{assetPath}{meshName}.prefab", InteractionMode.UserAction); // make prefab and connect
                // we apparently need to get rid of the children to avoid nested prefabs...
                Debug.Log($"meshName: {meshName}, childCount: {child.childCount}");
                if (child.childCount > 0)
                {
                    for (int c = child.childCount - 1; c >= 0; c--)
                    {
                        Debug.Log($"current child index: {c}");
                        child.GetChild(c).parent = null;
                    }
                }
                var childPrefab = PrefabUtility.SaveAsPrefabAsset(child.gameObject, $"{assetPath}{meshName}.prefab"); // make prefab
                // reset prefab transform data to identity
                childPrefab.transform.localPosition = Vector3.zero;
                childPrefab.transform.localRotation = Quaternion.identity;
                childPrefab.transform.localScale = Vector3.one;
                childPrefab = null; // may not be necessary?
            }
        }
        usedMeshNames.Clear(); // may not be necessary?
        for (int i = children.Count - 1; i >= 0; i--)
        {
            GameObject.DestroyImmediate(children[i].gameObject);
        }
        children.Clear();
        GameObject.DestroyImmediate(instanceRoot);
    }

    void InstantiateFBXHierarchyAsPrefabs()
    {
        var oldRoot = GameObject.Find(sourceObj.name);
        if (oldRoot != null) GameObject.DestroyImmediate(oldRoot);

        var instanceRoot = (GameObject)PrefabUtility.InstantiatePrefab(sourceObj); // instantiate fbx
        PrefabUtility.UnpackPrefabInstance(instanceRoot, PrefabUnpackMode.OutermostRoot, InteractionMode.AutomatedAction); // make normal GO

        //var sceneRoot = new GameObject(sourceObj.name);
        string assetPath = $"Assets/_Prefabs/{instanceRoot.name}/";
        int childCount = instanceRoot.transform.childCount;
        List<Transform> children = new List<Transform>(childCount);
        GetAllChildren(instanceRoot.transform, ref children);
        for (int i = 0; i < children.Count; i++)
        {
            var child = children[i];
            string meshName = child.GetComponent<MeshFilter>().sharedMesh.name;
            var loadedChild = (GameObject)AssetDatabase.LoadAssetAtPath($"{assetPath}{meshName}.prefab", typeof(GameObject));
            var instantiatedChild = (GameObject)PrefabUtility.InstantiatePrefab(loadedChild);
            instantiatedChild.transform.parent = child.parent;
            instantiatedChild.transform.localPosition = child.localPosition;
            instantiatedChild.transform.localRotation = child.localRotation;
            instantiatedChild.transform.localScale = child.localScale;
            for (int j = 0; j < child.childCount; j++)
            {
                child.GetChild(j).parent = instantiatedChild.transform;
            }
        }
        // Delete the child's
        for (int i = children.Count - 1; i >= 0; i--)
        {
            GameObject.DestroyImmediate(children[i].gameObject);
        }
        children.Clear();
    }

    // --------------------Helpers-----------------------

    void GetAllChildren(Transform parent, ref List<Transform> children)
    {
        foreach (Transform child in parent)
        {
            children.Add(child);

            // recursive:
            GetAllChildren(child, ref children);
        }
    }
}
