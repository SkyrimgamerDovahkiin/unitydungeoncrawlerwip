using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FireGolem))]
public class FieldOfViewEditor : Editor
{
    void OnSceneGUI()
    {
        //Enemy enemy = (Enemy)target;
        FireGolem enemy = (FireGolem)target;
        //Debug.Log(enemy + "enemy!");

        //view distance
        Handles.color = Color.white;
        Handles.DrawWireArc(enemy.transform.position, Vector3.up, Vector3.forward, 360, enemy.viewDistance);

        //define angle
        Vector3 viewAngleA = enemy.DirFromAngle(-enemy.viewAngle / 2, false);
        Vector3 viewAngleB = enemy.DirFromAngle(enemy.viewAngle / 2, false);
        
        // draw angle lines
        Handles.DrawLine(enemy.transform.position, enemy.transform.position + viewAngleA * enemy.viewDistance);
        Handles.DrawLine(enemy.transform.position, enemy.transform.position + viewAngleB * enemy.viewDistance);

        Handles.color = Color.yellow;
        //Handles.DrawLine(enemy.transform.position, (enemy.transform.position - enemy.playerTarget.position).normalized);


        //line to player and aware distance
        Handles.color = Color.red;
        Handles.DrawWireArc(enemy.transform.position, Vector3.up, Vector3.forward, 360, enemy.awareDistance);
        Handles.DrawLine(enemy.transform.position, enemy.transform.position + viewAngleA * enemy.awareDistance);
        Handles.DrawLine(enemy.transform.position, enemy.transform.position + viewAngleB * enemy.awareDistance);
       // Handles.DrawLine(enemy.transform.position, enemy.playerTarget.position + new Vector3(0f, enemy.transform.position.y - enemy.playerTarget.position.y, 0f));
    }
}
