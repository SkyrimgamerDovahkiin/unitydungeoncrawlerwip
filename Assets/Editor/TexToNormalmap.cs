using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TexToNormalmap : AssetPostprocessor
{

// Automatically convert any texture file with "_bumpmap"
// in its file name into a normal map.
    void OnPreprocessTexture()
    {
        
        if (assetPath.Contains("_Normal"))
        {
            TextureImporter textureImporter  = (TextureImporter)assetImporter;
            textureImporter.convertToNormalmap = true;
        }
    }
}
