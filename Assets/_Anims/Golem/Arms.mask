%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Arms
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/body
    m_Weight: 0
  - m_Path: Armature/body/armup.L
    m_Weight: 1
  - m_Path: Armature/body/armup.L/armdown.L
    m_Weight: 1
  - m_Path: Armature/body/armup.L/armdown.L/hand.L
    m_Weight: 1
  - m_Path: Armature/body/armup.L/armdown.L/hand.L/hand.L_end
    m_Weight: 1
  - m_Path: Armature/body/armup.R
    m_Weight: 1
  - m_Path: Armature/body/armup.R/armdown.R
    m_Weight: 1
  - m_Path: Armature/body/armup.R/armdown.R/hand.R
    m_Weight: 1
  - m_Path: Armature/body/armup.R/armdown.R/hand.R/hand.R_end
    m_Weight: 1
  - m_Path: Armature/body/Body_low
    m_Weight: 0
  - m_Path: Armature/body/head
    m_Weight: 0
  - m_Path: Armature/body/head/head_end
    m_Weight: 0
  - m_Path: Armature/body/head/Head_low
    m_Weight: 0
  - m_Path: Armature/body/head/Sphere_low
    m_Weight: 0
  - m_Path: Armature/body/legup.L
    m_Weight: 0
  - m_Path: Armature/body/legup.L/foot.L
    m_Weight: 0
  - m_Path: Armature/body/legup.L/foot.L/foot.L_end
    m_Weight: 0
  - m_Path: Armature/body/legup.R
    m_Weight: 0
  - m_Path: Armature/body/legup.R/foot.R
    m_Weight: 0
  - m_Path: Armature/body/legup.R/foot.R/foot.R_end
    m_Weight: 0
  - m_Path: Armature/elbowIK.L
    m_Weight: 1
  - m_Path: Armature/elbowIK.L/elbowIK.L_end
    m_Weight: 1
  - m_Path: Armature/elbowIK.R
    m_Weight: 1
  - m_Path: Armature/elbowIK.R/elbowIK.R_end
    m_Weight: 1
  - m_Path: Armature/handIK.L
    m_Weight: 1
  - m_Path: Armature/handIK.L/handIK.L_end
    m_Weight: 1
  - m_Path: Armature/handIK.R
    m_Weight: 1
  - m_Path: Armature/handIK.R/handIK.R_end
    m_Weight: 1
  - m_Path: Armature/heelIK.L
    m_Weight: 0
  - m_Path: Armature/heelIK.L/heelIK.L_end
    m_Weight: 0
  - m_Path: Armature/heelIK.R
    m_Weight: 0
  - m_Path: Armature/heelIK.R/heelIK.R_end
    m_Weight: 0
  - m_Path: Armature/kneeIK.L
    m_Weight: 0
  - m_Path: Armature/kneeIK.L/kneeIK.L_end
    m_Weight: 0
  - m_Path: Armature/kneeIK.R
    m_Weight: 0
  - m_Path: Armature/kneeIK.R/kneeIK.R_end
    m_Weight: 0
  - m_Path: Arms_low
    m_Weight: 0
  - m_Path: Feet_low
    m_Weight: 0
  - m_Path: Hands_low
    m_Weight: 0
  - m_Path: Legs_low
    m_Weight: 0
  - m_Path: Shoulders_low
    m_Weight: 0
