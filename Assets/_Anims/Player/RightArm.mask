%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: RightArm
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Bone
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Neck
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger1.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger1.L/Finger2.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger1.L/Finger2.L/Finger2.L_end
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger3.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger3.L/Finger4.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger3.L/Finger4.L/Finger5.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger3.L/Finger4.L/Finger5.L/Finger5.L_end
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger6.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger6.L/Finger7.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger6.L/Finger7.L/Finger8.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger6.L/Finger7.L/Finger8.L/Finger8.L_end
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger9.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger9.L/Finger10.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger9.L/Finger10.L/Finger11.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger9.L/Finger10.L/Finger11.L/Finger11.L_end
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger12.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger12.L/Finger13.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger12.L/Finger13.L/Finger14.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.L/UpperArm.L/Forearm.L/Hand.L/Finger12.L/Finger13.L/Finger14.L/Finger14.L_end
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger1.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger1.R/Finger2.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger1.R/Finger2.R/Finger2.R_end
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger3.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger3.R/Finger4.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger3.R/Finger4.R/Finger5.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger3.R/Finger4.R/Finger5.R/Finger5.R_end
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger6.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger6.R/Finger7.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger6.R/Finger7.R/Finger8.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger6.R/Finger7.R/Finger8.R/Finger8.R_end
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger9.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger9.R/Finger10.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger9.R/Finger10.R/Finger11.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger9.R/Finger10.R/Finger11.R/Finger11.R_end
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger12.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger12.R/Finger13.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger12.R/Finger13.R/Finger14.R
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Spine1/Spine2/Shoulder.R/UpperArm.R/Forearm.R/Hand.R/Finger12.R/Finger13.R/Finger14.R/Finger14.R_end
    m_Weight: 1
  - m_Path: Armature/Bone/Spine.Control/Spine/Thigh.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Thigh.L/Calf.L
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Thigh.L/Calf.L/Calf.L_end
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Thigh.R
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Thigh.R/Calf.R
    m_Weight: 0
  - m_Path: Armature/Bone/Spine.Control/Spine/Thigh.R/Calf.R/Calf.R_end
    m_Weight: 0
  - m_Path: Armature/ElbowIK.L
    m_Weight: 0
  - m_Path: Armature/ElbowIK.L/ElbowIK.L_end
    m_Weight: 0
  - m_Path: Armature/ElbowIK.R
    m_Weight: 0
  - m_Path: Armature/ElbowIK.R/ElbowIK.R_end
    m_Weight: 0
  - m_Path: Armature/HandIK.L
    m_Weight: 0
  - m_Path: Armature/HandIK.L/HandIK.L_end
    m_Weight: 0
  - m_Path: Armature/HandIK.R
    m_Weight: 0
  - m_Path: Armature/HandIK.R/HandIK.R_end
    m_Weight: 0
  - m_Path: Armature/HeelIK.L
    m_Weight: 0
  - m_Path: Armature/HeelIK.L/Foot.L
    m_Weight: 0
  - m_Path: Armature/HeelIK.L/Foot.L/Toe.L
    m_Weight: 0
  - m_Path: Armature/HeelIK.L/Foot.L/Toe.L/Toe.L_end
    m_Weight: 0
  - m_Path: Armature/HeelIK.R
    m_Weight: 0
  - m_Path: Armature/HeelIK.R/Foot.R
    m_Weight: 0
  - m_Path: Armature/HeelIK.R/Foot.R/Toe.R
    m_Weight: 0
  - m_Path: Armature/HeelIK.R/Foot.R/Toe.R/Toe.R_end
    m_Weight: 0
  - m_Path: Armature/KneeIK.L
    m_Weight: 0
  - m_Path: Armature/KneeIK.L/KneeIK.L_end
    m_Weight: 0
  - m_Path: Armature/KneeIK.R
    m_Weight: 0
  - m_Path: Armature/KneeIK.R/KneeIK.R_end
    m_Weight: 0
  - m_Path: Char
    m_Weight: 0
