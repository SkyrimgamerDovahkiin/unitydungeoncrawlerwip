using UnityEngine;

[CreateAssetMenu(fileName = "Spell", menuName = "Inventory/Magic/Spells/Spell", order = 0)]
public class Spell : ScriptableObject
{
    public string label = "Spell";
    public SpellType SpellType;
    public int damage;
    public int time;
    public int cost;
    public GameObject UIItem;
    public GameObject InstanciableItem;
    public GameObject Trail;
    public AudioClip ShootClip;
}

public enum SpellType
{
    Touch, Ranged, Area, Self
}
