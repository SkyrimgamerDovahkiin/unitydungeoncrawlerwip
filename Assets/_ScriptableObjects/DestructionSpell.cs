using UnityEngine;

[CreateAssetMenu(fileName = "DestructionSpell", menuName = "Inventory/Magic/Spells/DestructionSpell", order = 0)]
public class DestructionSpell : Spell
{
    public DestructionSpellClass destructionSpellClass;
}

public enum DestructionSpellClass
{
    Fire, Frost, Lightning
}
