using UnityEngine;

[CreateAssetMenu(fileName = "StaminaPotion", menuName = "Inventory/Items/Potions/StaminaPotion", order = 0)]
public class StaminaPotionItem : PotionItem
{
    public float stamina;
}
