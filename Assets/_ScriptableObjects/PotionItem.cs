using UnityEngine;

[CreateAssetMenu(fileName = "Potion", menuName = "Inventory/Items/Potions/Potion", order = 0)]
public class PotionItem : ScriptableObject
{
    public string label = "Potion";
    public PotionType PotionType;
    public int weight;
    public int value;
    public AudioClip[] PickupClipArray;
    public AudioClip[] UseClipArray;
    public GameObject UIItem;
    //public GameObject InstanciableItem;
    public GameObject Collectable;
}
