using UnityEngine;

[CreateAssetMenu(fileName = "LootList", menuName = "Inventory/LootLists/MagicBook", order = 0)]
public class MagicBooksLootList : LootList
{
}
