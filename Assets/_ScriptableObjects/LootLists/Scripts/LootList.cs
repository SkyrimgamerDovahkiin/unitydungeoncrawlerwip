using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "LootList", menuName = "Inventory/LootLists", order = 0)]
public class LootList : ScriptableObject
{
    public List<ScriptableObject> Loot;

    [Range(0, 100)]
    public float chanceForNothing;
}
