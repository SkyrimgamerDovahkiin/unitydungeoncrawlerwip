using UnityEngine;

[CreateAssetMenu(fileName = "LootList", menuName = "Inventory/LootLists/Weapon", order = 0)]
public class WeaponLootList : LootList
{
}
