using UnityEngine;

[CreateAssetMenu(fileName = "LootList", menuName = "Inventory/LootLists/Armor", order = 0)]
public class ArmorLootList : LootList
{
}
