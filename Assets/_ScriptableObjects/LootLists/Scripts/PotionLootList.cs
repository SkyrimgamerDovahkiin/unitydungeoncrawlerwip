using UnityEngine;

[CreateAssetMenu(fileName = "LootList", menuName = "Inventory/LootLists/Potion", order = 0)]
public class PotionLootList : LootList
{
}
