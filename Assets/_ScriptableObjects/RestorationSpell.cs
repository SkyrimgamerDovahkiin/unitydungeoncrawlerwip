using UnityEngine;

[CreateAssetMenu(fileName = "RestorationSpell", menuName = "Inventory/Magic/Spells/RestorationSpell", order = 0)]
public class RestorationSpell : Spell
{
    
}

//gets added later, currently restoration Spells don't exist
// public enum RestorationSpellClass
// {
//     Fire, Frost, Lightning
// }
