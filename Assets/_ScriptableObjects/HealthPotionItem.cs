using UnityEngine;

[CreateAssetMenu(fileName = "HealthPotion", menuName = "Inventory/Items/Potions/HealthPotion", order = 0)]
public class HealthPotionItem : PotionItem
{
    public float health;
}
