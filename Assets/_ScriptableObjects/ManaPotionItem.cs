using UnityEngine;

[CreateAssetMenu(fileName = "ManaPotion", menuName = "Inventory/Items/Potions/ManaPotion", order = 0)]
public class ManaPotionItem : PotionItem
{
    public float mana;
}