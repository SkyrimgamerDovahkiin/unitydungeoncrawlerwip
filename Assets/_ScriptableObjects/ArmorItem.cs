using UnityEngine;

[CreateAssetMenu(fileName = "Armor", menuName = "Inventory/Items/Armor", order = 0)]
public class ArmorItem : ScriptableObject
{
    public string label = "Armor";
    public ArmorClass ArmorClass;
    public ArmorType ArmorType;
    public int weight;
    public int armor;
    public int value;
    public AudioClip[] PickupClipArray;
    public AudioClip[] HitSoundClipArray;
    public GameObject UIItem;
    public GameObject InstanciableItem;
    public GameObject Collectable;
}