using UnityEngine;

[CreateAssetMenu(fileName = "Magic Book", menuName = "Inventory/Items/Magic/Magic Book", order = 0)]
public class MagicBookItem : ScriptableObject
{
    public string label = "Magic Book";
    public SpellClass SpellClass;
    public Spell spell;
    public int weight;
    public int value;
    //public int amount = 1;
    public AudioClip[] PickupClipArray;
    public AudioClip[] UseSoundClipArray;
    public GameObject UIItem;
    public GameObject InstanciableItem;
    public GameObject Collectable;
}