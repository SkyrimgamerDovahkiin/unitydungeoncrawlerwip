using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Inventory/Items/Weapon", order = 0)]
public class WeaponItem : ScriptableObject
{
    public string label = "Weapon";
    public WeaponClass WeaponClass;
    public WeaponType WeaponType;
    public int weight;
    public int damage;
    public int damageLowStamina;
    public int value;
    public int StaminaToRemove;
    public AudioClip[] PickupClipArray;
    public AudioClip[] DrawSoundClipArray;
    public AudioClip[] SheatheSoundClipArray;
    public AudioClip[] FightingSoundClipArray;
    public GameObject UIItem;
    public GameObject InstanciableItem;
    public GameObject Collectable;
}