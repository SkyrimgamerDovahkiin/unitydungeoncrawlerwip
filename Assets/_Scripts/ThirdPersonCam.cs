/*
Copyright (c) 2014, Marié Paulino
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(This is the Simplified BSD Licence - http://opensource.org/licenses/BSD-2-Clause)
 */

using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;


//Third Person Camera Controller
public class ThirdPersonCam : MonoBehaviour
{
    [SerializeField] InputActionAsset inputActions;
    //[SerializeField] TextMeshProUGUI textMeshPro;

    #region Public fields
    [Space(10.0f)]
    [Tooltip("Point aimed by the camera")]
    public Transform Target;

    [Tooltip("Maximum distance between the camera and Target")]
    public float MaxDistance = 2;

    [Tooltip("Minimum distance between the camera and Target")]
    public float MinDistance = .2f;

    [Tooltip("Distance to change the camera from First to Third Person")]
    public float ChangeDistance = 1f;

    [Tooltip("Distance lerp factor")]
    [Range(.0f, 1.0f)]
    public float LerpSpeed = .1f;

    [Space(10.0f)]
    [Tooltip("Collision parameters")]
    public TraceInfo RayTrace = new TraceInfo { Thickness = .2f };

    [Tooltip("Camera pitch limitations")]
    public LimitsInfo PitchLimits = new LimitsInfo { Minimum = -60.0f, Maximum = 60.0f };

    [Tooltip("Input axes used to control the camera")]
    public InputInfo InputAxes = new InputInfo
    {
        Horizontal = new InputAxisInfo { Name = "Mouse X", Sensitivity = 15.0f },
        Vertical = new InputAxisInfo { Name = "Mouse Y", Sensitivity = 15.0f }
    };

    #endregion

    #region Structs

    [System.Serializable]
    public struct LimitsInfo
    {
        [Tooltip("Minimum pitch angle, in the range [-90, Maximum]")]
        public float Minimum;

        [Tooltip("Maximum pitch angle, in the range [Minimum, 90]")]
        public float Maximum;
    }

    [System.Serializable]
    public struct TraceInfo
    {
        [Tooltip("Ray thickness")]
        public float Thickness;

        [Tooltip("Layers the camera collide with")]
        public LayerMask CollisionMask;
    }

    [System.Serializable]
    public struct InputInfo
    {
        [Tooltip("Horizontal axis")]
        public InputAxisInfo Horizontal;

        [Tooltip("Vertical axis")]
        public InputAxisInfo Vertical;
    }

    [System.Serializable]
    public struct InputAxisInfo
    {
        [Tooltip("Input axis name")]
        public string Name;

        [Tooltip("Axis sensitivity")]
        public float Sensitivity;
    }

    #endregion

    float _pitch;
    float _distance;
    public float yRot;
    public float xRot;
    public float currentDistance;
    public float zoomSpeed = 0.0001f;
    public Vector3 rotationOffset = Vector3.zero;
    InputActionMap gameplayActionMap;
    InputAction RotateAction;
    InputAction ZoomAction;
    Vector3 MouseMoveVector;
    public GameObject Player;
    void Awake()
    {
        //NOTIZ: Wenn rotation geändert werden soll, dann NUR yRot oder xRot ändern!!!!
        //negative yRot == xRotation
        //normal xRot == yRotation
        //values sind so groß (immer zwei nullen ran), weil später noch mit der Sensitivity multipliziert wird
        gameplayActionMap = inputActions.FindActionMap("Player");
        RotateAction = gameplayActionMap.FindAction("Look");
        ZoomAction = gameplayActionMap.FindAction("Zoom");
        gameplayActionMap.Enable();
        //yRot = -36000f;
        //xRot = 2500f;
    }

    public void Start()
    {
        _pitch = Mathf.DeltaAngle(0, -transform.localEulerAngles.x);
        _distance = MaxDistance;
        currentDistance = MaxDistance;
    }

    public void Update()
    {
        Zoom();
    }

    public void LateUpdate()
    {
        if (Target == null) { return; }

        if (Time.timeScale > 0f)
        {
            Rotate(Target);
        }
        Move();
    }

    //FUNCTION SO THE THIRD PERSON CAM FOLLOWS THE PLAYER
    void Move()
    {
        if (Target == null) return;

        //SET START POSITION (THE TARGET) AND THE END POSITION
        var startPos = Target.position; //+ new Vector3(1.5f, 1.5f, 0f);
        var endPos = startPos - transform.forward * currentDistance;
        var result = Vector3.zero;

        //DO A RAYCAST TO GET DISTANCE
        RayCast(startPos, endPos, ref result, RayTrace.Thickness);
        var resultDistance = Vector3.Distance(Target.position, result);

        if (resultDistance <= _distance)    // closest collision
        {
            var resultY = result.y;
            //transform.localPosition = result;
            transform.position = result;
            _distance = resultDistance;
        }
        else
        {
            _distance = Mathf.Lerp(_distance, resultDistance, LerpSpeed);
            transform.localPosition = startPos - transform.forward * _distance;
        }
    }

    //THIRD PERSON CAMERA ZOOM
    void Zoom()
    {
        //GET ZOOM INPUT AND APPLY TO THE CURRENT DISTANCE
        float zoom = ZoomAction.ReadValue<Vector2>().y;
#if UNITY_EDITOR
        currentDistance -= zoom * zoomSpeed / 1000f;
#endif

        if (!Application.isEditor)
        {
            currentDistance -= zoom * zoomSpeed / 10f;
        }
        //textMeshPro.text = zoom + " is zooming!";
        //CLAMP DISTANCE SO THE ZOOM DOESN'T GO INTO THE PLAYER/TOO FAR AWAY
        if (currentDistance > MaxDistance)
        {
            currentDistance = MaxDistance;
            zoom = 0f;
        }
        else if (currentDistance < MinDistance)
        {
            currentDistance = MinDistance;
            zoom = 0f;
        }
    }

    //ROTATE THE THIRD PERSON CAMERA
    void Rotate(Transform transformFromTarget)
    {
        //SET THE VALUES TO GET LATER. QUATERNION IS NEEDED BECAUSE THE ENTITY ROTATION IS A QUATERNION
        MouseMoveVector = RotateAction.ReadValue<Vector2>();
        Vector3 pitchYawVector = Vector3.zero;
        Quaternion rotate = Quaternion.identity;
        //yRot -= ia.Player.Look.ReadValue<Vector2>().y;
        //xRot += MouseMoveVector.x;
        yRot -= MouseMoveVector.y;

        //SET THE PITCH AND CLAMP IT, SO IT DOESEN'T MOVE AROUND THE PLAYER
        //float yaw = xRot * InputAxes.Horizontal.Sensitivity;
        _pitch = yRot * InputAxes.Vertical.Sensitivity;
        _pitch = Mathf.Clamp(_pitch, PitchLimits.Minimum, PitchLimits.Maximum);

        if (yRot < PitchLimits.Minimum && Mathf.Sign(RotateAction.ReadValue<Vector2>().y) == -1f)
        {
            Debug.Log("is Min!");
            yRot = PitchLimits.Minimum;
        }
        else if (yRot > PitchLimits.Maximum && System.MathF.Sign(RotateAction.ReadValue<Vector2>().y) == 1f)
        {
            Debug.Log("is Max!");
            yRot = PitchLimits.Maximum;
        }

        //PUT THE VALUES IN THE ROTATE QUATERNION/THE VECTOR, THEN ROTATE THE CAMERA 
        //rotate = new Quaternion(0f, rotation.Value.value.y, 0f, rotation.Value.value.w);
        //rotate = new Quaternion(0f, transform.rotation.y, 0f, transform.rotation.w);
        //Vector3 rotationTest = new Vector3(transformFromTarget.rotation.eulerAngles.x, transformFromTarget.rotation.eulerAngles.y, 0f);
        Vector3 rotationTest = new Vector3(0f, transformFromTarget.rotation.eulerAngles.y, 0f);
        pitchYawVector = rotationTest;//transform.rotation.eulerAngles;//rotate.eulerAngles;
        pitchYawVector.x = _pitch;
        //pitchYawVector.y = yaw;
        //transform.eulerAngles = pitchYawVector + rotationOffset;
        transform.localEulerAngles = pitchYawVector + rotationOffset;

        //ORIGINAL
        // xRot += MouseMoveVector.x;
        // yRot -= MouseMoveVector.y;

        // //float yaw = transform.localEulerAngles.x + xRot * InputAxes.Horizontal.Sensitivity;
        // float yaw = xRot * InputAxes.Horizontal.Sensitivity;
        // _pitch = yRot * InputAxes.Vertical.Sensitivity;

        // _pitch = Mathf.Clamp(_pitch, PitchLimits.Minimum, PitchLimits.Maximum);

        // pitchYawVector.x = -_pitch;
        // pitchYawVector.y = yaw;

        // //if (MouseMoveVector.magnitude > 0.001f)
        // //{
        // transform.localEulerAngles = pitchYawVector;

        // //}
        // // else
        // // {
        // //     pitchYawVector = Vector3.zero;
        // // }
    }


    private bool RayCast(Vector3 start, Vector3 end, ref Vector3 result, float thickness)
    {
        var direction = end - start;
        var distance = Vector3.Distance(start, end);

        RaycastHit hit;
        if (Physics.SphereCast(new Ray(start, direction), thickness, out hit, distance, RayTrace.CollisionMask.value))
        {
            result = hit.point + hit.normal * RayTrace.Thickness;
            return true;
        }
        else
        {
            result = end;
            return false;
        }
    }
}
