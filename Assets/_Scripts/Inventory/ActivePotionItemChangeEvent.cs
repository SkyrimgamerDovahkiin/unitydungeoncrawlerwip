using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ActivePotionItemChangeEvent : UnityEvent<PotionItem>{}
