using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ActiveWeaponItemChangeEvent : UnityEvent<WeaponItem>{}