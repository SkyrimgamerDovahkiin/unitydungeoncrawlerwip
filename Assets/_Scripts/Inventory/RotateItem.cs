using UnityEngine;
using UnityEngine.InputSystem;

public class RotateItem : MonoBehaviour
{
    public float rotationSpeed = 5f;
    public InputActionAsset inputActions;
    InputAction UIRotateAction;
    InputAction UIDragClickAction;
    float xAxisRotation;
    float yAxisRotation;
    Camera cam;
    RotateManager rotateManager;

    void Awake()
    {
        rotateManager = GameObject.Find("GAME").GetComponent<RotateManager>();
        cam = GameObject.Find("UICam").GetComponent<Camera>();
        InputActionMap gameplayActionMap = inputActions.FindActionMap("UI");
        UIRotateAction = gameplayActionMap.FindAction("Rotate");
        rotateManager.rotateItem = this;
    }

    public void OnMouseDrag(InputAction.CallbackContext context)
    {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Mouse.current.position.ReadValue());
        if (Physics.Raycast(cam.ScreenPointToRay(Mouse.current.position.ReadValue()), out hit, 500f))
        {
            Vector2 rotateVector = UIRotateAction.ReadValue<Vector2>();
            xAxisRotation = rotateVector.x * rotationSpeed;
            yAxisRotation = rotateVector.y * rotationSpeed;

            // select the axis by which you want to rotate the GameObject
            transform.Rotate(Vector3.down, xAxisRotation);
            transform.Rotate(Vector3.right, yAxisRotation);
        }
    }
}
