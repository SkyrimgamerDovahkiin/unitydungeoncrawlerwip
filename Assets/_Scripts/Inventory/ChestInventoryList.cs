using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChestInventoryList : MonoBehaviour
{
    public AudioClip[] _openClips;
    public AudioClip[] _closeClips;
    public List<ScriptableObject> _items = new List<ScriptableObject>();
    [SerializeField] List<LootList> _lootLists = default;
    [SerializeField] int maxDraws = 5;
    //public List<string> _itemStrings {get; private set;} = new List<string>(); 
    public List<string> _itemStrings = new List<string>(); 
    ActiveWeaponItemChangeEvent activeWeaponItemChangeEvent;

    public void AddRandomItem()
    {
        int randRange = Random.Range(0, maxDraws + 1); //wie oft soll item "gezogen" werden?
        int index = 0; //index festlegen

        for (int i = 0; i < randRange; i++)
        {
            //random irgendeine loot list auswählen
            LootList lootList = _lootLists[Random.Range(0, _lootLists.Count)];

            //wenn random zahl unter "changeForNothing", dann nix "ziehen".
            int r = Random.Range(0, 100);
            if (r <= lootList.chanceForNothing)
            {
                index = -1;
            }
            else
            {
                index = Random.Range(0, lootList.Loot.Count);
            }

            //wenn index über null (-1) und item noch nicht auf der liste, dann hinzufügen
            if (index > -1)
            {
                if (!_itemStrings.Contains(lootList.Loot[index].name))
                {
                    _itemStrings.Add(lootList.Loot[index].name);
                }
                _items.Add(lootList.Loot[index]);
                //else if (_itemStrings.Contains(lootList.Loot[index].name))
                //{
                //_items.Add(lootList.Loot[index]);
                //}
            }
        }
    }
}
