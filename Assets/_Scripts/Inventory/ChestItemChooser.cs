using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class ChestItemChooser : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] ActiveWeaponItemChangeEvent activeWeaponItemChangeEvent;
    [SerializeField] ActiveMagicBookItemChangeEvent activeMagicBookItemChangeEvent;
    [SerializeField] ActiveArmorItemChangeEvent activeArmorItemChangeEvent;
    [SerializeField] ActivePotionItemChangeEvent activePotionItemChangeEvent;
    public WeaponItem weaponItem;
    public MagicBookItem magicBookItem;
    public ArmorItem armorItem;
    public PotionItem potionItem;
    public GameObject chestItemDetails;
    public ItemDetail chestItemDetail;
    public GameObject chestInvMarker;
    GameObject canvas;
    ChestInventoryItemList chestInventoryItemList;
    Player player;
    GameObject weapon;
    GameObject armor;
    GameObject potion;
    GameObject magicBook;

    void Awake()
    {
        chestInvMarker = GameObject.Find("ChestInvMarker");
        canvas = GameObject.Find("Canvas");
        chestItemDetails = GameObject.Find("ChestItemDetails");
        chestItemDetail = chestItemDetails.GetComponent<ItemDetail>();
        chestInventoryItemList = GameObject.Find("ChestMenu").GetComponent<ChestInventoryItemList>();
        player = canvas.GetComponent<Player>();

        activeWeaponItemChangeEvent.AddListener((weaponItem) => chestItemDetail.SetWeaponItem(weaponItem));
        activeMagicBookItemChangeEvent.AddListener((magicBookItem) => chestItemDetail.SetMagicBookItem(magicBookItem));
        activeArmorItemChangeEvent.AddListener((armorItem) => chestItemDetail.SetArmorItem(armorItem));
        activePotionItemChangeEvent.AddListener((potionItem) => chestItemDetail.SetPotionItem(potionItem));
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        if (weaponItem)
        {
            activeWeaponItemChangeEvent.Invoke(weaponItem);
            weapon = weaponItem.UIItem;

            if (chestInvMarker.transform.childCount != 0)
            {
                foreach (Transform child in chestInvMarker.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                Instantiate(weapon, chestInvMarker.transform.position, Quaternion.identity, chestInvMarker.transform);
            }
            else
            {
                Instantiate(weapon, chestInvMarker.transform.position, Quaternion.identity, chestInvMarker.transform);
            }
            GetComponentInChildren<Image>().enabled = true;
        }

        else if (armorItem)
        {
            activeArmorItemChangeEvent.Invoke(armorItem);
            armor = armorItem.UIItem;

            if (chestInvMarker.transform.childCount != 0)
            {
                foreach (Transform child in chestInvMarker.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                Instantiate(armor, chestInvMarker.transform.position, Quaternion.identity, chestInvMarker.transform);
            }
            else
            {
                Instantiate(armor, chestInvMarker.transform.position, Quaternion.identity, chestInvMarker.transform);
            }
            GetComponentInChildren<Image>().enabled = true;
        }

        else if (potionItem)
        {
            activePotionItemChangeEvent.Invoke(potionItem);
            potion = potionItem.UIItem;

            if (chestInvMarker.transform.childCount != 0)
            {
                foreach (Transform child in chestInvMarker.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                Instantiate(potion, chestInvMarker.transform.position, Quaternion.identity, chestInvMarker.transform);
            }
            else
            {
                Instantiate(potion, chestInvMarker.transform.position, Quaternion.identity, chestInvMarker.transform);
            }
            GetComponentInChildren<Image>().enabled = true;
        }

        else if (magicBookItem)
        {
            activeMagicBookItemChangeEvent.Invoke(magicBookItem);
            magicBook = magicBookItem.UIItem;

            if (chestInvMarker.transform.childCount != 0)
            {
                foreach (Transform child in chestInvMarker.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                Instantiate(magicBook, chestInvMarker.transform.position, Quaternion.identity, chestInvMarker.transform);
            }
            else
            {
                Instantiate(magicBook, chestInvMarker.transform.position, Quaternion.identity, chestInvMarker.transform);
            }
            GetComponentInChildren<Image>().enabled = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponentInChildren<Image>().enabled = false;
    }

    public void ChooseItem()
    {
        if (weaponItem != null)
        {
            player.AddWeaponItemInfo(weaponItem);
            chestInventoryItemList.RemoveFromWeaponItemDic(weaponItem);
            chestInventoryItemList.RemoveWeaponItem(weaponItem, this.gameObject);
            chestInventoryItemList.SetWeaponItemOnButtonClick(weaponItem, this.gameObject);
        }
        else if (magicBookItem != null)
        {
            player.AddMagicBookItemInfo(magicBookItem);
            chestInventoryItemList.RemoveFromMagicBookItemDic(magicBookItem);
            chestInventoryItemList.RemoveMagicBookItem(magicBookItem, this.gameObject);
            chestInventoryItemList.SetMagicBookItemOnButtonClick(magicBookItem, this.gameObject);
        }
        else if (armorItem != null)
        {
            player.AddArmorItemInfo(armorItem);
            chestInventoryItemList.RemoveFromArmorItemDic(armorItem);
            chestInventoryItemList.RemoveArmorItem(armorItem, this.gameObject);
            chestInventoryItemList.SetArmorItemOnButtonClick(armorItem, this.gameObject);
        }
        else if (potionItem != null)
        {
            player.AddPotionItemInfo(potionItem);
            chestInventoryItemList.RemoveFromPotionItemDic(potionItem);
            chestInventoryItemList.RemovePotionItem(potionItem, this.gameObject);
            chestInventoryItemList.SetPotionItemOnButtonClick(potionItem, this.gameObject);
        }
    }
}
