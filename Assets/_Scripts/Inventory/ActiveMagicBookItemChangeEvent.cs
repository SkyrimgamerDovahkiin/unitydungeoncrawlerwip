using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ActiveMagicBookItemChangeEvent : UnityEvent<MagicBookItem>{}