using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemDetail : MonoBehaviour
{
    public WeaponItem _weaponItem = default;
    public ArmorItem _armorItem = default;
    public PotionItem _potionItem = default;
    public MagicBookItem _magicBookItem = default;
    [SerializeField] TextMeshProUGUI _text = default;
    [SerializeField] TextMeshProUGUI _damage = default;
    [SerializeField] TextMeshProUGUI _weight = default;
    [SerializeField] TextMeshProUGUI _value = default;
    public void SetWeaponItem(WeaponItem item)
    {
        _weaponItem = item;
        _text.text = _weaponItem.label;
        _damage.enabled = true;
        _damage.text = "Damage: " + _weaponItem.damage.ToString();
        _weight.text = "Weight: " + _weaponItem.weight.ToString();
        _value.text = "Value: " + _weaponItem.value.ToString();
    }

    public void SetArmorItem(ArmorItem item)
    {
        _armorItem = item;
        _text.text = _armorItem.label;
        _damage.enabled = true;
        _damage.text = "Armor: " + _armorItem.armor.ToString();
        _weight.text = "Weight: " + _armorItem.weight.ToString();
        _value.text = "Value: " + _armorItem.value.ToString();
    }

    public void SetPotionItem(PotionItem item)
    {
        _potionItem = item;
        _text.text = _potionItem.label;
        _damage.enabled = false;
        _weight.text = "Weight: " + _potionItem.weight.ToString();
        _value.text = "Value: " + _potionItem.value.ToString();
    }
    
    public void SetMagicBookItem(MagicBookItem item)
    {
        _magicBookItem = item;
        _text.text = _magicBookItem.label;
        _damage.enabled = false;
        _weight.text = "Weight: " + _magicBookItem.weight.ToString();
        _value.text = "Value: " + _magicBookItem.value.ToString();
    }
}
