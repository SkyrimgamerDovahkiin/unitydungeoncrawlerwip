using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Ilumisoft.VisualStateMachine;

public class ChestInventoryItemList : MonoBehaviour
{
    //ActiveWeaponItemChangeEvent activeWeaponItemChangeEvent;
    //[SerializeField] GameObject newMenuItem;
    //public List<GameObject> _menuItems = new List<GameObject>();

    [SerializeField] string ButtonName;
    [SerializeField] StateMachine stateMachine = null;
    [SerializeField] Inventory inventory;
    [SerializeField] GameObject content;
    [SerializeField] GameObject menuItemTemplate;
    [SerializeField] GameObject inventoryItemList;
    [SerializeField] GameObject chestItemDetails;
    [SerializeField] GameObject chestInvMarker;
    List<GameObject> _menuItems = new List<GameObject>();
    public ChestInventoryList chestInventoryList { get; private set; }
    Dictionary<PotionItem, int> potionItemDic = new Dictionary<PotionItem, int>();
    Dictionary<MagicBookItem, int> magicBookItemDic = new Dictionary<MagicBookItem, int>();
    Dictionary<WeaponItem, int> weaponItemDic = new Dictionary<WeaponItem, int>();
    Dictionary<ArmorItem, int> armorItemDic = new Dictionary<ArmorItem, int>();
    GameObject newMenuItem;


    public void AddMenuItem(ScriptableObject item)
    {
        newMenuItem = Instantiate(menuItemTemplate, transform.position, transform.rotation);
        newMenuItem.SetActive(true);
        newMenuItem.transform.SetParent(content.transform, true);
        _menuItems.Add(newMenuItem);
    }

    public void AddToWeaponItemDic(WeaponItem item)
    {
        int count = 0;
        if (!weaponItemDic.ContainsKey(item))
        {
            weaponItemDic.Add(item, 1);
        }
        else
        {
            count = 0;
            weaponItemDic.TryGetValue(item, out count);
            weaponItemDic.Remove(item);
            weaponItemDic.Add(item, count + 1);
        }
    }

    public void SetWeaponItem()
    {
        int pos = 0;
        foreach (KeyValuePair<WeaponItem, int> entry in weaponItemDic)
        {
            string label = $" {entry.Key.label}";
            string damage = $" {entry.Key.damage}";
            string weight = $" {entry.Key.weight}";
            string value = $" {entry.Key.value}";
            string weaponType = $" {entry.Key.WeaponType}";

            for (int i = 0; i < _menuItems.Count; i++)
            {
                if (_menuItems[i].name == ButtonName)
                {
                    pos = i;
                    break;
                }
            }
            for (int i = 0; i < _menuItems.Count; i++)
            {
                if (_menuItems[i].name == label)
                {
                    pos = i;
                    break;
                }
            }

            newMenuItem = _menuItems[pos];
            newMenuItem.name = label;
            if (entry.Value == 1)
            {
                newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value}";
            }
            else if (entry.Value > 1)
            {
                newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value} {"(" + entry.Value + ")"}";
            }
            newMenuItem.GetComponent<ChestItemChooser>().weaponItem = entry.Key;
        }
    }

    public void RemoveFromWeaponItemDic(WeaponItem item)
    {
        int count = 0;
        foreach (KeyValuePair<WeaponItem, int> entry in weaponItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }
        weaponItemDic.Remove(item);
        weaponItemDic.Add(item, count - 1);
    }

    public void RemoveWeaponItem(WeaponItem item, GameObject button)
    {
        int count = 0;
        foreach (KeyValuePair<WeaponItem, int> entry in weaponItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }

        if (count <= 0)
        {
            _menuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 0)
        {
            count--;
        }
        chestInventoryList._items.Remove(item);
    }

    public void SetWeaponItemOnButtonClick(WeaponItem item, GameObject button)
    {
        string label = $" {item.label}";
        string damage = $" {item.damage}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string weaponType = $" {item.WeaponType}";
        int count = 0;

        foreach (KeyValuePair<WeaponItem, int> entry in weaponItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }

        newMenuItem = button;
        newMenuItem.name = label;

        if (count == 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value}";
        }
        else if (count < 1)
        {
            chestInventoryList._itemStrings.Remove(item.name);
        }
        else if (count > 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value} {"(" + count + ")"}";
        }
        newMenuItem.GetComponent<ChestItemChooser>().weaponItem = item;
    }

    #region MagicBookFunctions
    public void AddToMagicBookItemDic(MagicBookItem item)
    {
        int count = 0;
        if (!magicBookItemDic.ContainsKey(item))
        {
            magicBookItemDic.Add(item, 1);
        }
        else
        {
            count = 0;
            magicBookItemDic.TryGetValue(item, out count);
            magicBookItemDic.Remove(item);
            magicBookItemDic.Add(item, count + 1);
        }
    }

    public void SetMagicBookItem()
    {
        int pos = 0;
        foreach (KeyValuePair<MagicBookItem, int> entry in magicBookItemDic)
        {
            string label = $" {entry.Key.label}";
            string weight = $" {entry.Key.weight}";
            string value = $" {entry.Key.value}";
            string spellClass = $" {entry.Key.SpellClass}";

            for (int i = 0; i < _menuItems.Count; i++)
            {
                if (_menuItems[i].name == ButtonName)
                {
                    pos = i;
                    break;
                }
            }
            for (int i = 0; i < _menuItems.Count; i++)
            {
                if (_menuItems[i].name == label)
                {
                    pos = i;
                    break;
                }
            }

            newMenuItem = _menuItems[pos];
            newMenuItem.name = label;
            if (entry.Value == 1)
            {
                newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value}";
            }
            else if (entry.Value > 1)
            {
                newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value} {"(" + entry.Value + ")"}";
            }
            newMenuItem.GetComponent<ChestItemChooser>().magicBookItem = entry.Key;
        }
    }

    public void RemoveFromMagicBookItemDic(MagicBookItem item)
    {
        int count = 0;
        foreach (KeyValuePair<MagicBookItem, int> entry in magicBookItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
                //Debug.Log(entry.Key + " x " + entry.Value);
                //potionItemDic.TryGetValue(item, out count);
            }
        }
        magicBookItemDic.Remove(item);
        magicBookItemDic.Add(item, count - 1);
    }

    public void RemoveMagicBookItem(MagicBookItem item, GameObject button)
    {
        int count = 0;
        foreach (KeyValuePair<MagicBookItem, int> entry in magicBookItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }

        if (count <= 0)
        {
            _menuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 0)
        {
            count--;
        }
        chestInventoryList._items.Remove(item);
    }

    public void SetMagicBookItemOnButtonClick(MagicBookItem item, GameObject button)
    {
        string label = $" {item.label}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string spellClass = $" {item.SpellClass}";
        int count = 0;

        foreach (KeyValuePair<MagicBookItem, int> entry in magicBookItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }

        newMenuItem = button;
        newMenuItem.name = label;

        if (count == 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value}";
        }
        else if (count < 1)
        {
            chestInventoryList._itemStrings.Remove(item.name);
        }
        else if (count > 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value} {"(" + count + ")"}";
        }
        newMenuItem.GetComponent<ChestItemChooser>().magicBookItem = item;
    }
    #endregion MagicBookFunctions

    public void AddToArmorItemDic(ArmorItem item)
    {
        int count = 0;
        if (!armorItemDic.ContainsKey(item))
        {
            armorItemDic.Add(item, 1);
        }
        else
        {
            count = 0;
            armorItemDic.TryGetValue(item, out count);
            armorItemDic.Remove(item);
            armorItemDic.Add(item, count + 1);
        }
    }

    public void SetArmorItem()
    {
        int pos = 0;
        foreach (KeyValuePair<ArmorItem, int> entry in armorItemDic)
        {
            string label = $" {entry.Key.label}";
            string armor = $" {entry.Key.armor}";
            string weight = $" {entry.Key.weight}";
            string value = $" {entry.Key.value}";
            string armorType = $" {entry.Key.ArmorType}";
            string armorClass = $" {entry.Key.ArmorClass}";

            for (int i = 0; i < _menuItems.Count; i++)
            {
                if (_menuItems[i].name == ButtonName)
                {
                    pos = i;
                    break;
                }
            }
            for (int i = 0; i < _menuItems.Count; i++)
            {
                if (_menuItems[i].name == label)
                {
                    pos = i;
                    break;
                }
            }

            newMenuItem = _menuItems[pos];
            newMenuItem.name = label;
            if (entry.Value == 1)
            {
                newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value}";
            }
            else if (entry.Value > 1)
            {
                newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value} {"(" + entry.Value + ")"}";
            }
            newMenuItem.GetComponent<ChestItemChooser>().armorItem = entry.Key;
        }
    }

    public void RemoveFromArmorItemDic(ArmorItem item)
    {
        int count = 0;
        foreach (KeyValuePair<ArmorItem, int> entry in armorItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }
        armorItemDic.Remove(item);
        armorItemDic.Add(item, count - 1);
    }

    public void RemoveArmorItem(ArmorItem item, GameObject button)
    {
        int count = 0;
        foreach (KeyValuePair<ArmorItem, int> entry in armorItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }

        if (count <= 0)
        {
            _menuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 0)
        {
            count--;
        }
        chestInventoryList._items.Remove(item);
    }

    public void SetArmorItemOnButtonClick(ArmorItem item, GameObject button)
    {
        string label = $" {item.label}";
        string armor = $" {item.armor}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string armorType = $" {item.ArmorType}";
        string armorClass = $" {item.ArmorClass}";
        int count = 0;

        foreach (KeyValuePair<ArmorItem, int> entry in armorItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }

        newMenuItem = button;
        newMenuItem.name = label;

        if (count == 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value}";
        }
        else if (count < 1)
        {
            chestInventoryList._itemStrings.Remove(item.name);
        }
        else if (count > 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value} {"(" + count + ")"}";
        }
        newMenuItem.GetComponent<ChestItemChooser>().armorItem = item;
    }

    #region PotionFunctions
    public void AddToPotionItemDic(PotionItem item)
    {
        int count = 0;
        if (!potionItemDic.ContainsKey(item))
        {
            potionItemDic.Add(item, 1);
        }
        else
        {
            count = 0;
            potionItemDic.TryGetValue(item, out count);
            potionItemDic.Remove(item);
            potionItemDic.Add(item, count + 1);
        }
    }

    public void SetPotionItem()
    {
        int pos = 0;
        foreach (KeyValuePair<PotionItem, int> entry in potionItemDic)
        {
            string label = $" {entry.Key.label}";
            string weight = $" {entry.Key.weight}";
            string value = $" {entry.Key.value}";
            string potionType = $" {entry.Key.PotionType}";

            for (int i = 0; i < _menuItems.Count; i++)
            {
                if (_menuItems[i].name == ButtonName)
                {
                    pos = i;
                    break;
                }
            }
            for (int i = 0; i < _menuItems.Count; i++)
            {
                if (_menuItems[i].name == label)
                {
                    pos = i;
                    break;
                }
            }

            newMenuItem = _menuItems[pos];
            newMenuItem.name = label;
            if (entry.Value == 1)
            {
                newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value}";
            }
            else if (entry.Value > 1)
            {
                newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value} {"(" + entry.Value + ")"}";
            }
            newMenuItem.GetComponent<ChestItemChooser>().potionItem = entry.Key;
        }

    }

    public void RemoveFromPotionItemDic(PotionItem item)
    {
        int count = 0;
        foreach (KeyValuePair<PotionItem, int> entry in potionItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
                //Debug.Log(entry.Key + " x " + entry.Value);
                //potionItemDic.TryGetValue(item, out count);
            }
        }
        potionItemDic.Remove(item);
        potionItemDic.Add(item, count - 1);
    }

    public void RemovePotionItem(PotionItem item, GameObject button)
    {
        int count = 0;
        foreach (KeyValuePair<PotionItem, int> entry in potionItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }

        if (count <= 0)
        {
            _menuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 0)
        {
            count--;
        }
        chestInventoryList._items.Remove(item);
    }

    public void SetPotionItemOnButtonClick(PotionItem item, GameObject button)
    {
        string label = $" {item.label}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string potionType = $" {item.PotionType}";
        int count = 0;

        foreach (KeyValuePair<PotionItem, int> entry in potionItemDic)
        {
            if (entry.Key == item)
            {
                count = entry.Value;
            }
        }

        newMenuItem = button;
        newMenuItem.name = label;

        if (count == 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value}";
        }
        else if (count < 1)
        {
            chestInventoryList._itemStrings.Remove(item.name);
        }
        else if (count > 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weight} {value} {"(" + count + ")"}";
        }
        newMenuItem.GetComponent<ChestItemChooser>().potionItem = item;
    }
    #endregion PotionFunctions

    //Added Buttons zur menu liste, die dann in game angeziegt werden
    public void AddItems(ChestInventoryList chestInventoryList)
    {
        this.chestInventoryList = chestInventoryList;
        if (_menuItems.Count < chestInventoryList._itemStrings.Count)
        {
            //_menuItems.Clear();
            potionItemDic.Clear();
            magicBookItemDic.Clear();
            weaponItemDic.Clear();
            armorItemDic.Clear();
            for (int i = 0; i < chestInventoryList._itemStrings.Count; i++)
            {
                AddMenuItem(chestInventoryList._items[i]);
            }
            for (int i = 0; i < chestInventoryList._items.Count; i++)
            {
                if (chestInventoryList._items[i].GetType().Name == "WeaponItem")
                {
                    AddToWeaponItemDic((WeaponItem)chestInventoryList._items[i]);
                }
                else if (chestInventoryList._items[i].GetType().Name == "MagicBookItem")
                {
                    AddToMagicBookItemDic((MagicBookItem)chestInventoryList._items[i]);
                }
                else if (chestInventoryList._items[i].GetType().Name == "ArmorItem")
                {
                    AddToArmorItemDic((ArmorItem)chestInventoryList._items[i]);
                }
                else if (chestInventoryList._items[i].GetType().Name.Contains("PotionItem"))
                {
                    AddToPotionItemDic((PotionItem)chestInventoryList._items[i]);
                }
            }
            SetPotionItem();
            SetMagicBookItem();
            SetWeaponItem();
            SetArmorItem();
        }
    }


    //Sortiert die Buttons In game
    public void SortItemsByName(string property = "name")
    {
        if (property == "name")
        {
            var rankedSorted = _menuItems = _menuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
    }

    public void ChangeInvToChest()
    {
        stateMachine.Trigger("ShowChestInvChest");
        inventory.invKeyPressed = false;
        Time.timeScale = 0f;
        Cursor.visible = true;
    }

    public void ChangeInvToPlayer()
    {
        Time.timeScale = 0;
        inventoryItemList.SetActive(false);
        stateMachine.Trigger("ShowInventoryChestMenu");
        stateMachine.Trigger("ShowPlayerInvChest");
        // inventory.currentState.OnStateExit();
        // inventory.currentState = inventory.inventoryChestState;
        // inventory.currentState.OnStateEnter();
        inventory.invKeyPressed = true;
    }
}
