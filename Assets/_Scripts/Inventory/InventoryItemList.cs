using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryItemList : MonoBehaviour
{
    [SerializeField] GameObject sortByTypeArmor;
    [SerializeField] GameObject sortByTypeWeapons;
    [SerializeField] GameObject sortByTypePotions;
    [SerializeField] GameObject sortByTypeBooks;
    [SerializeField] GameObject sortByArmorClass;
    [SerializeField] GameObject SortByArmor;
    [SerializeField] GameObject SortByDamage;
    [SerializeField] GameObject _contentForWeapons = default;
    [SerializeField] GameObject _contentForArmor = default;
    [SerializeField] GameObject _contentForPotions = default;
    [SerializeField] GameObject _contentForBooks = default;
    [SerializeField] GameObject newMenuItem;
    [SerializeField] GameObject _menuItemTemplate = default;
    public List<WeaponItem> _weaponItems = default;
    public List<ArmorItem> _armorItems = default;
    public List<PotionItem> _potionItems = default;
    public List<MagicBookItem> _bookItems = default;
    public List<GameObject> _weaponMenuItems = new List<GameObject>();
    public List<GameObject> _armorMenuItems = new List<GameObject>();
    public List<GameObject> _potionMenuItems = new List<GameObject>();
    public List<GameObject> _bookMenuItems = new List<GameObject>();
    public string ButtonName;

    void Awake()
    {
        _bookItems.Clear();
        _bookMenuItems.Clear();
        sortByTypeArmor.SetActive(false);
        sortByTypePotions.SetActive(false);
        sortByTypeBooks.SetActive(false);
        sortByTypeWeapons.SetActive(true);
        sortByArmorClass.SetActive(false);
        SortByArmor.SetActive(false);
        SortByDamage.SetActive(true);
        _contentForArmor.SetActive(false);
        _contentForPotions.SetActive(false);
        _contentForBooks.SetActive(false);
    }

    public void AddWeaponItem(WeaponItem item)
    {
        if (!_weaponItems.Contains(item))
        {
            newMenuItem = Instantiate(_menuItemTemplate, transform.position, transform.rotation);
            newMenuItem.SetActive(true);
            newMenuItem.transform.SetParent(_contentForWeapons.transform, true);
            _weaponMenuItems.Add(newMenuItem);
        }
        _weaponItems.Add(item);
    }

    public void AddArmorItem(ArmorItem item)
    {
        if (!_armorItems.Contains(item))
        {
            newMenuItem = Instantiate(_menuItemTemplate, transform.position, transform.rotation);
            newMenuItem.SetActive(true);
            newMenuItem.transform.SetParent(_contentForArmor.transform, true);
            _armorMenuItems.Add(newMenuItem);
        }
        _armorItems.Add(item);
    }

    public void AddPotionItem(PotionItem item)
    {
        if (!_potionItems.Contains(item))
        {
            newMenuItem = Instantiate(_menuItemTemplate, transform.position, transform.rotation);
            newMenuItem.SetActive(true);
            newMenuItem.transform.SetParent(_contentForPotions.transform, true);
            _potionMenuItems.Add(newMenuItem);
        }
        _potionItems.Add(item);
    }

    public void AddMagicBookItem(MagicBookItem item)
    {

        if (!_bookItems.Contains(item))
        {
            newMenuItem = Instantiate(_menuItemTemplate, transform.position, transform.rotation);
            newMenuItem.SetActive(true);
            newMenuItem.transform.SetParent(_contentForBooks.transform, true);
            _bookMenuItems.Add(newMenuItem);
        }
        _bookItems.Add(item);
    }

    public void RemoveWeaponItem(WeaponItem item, GameObject button)
    {
        int count = 0;
        for (int i = 0; i < _weaponItems.Count(); i++)
        {
            if (_weaponItems[i] == item)
            {
                count++;
            }
        }

        if (count <= 1)
        {
            _weaponMenuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 1)
        {
            count--;
        }
        _weaponItems.Remove(item);
    }
    public void RemoveArmorItem(ArmorItem item, GameObject button)
    {
        int count = 0;
        for (int i = 0; i < _armorItems.Count(); i++)
        {
            if (_armorItems[i] == item)
            {
                count++;
            }
        }

        if (count <= 1)
        {
            _armorMenuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 1)
        {
            count--;
        }
        _armorItems.Remove(item);
    }
    public void RemovePotionItem(PotionItem item, GameObject button)
    {
        int count = 0;
        for (int i = 0; i < _potionItems.Count(); i++)
        {
            if (_potionItems[i] == item)
            {
                count++;
            }
        }

        if (count <= 1)
        {
            _potionMenuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 1)
        {
            count--;
        }
        _potionItems.Remove(item);
    }

    public void RemoveMagicBookItem(MagicBookItem item, GameObject button)
    {
        int count = 0;
        for (int i = 0; i < _bookItems.Count(); i++)
        {
            if (_bookItems[i] == item)
            {
                count++;
            }
        }

        if (count <= 1)
        {
            _bookMenuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 1)
        {
            count--;
        }
        _bookItems.Remove(item);
    }

    public void PutMagicBookItemInChest(MagicBookItem item, GameObject button)
    {
        int count = 0;
        for (int i = 0; i < _bookItems.Count(); i++)
        {
            if (_bookItems[i] == item)
            {
                count++;
            }
        }

        if (count <= 1)
        {
            _bookMenuItems.Remove(button);
            Destroy(button);
        }
        else if (count > 1)
        {
            count--;
        }
        _bookItems.Remove(item);
    }

    public void RemoveWeaponItems()
    {
        foreach (Transform child in _contentForWeapons.transform)
        {
            Destroy(child.gameObject);
        }
    }
    public void RemoveArmorItems()
    {
        foreach (Transform child in _contentForArmor.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void RemovePotionItems()
    {
        foreach (Transform child in _contentForPotions.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void RemoveBookItems()
    {
        foreach (Transform child in _contentForBooks.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void SetWeaponItem(WeaponItem item)
    {
        string label = $" {item.label}";
        string damage = $" {item.damage}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string weaponType = $" {item.WeaponType}";
        int count = 0;

        foreach (WeaponItem weaponItem in _weaponItems)
        {
            if (weaponItem.name == item.name)
            {
                count += 1;
                ButtonName = item.name;
            }
        }

        if (count == 1)
        {
            if (newMenuItem != null)
            {
                newMenuItem.name = label;
            }
            newMenuItem = _weaponMenuItems.Find(x => x.name.Contains(ButtonName));
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weaponType} {damage} {weight} {value}";
            newMenuItem.GetComponent<ItemChooser>().weaponItem = item;
        }
        else if (count > 1)
        {
            newMenuItem = _weaponMenuItems.Find(x => x.name.Contains(ButtonName));
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weaponType} {damage} {weight} {value} {"(" + count + ")"}";
        }
    }

    public void SetArmorItem(ArmorItem item)
    {
        string label = $" {item.label}";
        string armor = $" {item.armor}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string armorType = $" {item.ArmorType}";
        string armorClass = $" {item.ArmorClass}";
        int count = 0;

        foreach (ArmorItem armorItem in _armorItems)
        {
            if (armorItem.name == item.name)
            {
                count += 1;
                ButtonName = item.name;
            }
        }

        if (count == 1)
        {
            if (newMenuItem != null)
            {
                newMenuItem.name = label;
            }
            newMenuItem = _armorMenuItems.Find(x => x.name.Contains(ButtonName));
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {armorType} {armorClass} {armor} {weight} {value}";
            newMenuItem.GetComponent<ItemChooser>().armorItem = item;
        }
        else if (count > 1)
        {
            newMenuItem = _armorMenuItems.Find(x => x.name.Contains(ButtonName));
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {armorType} {armorClass} {armor} {weight} {value} {"(" + count + ")"}";
        }
    }

    public void SetPotionItem(PotionItem item)
    {
        string label = $" {item.label}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string potionType = $" {item.PotionType}";
        int count = 0;

        foreach (PotionItem potionItem in _potionItems)
        {
            if (potionItem.name == item.name)
            {
                count += 1;
                ButtonName = item.name;
            }
        }

        if (count == 1)
        {
            if (newMenuItem != null)
            {
                newMenuItem.name = label;
            }
            newMenuItem = _potionMenuItems.Find(x => x.name.Contains(ButtonName));
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {potionType} {weight} {value}";
            newMenuItem.GetComponent<ItemChooser>().potionItem = item;
        }
        else if (count > 1)
        {
            newMenuItem = _potionMenuItems.Find(x => x.name.Contains(ButtonName));
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {potionType} {weight} {value} {"(" + count + ")"}";
        }
    }

    public void SetBookItem(MagicBookItem item)
    {
        string label = $" {item.label}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string spellClass = $" {item.SpellClass}";
        int count = 0;

        foreach (MagicBookItem bookItem in _bookItems)
        {
            if (bookItem.name == item.name)
            {
                count += 1;
                ButtonName = item.name;
            }
        }

        if (count == 1)
        {
            if (newMenuItem != null)
            {
                newMenuItem.name = label;
            }
            newMenuItem = _bookMenuItems.Find(x => x.name.Contains(ButtonName));
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {spellClass} {weight} {value}";
            newMenuItem.GetComponent<ItemChooser>().magicBookItem = item;
        }
        else if (count > 1)
        {
            newMenuItem = _bookMenuItems.Find(x => x.name.Contains(ButtonName));
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {spellClass} {weight} {value} {"(" + count + ")"}";
        }
    }

    public void SetBookItemOnButtonClick(MagicBookItem item, GameObject button)
    {
        string label = $" {item.label}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string spellClass = $" {item.SpellClass}";
        int count = 0;

        foreach (MagicBookItem bookItem in _bookItems)
        {
            if (bookItem.name == item.name)
            {
                count += 1;
            }
        }

        newMenuItem = button;
        newMenuItem.name = label;
        if (count == 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {spellClass} {weight} {value}";
        }
        else if (count > 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {spellClass} {weight} {value} {"(" + count + ")"}";
        }
        newMenuItem.GetComponent<ItemChooser>().magicBookItem = item;
    }

    public void SetWeaponItemOnButtonClick(WeaponItem item, GameObject button)
    {
        string label = $" {item.label}";
        string damage = $" {item.damage}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string weaponType = $" {item.WeaponType}";
        int count = 0;

        foreach (WeaponItem weaponItem in _weaponItems)
        {
            if (weaponItem.name == item.name)
            {
                count += 1;
            }
        }

        newMenuItem = button;
        newMenuItem.name = label;

        if (count == 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weaponType} {damage} {weight} {value}";
        }
        else if (count > 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {weaponType} {damage} {weight} {value} {"(" + count + ")"}";
        }
        newMenuItem.GetComponent<ItemChooser>().weaponItem = item;
    }

    public void SetPotionItemOnButtonClick(PotionItem item, GameObject button)
    {
        string label = $" {item.label}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string potionType = $" {item.PotionType}";
        int count = 0;

        foreach (PotionItem potionItem in _potionItems)
        {
            if (potionItem.name == item.name)
            {
                count += 1;
            }
        }

        newMenuItem = button;
        newMenuItem.name = label;

        if (count == 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {potionType} {weight} {value}";
        }
        else if (count > 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {potionType} {weight} {value} {"(" + count + ")"}";
        }
        newMenuItem.GetComponent<ItemChooser>().potionItem = item;
    }

    public void SetArmorItemOnButtonClick(ArmorItem item, GameObject button)
    {
        string label = $" {item.label}";
        string armor = $" {item.armor}";
        string weight = $" {item.weight}";
        string value = $" {item.value}";
        string armorType = $" {item.ArmorType}";
        string armorClass = $" {item.ArmorClass}";
        int count = 0;

        foreach (ArmorItem armorItem in _armorItems)
        {
            if (armorItem.name == item.name)
            {
                count += 1;
            }
        }

        newMenuItem = button;
        newMenuItem.name = label;

        if (count == 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {armorType} {armorClass} {armor} {weight} {value}";
        }
        else if (count > 1)
        {
            newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {armorType} {armorClass} {armor} {weight} {value} {"(" + count + ")"}";
        }
        newMenuItem.GetComponent<ItemChooser>().armorItem = item;
    }

    public void SortByName(string property = "name")
    {
        if (property == "name" && _contentForWeapons.activeSelf)
        {
            _weaponItems = _weaponItems.OrderBy(item => item.name).ToList();
            var rankedSorted = _weaponMenuItems = _weaponMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "name" && _contentForArmor.activeSelf)
        {
            _armorItems = _armorItems.OrderBy(item => item.name).ToList();
            var rankedSorted = _armorMenuItems = _armorMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "name" && _contentForPotions.activeSelf)
        {
            _potionItems = _potionItems.OrderBy(item => item.name).ToList();
            var rankedSorted = _potionMenuItems = _potionMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "name" && _contentForBooks.activeSelf)
        {
            _bookItems = _bookItems.OrderBy(item => item.name).ToList();
            var rankedSorted = _bookMenuItems = _bookMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }

        }
    }

    public void SortByType(string property = "type")
    {
        if (property == "type" && _contentForWeapons.activeSelf)
        {
            _weaponItems = _weaponItems.OrderBy(item => item.WeaponType.ToString()).ToList();
            var rankedSorted = _weaponMenuItems = _weaponMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "type" && _contentForArmor.activeSelf)
        {
            _armorItems = _armorItems.OrderBy(item => item.ArmorType.ToString()).ToList();
            var rankedSorted = _armorMenuItems = _armorMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "type" && _contentForPotions.activeSelf)
        {
            _potionItems = _potionItems.OrderBy(item => item.PotionType.ToString()).ToList();
            var rankedSorted = _potionMenuItems = _potionMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "type" && _contentForBooks.activeSelf)
        {
            _bookItems = _bookItems.OrderBy(item => item.SpellClass.ToString()).ToList();
            var rankedSorted = _bookMenuItems = _bookMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }

        }
    }

    public void SortByWeight(string property = "weight")
    {
        if (property == "weight" && _contentForWeapons.activeSelf)
        {
            _weaponItems = _weaponItems.OrderBy(item => item.weight).ToList();
            var rankedSorted = _weaponMenuItems = _weaponMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "weight" && _contentForArmor.activeSelf)
        {
            _armorItems = _armorItems.OrderBy(item => item.weight).ToList();
            var rankedSorted = _armorMenuItems = _armorMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "weight" && _contentForPotions.activeSelf)
        {
            _potionItems = _potionItems.OrderBy(item => item.weight).ToList();
            var rankedSorted = _potionMenuItems = _potionMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "weight" && _contentForBooks.activeSelf)
        {
            _bookItems = _bookItems.OrderBy(item => item.weight).ToList();
            var rankedSorted = _bookMenuItems = _bookMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }

        }
    }

    public void SortByValue(string property = "value")
    {
        if (property == "value" && _contentForWeapons.activeSelf)
        {
            _weaponItems = _weaponItems.OrderBy(item => item.value).ToList();
            var rankedSorted = _weaponMenuItems = _weaponMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "value" && _contentForArmor.activeSelf)
        {
            _armorItems = _armorItems.OrderBy(item => item.value).ToList();
            var rankedSorted = _armorMenuItems = _armorMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "value" && _contentForPotions.activeSelf)
        {
            _potionItems = _potionItems.OrderBy(item => item.value).ToList();
            var rankedSorted = _potionMenuItems = _potionMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }
        }
        else if (property == "value" && _contentForBooks.activeSelf)
        {
            _bookItems = _bookItems.OrderBy(item => item.value).ToList();
            var rankedSorted = _bookMenuItems = _bookMenuItems.OrderBy(item => item.name).ToList();
            foreach (GameObject button in rankedSorted)
            {
                button.transform.SetAsLastSibling();
            }

        }
    }

    public void SwitchListToWeapons()
    {
        sortByTypeArmor.SetActive(false);
        sortByTypeWeapons.SetActive(true);
        sortByTypePotions.SetActive(false);
        sortByTypeBooks.SetActive(false);
        sortByArmorClass.SetActive(false);
        SortByArmor.SetActive(false);
        SortByDamage.SetActive(true);
        _contentForArmor.SetActive(false);
        _contentForPotions.SetActive(false);
        _contentForWeapons.SetActive(true);
        _contentForBooks.SetActive(false);
    }

    public void SwitchListToArmors()
    {
        sortByTypeArmor.SetActive(true);
        sortByTypeWeapons.SetActive(false);
        sortByTypePotions.SetActive(false);
        sortByTypeBooks.SetActive(false);
        sortByArmorClass.SetActive(true);
        SortByArmor.SetActive(true);
        SortByDamage.SetActive(false);
        _contentForArmor.SetActive(true);
        _contentForWeapons.SetActive(false);
        _contentForPotions.SetActive(false);
        _contentForBooks.SetActive(false);
    }

    public void SwitchListToPotions()
    {
        sortByTypeArmor.SetActive(false);
        sortByTypeWeapons.SetActive(false);
        sortByTypePotions.SetActive(true);
        sortByTypeBooks.SetActive(false);
        sortByArmorClass.SetActive(false);
        SortByArmor.SetActive(false);
        SortByDamage.SetActive(false);
        _contentForArmor.SetActive(false);
        _contentForWeapons.SetActive(false);
        _contentForPotions.SetActive(true);
        _contentForBooks.SetActive(false);
    }

    public void SwitchListToBooks()
    {
        sortByTypeArmor.SetActive(false);
        sortByTypeWeapons.SetActive(false);
        sortByTypePotions.SetActive(false);
        sortByTypeBooks.SetActive(true);
        sortByArmorClass.SetActive(false);
        SortByArmor.SetActive(false);
        SortByDamage.SetActive(false);
        _contentForArmor.SetActive(false);
        _contentForWeapons.SetActive(false);
        _contentForPotions.SetActive(false);
        _contentForBooks.SetActive(true);
    }
}
