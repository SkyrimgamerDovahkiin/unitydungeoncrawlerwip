using UnityEngine;
using UnityEngine.InputSystem;

public class RotateManager : MonoBehaviour
{
    public InputActionAsset inputActions;
    InputAction UIRotateAction;
    InputAction UIDragClickAction;
    public RotateItem rotateItem;

    void Awake()
    {
        InputActionMap gameplayActionMap = inputActions.FindActionMap("UI");
        UIRotateAction = gameplayActionMap.FindAction("Rotate");
        UIDragClickAction = gameplayActionMap.FindAction("DragClick");
        UIRotateAction.Enable();
        UIDragClickAction.Enable();
        UIDragClickAction.started += OnClickStarted;
        UIDragClickAction.canceled += OnClickCanceled;
    }

    void OnClickStarted(InputAction.CallbackContext context)
    {
        if (rotateItem != null)
        {
            UIRotateAction.performed += rotateItem.OnMouseDrag;
        }
    }

    void OnClickCanceled(InputAction.CallbackContext context)
    {
        if (rotateItem != null)
        {
            UIRotateAction.performed -= rotateItem.OnMouseDrag;
        }
    }

}
