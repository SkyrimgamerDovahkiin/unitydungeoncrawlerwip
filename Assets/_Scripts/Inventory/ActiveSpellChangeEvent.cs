using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ActiveSpellChangeEvent : UnityEvent<Spell>{}
