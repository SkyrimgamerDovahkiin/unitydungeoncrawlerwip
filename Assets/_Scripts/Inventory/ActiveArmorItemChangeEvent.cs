using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ActiveArmorItemChangeEvent : UnityEvent<ArmorItem>{}
