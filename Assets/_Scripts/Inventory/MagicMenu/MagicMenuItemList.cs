using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MagicMenuItemList : MonoBehaviour
{
    // //[SerializeField] GameObject sortByMag;

    //[SerializeField] GameObject sortByLevel;
    //[SerializeField] GameObject sortByDamage;
    //[SerializeField] GameObject sortByTime;
    //[SerializeField] GameObject sortByCost;

    [SerializeField] GameObject menuItemTemplate;
    [SerializeField] GameObject _contentForDestruction = default;
    [SerializeField] GameObject _contentForRestoration = default;
    //[SerializeField] GameObject _contentForConjuration = default;
    public List<DestructionSpell> _DestructionSpells = default;
    public List<RestorationSpell> _RestorationSpells = default;
    //public List<ConjurationSpell> _ConjurationSpells = default;
    public List<GameObject> _DestructionMenuItems = new List<GameObject>();
    public List<GameObject> _RestorationMenuItems = new List<GameObject>();
    //public List<GameObject> _ConjurationMenuItems = new List<GameObject>();
    public string ButtonName;
    //[SerializeField] GameObject newMenuItem;
    GameObject newMenuItem;

    void Awake()
    {
        _contentForDestruction.SetActive(true);
        _contentForRestoration.SetActive(false);
        //_contentForConjuration.SetActive(false);
    }

    public void AddDestructionSpell(DestructionSpell spell)
    {
        newMenuItem = Instantiate(menuItemTemplate, transform.position, transform.rotation);
        newMenuItem.SetActive(true);
        newMenuItem.transform.SetParent(_contentForDestruction.transform, true);
        _DestructionMenuItems.Add(newMenuItem);
        _DestructionSpells.Add(spell);
    }

    public void SetDestructionSpell(DestructionSpell spell)
    {
        string label = $" {spell.label}";
        string damage = $" {spell.damage}";
        string time = $" {spell.time}";
        string cost = $" {spell.cost}";
        string spellType = $" {spell.SpellType}";

        newMenuItem.name = label;
        newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {spellType} {damage} {time} {cost}";
        newMenuItem.GetComponent<MagicMenuItemChooser>().spell = spell;
    }

    public void AddRestorationSpell(RestorationSpell spell)
    {
        newMenuItem = Instantiate(menuItemTemplate, transform.position, transform.rotation);
        newMenuItem.SetActive(true);
        newMenuItem.transform.SetParent(_contentForRestoration.transform, true);
        _RestorationMenuItems.Add(newMenuItem);
        _RestorationSpells.Add(spell);
    }

    public void SetRestorationSpell(RestorationSpell spell)
    {
        string label = $" {spell.label}";
        string damage = $" {spell.damage}";
        string time = $" {spell.time}";
        string cost = $" {spell.cost}";
        string spellType = $" {spell.SpellType}";

        newMenuItem.name = label;
        newMenuItem.GetComponentInChildren<TextMeshProUGUI>().text = $"{label} {spellType} {damage} {time} {cost}";
        newMenuItem.GetComponent<MagicMenuItemChooser>().spell = spell;
    }

    public void SortSpellsByName()
    {
        //sorts the Destruction Spell list in ascending order
        _DestructionSpells = _DestructionSpells.OrderBy(item => item.name).ToList();

        //sorts the list of buttons in ascending order
        var rankedSorted = _DestructionMenuItems = _DestructionMenuItems.OrderBy(item => item.name).ToList();

        //sorts the actual button Game Obejects so they get shown correctly in game
        foreach (GameObject button in rankedSorted)
        {
            button.transform.SetAsLastSibling();
        }
    }

    public void SortSpellsByDamage()
    {
        _DestructionSpells = _DestructionSpells.OrderBy(item => item.damage.ToString()).ToList();
        var rankedSorted = _DestructionMenuItems = _DestructionMenuItems.OrderBy(item => item.name).ToList();
        foreach (GameObject button in rankedSorted)
        {
            button.transform.SetAsLastSibling();
        }
    }

    public void SortSpellsByType()
    {
        _DestructionSpells = _DestructionSpells.OrderBy(item => item.SpellType.ToString()).ToList();
        var rankedSorted = _DestructionMenuItems = _DestructionMenuItems.OrderBy(item => item.name).ToList();
        foreach (GameObject button in rankedSorted)
        {
            button.transform.SetAsLastSibling();
        }
    }

    public void SortSpellsByTime()
    {
        _DestructionSpells = _DestructionSpells.OrderBy(item => item.time.ToString()).ToList();
        var rankedSorted = _DestructionMenuItems = _DestructionMenuItems.OrderBy(item => item.name).ToList();
        foreach (GameObject button in rankedSorted)
        {
            button.transform.SetAsLastSibling();
        }
    }

    public void SortSpellsByCost()
    {  
        _DestructionSpells = _DestructionSpells.OrderBy(item => item.cost.ToString()).ToList();
        var rankedSorted = _DestructionMenuItems = _DestructionMenuItems.OrderBy(item => item.name).ToList();
        foreach (GameObject button in rankedSorted)
        {
            button.transform.SetAsLastSibling();
        }
    }

    public void RemoveDestructionSpells()
    {
        foreach (Transform child in _contentForDestruction.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void RemoveRestorationSpells()
    {
        foreach (Transform child in _contentForDestruction.transform)
        {
            Destroy(child.gameObject);
        }
    }
    
    public void SwitchListToDestructionSpells()
    {
        _contentForRestoration.SetActive(false);
        _contentForDestruction.SetActive(true);
    }

    public void SwitchListToRestorationSpells()
    {
        _contentForRestoration.SetActive(true);
        _contentForDestruction.SetActive(false);
    }
}
