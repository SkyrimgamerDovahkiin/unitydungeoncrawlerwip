using UnityEngine;
using TMPro;

public class MagicMenuItemDetail : MonoBehaviour
{
    //Spell _Spell = default;
    [SerializeField] TextMeshProUGUI _text = default;
    [SerializeField] TextMeshProUGUI _damage = default;
    [SerializeField] TextMeshProUGUI _time = default;
    [SerializeField] TextMeshProUGUI _cost = default;

    public void SetSpell(Spell spell)
    {
        //_Spell = spell;
        _text.text = spell.label;
        _damage.text = "Damage: " + spell.damage.ToString();
        _time.text = "Time: " + spell.time.ToString();
        _cost.text = "Cost: " + spell.cost.ToString();
    }
}
