using UnityEngine;
//using UnityEngine.Events;
using UnityEngine.EventSystems;
//using TMPro;
using UnityEngine.UI;
//using UnityEngine.InputSystem;

public class MagicMenuItemChooser : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //public InputActionAsset inputActions;
    // GameObject magicMenuItemDetails;
    [HideInInspector] public Spell spell;
    [HideInInspector] public ActiveSpellChangeEvent activeSpellChangeEvent = default;
    HealthSystem healthSystem;
    SimpleGrabSystem simpleGrabSystem;
    MagicMenuItemDetail magicMenuItemDetail;
    Player player;
    MagicMenuItemList magicMenuItemList;
    GameObject canvas;
    GameObject magicMenuInvMarker;
    GameObject spellUIItem;

    void Awake()
    {
        magicMenuInvMarker = GameObject.Find("MagicMenuInvMarker");
        canvas = GameObject.Find("Canvas");
        player = canvas.GetComponent<Player>();
        healthSystem = canvas.GetComponent<HealthSystem>();
        simpleGrabSystem = GameObject.Find("Player").GetComponent<SimpleGrabSystem>();
        magicMenuItemDetail = player.magicMenuItemDetail;
        magicMenuItemList = player.magicMenuItemList;

        activeSpellChangeEvent.AddListener((spell) => magicMenuItemDetail.SetSpell(spell));
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (spell)
        {
            activeSpellChangeEvent.Invoke(spell);
            spellUIItem = spell.UIItem;

            foreach (Transform child in magicMenuInvMarker.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            Instantiate(spellUIItem, magicMenuInvMarker.transform.position, Quaternion.identity, magicMenuInvMarker.transform);
            GetComponentInChildren<Image>().enabled = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponentInChildren<Image>().enabled = false;
    }

    public void ChooseItem()
    {
        //Nur Spell unequippen, wenn auf selben button geclickt wird;
        if (spell)
        {
            if (!simpleGrabSystem.isEquippedSpellOneHanded || magicMenuItemList.ButtonName != gameObject.name)
            {
                magicMenuItemList.ButtonName = gameObject.name;
                simpleGrabSystem.UnequipOneHanded();
                simpleGrabSystem.EquipSpell(spell.InstanciableItem);
                if (!simpleGrabSystem.isSheathedSpellSide) //|| !simpleGrabSystem.isSheathedWeaponSide)
                {
                    simpleGrabSystem.DrawSpell(simpleGrabSystem.MainHand);
                }
            }
            else if (simpleGrabSystem.isEquippedSpellOneHanded && magicMenuItemList.ButtonName == gameObject.name)
            {
                simpleGrabSystem.UnequipOneHanded();
                if (!simpleGrabSystem.isSheathedSpellSide)
                {
                    simpleGrabSystem.isSheathedSpellSide = true;
                }
            }
        }
    }
}
