using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class ItemChooser : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] ActiveWeaponItemChangeEvent activeWeaponItemChangeEvent = default;
    [SerializeField] ActiveArmorItemChangeEvent activeArmorItemChangeEvent = default;
    [SerializeField] ActivePotionItemChangeEvent activePotionItemChangeEvent = default;
    [SerializeField] ActiveMagicBookItemChangeEvent activeMagicBookItemChangeEvent = default;
    public InputActionAsset inputActions;
    public InputAction DropAction;
    public WeaponItem weaponItem;
    public ArmorItem armorItem;
    public PotionItem potionItem;
    public HealthPotionItem healthPotionItem;
    public MagicBookItem magicBookItem;
    GameObject itemDetails;
    GameObject canvas;
    GameObject invMarker;
    SimpleGrabSystem simpleGrabSystem;
    ItemDetail itemDetail;
    GameObject weapon;
    GameObject armor;
    GameObject potion;
    GameObject magicBook;
    InventoryItemList inventoryItemList;
    MagicMenuItemList magicMenuItemList;
    HealthSystem healthSystem;
    Player player;
    GameObject thoughtsGO;
    TextMeshProUGUI thoughts;
    TextFade textFade;

    void Start()
    {
        invMarker = GameObject.Find("InvMarker");
        canvas = GameObject.Find("Canvas");
        healthSystem = canvas.GetComponent<HealthSystem>();
        player = canvas.GetComponent<Player>();
        itemDetails = GameObject.Find("ItemDetails");
        itemDetail = itemDetails.GetComponent<ItemDetail>();
        thoughtsGO = GameObject.Find("Thoughts");
        thoughts = thoughtsGO.GetComponent<TextMeshProUGUI>();
        textFade = thoughtsGO.GetComponent<TextFade>();
        GameObject playerGO = GameObject.Find("Player");
        simpleGrabSystem = playerGO.GetComponent<SimpleGrabSystem>();
        var gameplayActionMap = inputActions.FindActionMap("UI");
        DropAction = gameplayActionMap.FindAction("Drop");
        DropAction.Enable();
        inventoryItemList = GameObject.Find("Inventory").GetComponent<InventoryItemList>();
        magicMenuItemList = GameObject.Find("MagicMenu").GetComponent<MagicMenuItemList>();

        activeWeaponItemChangeEvent.AddListener((weaponItem) => itemDetail.SetWeaponItem(weaponItem));
        activeArmorItemChangeEvent.AddListener((armorItem) => itemDetail.SetArmorItem(armorItem));
        activePotionItemChangeEvent.AddListener((potionItem) => itemDetail.SetPotionItem(potionItem));
        activeMagicBookItemChangeEvent.AddListener((magicBookItem) => itemDetail.SetMagicBookItem(magicBookItem));
    }

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject() && weaponItem && DropAction.triggered && GetComponentInChildren<Image>().enabled == true)
        {
            inventoryItemList.RemoveWeaponItem(weaponItem, this.gameObject);
            inventoryItemList.SetWeaponItemOnButtonClick(weaponItem, this.gameObject);
            foreach (Transform child in invMarker.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        if (EventSystem.current.IsPointerOverGameObject() && armorItem && DropAction.triggered && GetComponentInChildren<Image>().enabled == true)
        {
            inventoryItemList.RemoveArmorItem(armorItem, this.gameObject);
            inventoryItemList.SetWeaponItemOnButtonClick(weaponItem, this.gameObject);
            foreach (Transform child in invMarker.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        if (EventSystem.current.IsPointerOverGameObject() && potionItem && DropAction.triggered && GetComponentInChildren<Image>().enabled == true)
        {
            inventoryItemList.RemovePotionItem(potionItem, this.gameObject);
            inventoryItemList.SetPotionItemOnButtonClick(potionItem, this.gameObject);
            foreach (Transform child in invMarker.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
        if (EventSystem.current.IsPointerOverGameObject() && magicBookItem && DropAction.triggered && GetComponentInChildren<Image>().enabled == true)
        {
            //Vector3 offset = new Vector3(0.6f, -0.6f, Camera.main.transform.localPosition.z);
            //Instantiate(weaponItem.collectable, Camera.main.transform.forward, Quaternion.identity);
            Debug.Log(magicBookItem + " magicBookItem");
            inventoryItemList.RemoveMagicBookItem(magicBookItem, this.gameObject);
            inventoryItemList.SetBookItemOnButtonClick(magicBookItem, this.gameObject);
            foreach (Transform child in invMarker.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (weaponItem)
        {
            activeWeaponItemChangeEvent.Invoke(weaponItem);
            weapon = weaponItem.UIItem;

            if (invMarker.transform.childCount != 0)
            {
                foreach (Transform child in invMarker.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                Instantiate(weapon, invMarker.transform.position, Quaternion.identity, invMarker.transform);
            }
            else
            {
                Instantiate(weapon, invMarker.transform.position, Quaternion.identity, invMarker.transform);
            }
            GetComponentInChildren<Image>().enabled = true;
        }

        if (armorItem)
        {
            activeArmorItemChangeEvent.Invoke(armorItem);
            armor = armorItem.UIItem;

            if (invMarker.transform.childCount != 0)
            {
                foreach (Transform child in invMarker.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                Instantiate(armor, invMarker.transform.position, Quaternion.identity, invMarker.transform);
            }
            else
            {
                Instantiate(armor, invMarker.transform.position, Quaternion.identity, invMarker.transform);
            }
            GetComponentInChildren<Image>().enabled = true;
        }
        if (potionItem)
        {
            activePotionItemChangeEvent.Invoke(potionItem);
            potion = potionItem.UIItem;

            if (invMarker.transform.childCount != 0)
            {
                foreach (Transform child in invMarker.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                Instantiate(potion, invMarker.transform.position, Quaternion.identity, invMarker.transform);
            }
            else
            {
                Instantiate(potion, invMarker.transform.position, Quaternion.identity, invMarker.transform);
            }
            GetComponentInChildren<Image>().enabled = true;
        }
        if (magicBookItem)
        {
            activeMagicBookItemChangeEvent.Invoke(magicBookItem);
            magicBook = magicBookItem.UIItem;

            Debug.Log(invMarker + "MagicBookInvMarker");
            if (invMarker.transform.childCount != 0)
            {
                foreach (Transform child in invMarker.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
                Instantiate(magicBook, invMarker.transform.position, Quaternion.identity, invMarker.transform);
            }
            else
            {
                Instantiate(magicBook, invMarker.transform.position, Quaternion.identity, invMarker.transform);
            }
            GetComponentInChildren<Image>().enabled = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponentInChildren<Image>().enabled = false;
    }

    public void ChooseItem()
    {
        //Nur Waffe unequippen, wenn auf selben button geclickt wird;
        if (weaponItem)
        {
            if ((weaponItem.WeaponType == WeaponType.Sword && !simpleGrabSystem.isEquippedOneHanded) || inventoryItemList.ButtonName != this.gameObject.name)
            {
                inventoryItemList.ButtonName = this.gameObject.name;
                simpleGrabSystem.UnequipOneHanded();
                simpleGrabSystem.EquipOneHanded(weaponItem.InstanciableItem);
                if (!simpleGrabSystem.isSheathedWeaponSide || !simpleGrabSystem.isSheathedSpellSide || !simpleGrabSystem.isSheathedWeaponBack)
                {
                    simpleGrabSystem.DrawWeapon(simpleGrabSystem.MainHand);
                }
            }

            else if (weaponItem.WeaponType == WeaponType.Sword && simpleGrabSystem.isEquippedOneHanded && inventoryItemList.ButtonName == this.gameObject.name)
            {
                simpleGrabSystem.UnequipOneHanded();
                if (!simpleGrabSystem.isSheathedWeaponSide)
                {
                    simpleGrabSystem.isSheathedWeaponSide = true;
                }
            }
        }
        else if (armorItem)
        {
            if ((armorItem.ArmorType == ArmorType.Shield && !simpleGrabSystem.isEquippedSecondHand) || inventoryItemList.ButtonName != this.gameObject.name)
            {
                inventoryItemList.ButtonName = this.gameObject.name;

                simpleGrabSystem.UnequipSecondHand();
                simpleGrabSystem.EquipSecondHand(armorItem.InstanciableItem);
                if (!simpleGrabSystem.isSheathedWeaponBack || !simpleGrabSystem.isSheathedWeaponSide)
                {
                    simpleGrabSystem.DrawShield(simpleGrabSystem.SecondHand);
                }
            }

            else if (armorItem.ArmorType == ArmorType.Shield && simpleGrabSystem.isEquippedSecondHand)
            {
                simpleGrabSystem.UnequipSecondHand();
                if (!simpleGrabSystem.isSheathedWeaponBack)
                {
                    simpleGrabSystem.isSheathedWeaponBack = true;
                }
            }
        }
        else if (potionItem)
        {
            if (potionItem.PotionType == PotionType.Health)
            {
                HealthPotionItem healthPotionItem;
                healthPotionItem = (HealthPotionItem)potionItem;
                inventoryItemList.RemovePotionItem(potionItem, this.gameObject);
                healthSystem.RestoreHealth(healthPotionItem.health);
            }
            else if (potionItem.PotionType == PotionType.Stamina)
            {
                StaminaPotionItem staminaPotionItem;
                staminaPotionItem = (StaminaPotionItem)potionItem;
                inventoryItemList.RemovePotionItem(potionItem, this.gameObject);
                healthSystem.RestoreStamina(staminaPotionItem.stamina);
            }
            else if (potionItem.PotionType == PotionType.Mana)
            {
                ManaPotionItem manaPotionItem;
                manaPotionItem = (ManaPotionItem)potionItem;
                inventoryItemList.RemovePotionItem(potionItem, this.gameObject);
                healthSystem.RestoreMana(manaPotionItem.mana);
            }
            inventoryItemList.SetPotionItemOnButtonClick(potionItem, this.gameObject);
        }
        else if (magicBookItem)
        {
            //Passiert beim Klick auf Magic Book
            //Wenn NICHT Chest geöffnet
            if (magicBookItem.SpellClass == SpellClass.Destruction && !magicMenuItemList._DestructionSpells.Contains((DestructionSpell)magicBookItem.spell))
            {
                player.AddDestructionSpellInfo((DestructionSpell)magicBookItem.spell);
                inventoryItemList.RemoveMagicBookItem(magicBookItem, this.gameObject);
                inventoryItemList.SetBookItemOnButtonClick(magicBookItem, this.gameObject);
            }
            else if (magicBookItem.SpellClass == SpellClass.Destruction && magicMenuItemList._DestructionSpells.Contains((DestructionSpell)magicBookItem.spell))
            {
                thoughts.text = "I already know that spell!";
                if (!textFade.isRunning)
                {
                    textFade.StartCoroutine(textFade.AnimateVertexColors());
                }
            }

            else if (magicBookItem.SpellClass == SpellClass.Restoration && !magicMenuItemList._RestorationSpells.Contains((RestorationSpell)magicBookItem.spell))
            {
                player.AddRestorationSpellInfo((RestorationSpell)magicBookItem.spell);
                inventoryItemList.RemoveMagicBookItem(magicBookItem, this.gameObject);
                inventoryItemList.SetBookItemOnButtonClick(magicBookItem, this.gameObject);
            }
            else if (magicBookItem.SpellClass == SpellClass.Restoration && magicMenuItemList._RestorationSpells.Contains((RestorationSpell)magicBookItem.spell))
            {
                thoughts.text = "I already know that spell!";
                if (!textFade.isRunning)
                {
                    textFade.StartCoroutine(textFade.AnimateVertexColors());
                }
            }
            //Wenn Chest geöffnet, muss noch implementiert werden
            //PutMagicBookItemInChest (InventoryItemList)
            //AddMenuItem(ChestInventoryItemList)
            //SetMagicBookItem(ChestInventoryItemList)
        }
    }
}
