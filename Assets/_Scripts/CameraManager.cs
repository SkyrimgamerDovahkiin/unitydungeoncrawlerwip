using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraManager : MonoBehaviour
{
    [SerializeField] ThirdPersonCam thirdPersonCam;
    [SerializeField] FirstPersonCam firstPersonCam;
    [SerializeField] GameObject ThirdPersonCamera;
    [SerializeField] GameObject FirstPersonCamera;
    [SerializeField] FirstPersonController FirstPersonController;
    [SerializeField] ThirdPersonController ThirdPersonController;
    [SerializeField] Player player;

    void Update()
    {
        bool isZoomedOut = firstPersonCam.Zoom();

        if (thirdPersonCam.currentDistance <= thirdPersonCam.ChangeDistance)
        {
            player.cam = FirstPersonCamera.GetComponent<Camera>();
            ThirdPersonController.enabled = false;
            FirstPersonController.enabled = true;
            ThirdPersonCamera.SetActive(false);
            thirdPersonCam.currentDistance = thirdPersonCam.MinDistance + 0.1f;
            FirstPersonController.xRot = ThirdPersonController.xRot;
            thirdPersonCam.rotationOffset = new Vector3(ThirdPersonCamera.transform.localEulerAngles.x, 0f, 0f);
            FirstPersonCamera.SetActive(true);
            thirdPersonCam.rotationOffset = Vector3.zero;
        }
        if (isZoomedOut)
        {
            player.cam = ThirdPersonCamera.GetComponent<Camera>();
            FirstPersonController.enabled = false;
            ThirdPersonController.enabled = true;
            ThirdPersonController.xRot = FirstPersonController.xRot;
            ThirdPersonCamera.SetActive(true);
            thirdPersonCam.rotationOffset = new Vector3(FirstPersonCamera.transform.localEulerAngles.x, 0f, 0f);
            FirstPersonCamera.SetActive(false);
            thirdPersonCam.rotationOffset = Vector3.zero;
        }
    }
}
