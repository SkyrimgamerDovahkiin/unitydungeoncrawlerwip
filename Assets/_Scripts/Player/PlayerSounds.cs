using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    AudioSource source;
    int footstepCounter;
    float footstepTimer;
    float walkSpeed;
    public float maxFsDelay = 1.5f;
    [SerializeField] List<AudioClip> footsteps = new List<AudioClip>();
    [SerializeField] float minFsDelay = 0.2f;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    public void UpdateFootsteps(float walkSpeed)
    {
        //this.walkSpeed = walkSpeed;
        //if (walkSpeed < 0.08f) { return; }

        float footstepDelay = Mathf.Lerp(maxFsDelay, minFsDelay, walkSpeed);

        if (footstepTimer == 0f)
        {
            source.clip = footsteps[footstepCounter];
            source.Play();
            footstepCounter++;
        }
        else if (footstepTimer >= footstepDelay)
        {
            footstepTimer = 0f;
            footstepCounter++;
            if (footstepCounter >= footsteps.Count)
            {
                footstepCounter = 0;
                footsteps.Shuffle();
            }
            source.clip = footsteps[footstepCounter];
            source.Play();
        }
        footstepTimer += Time.deltaTime;
    }
}
