using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerWeaponSounds : MonoBehaviour
{
    [SerializeField] Transform MainHand;
    [SerializeField] Transform SecondHand;
    [SerializeField] Transform SideSlot;
    [SerializeField] Transform BackSlot;

    public void PlayAttackClip()
    {
        WeaponData weaponData = MainHand.GetChild(0).GetComponent<WeaponData>();
        weaponData.PlayAttackClipAtPoint(this.transform);
    }

    public void Play1HDrawClip()
    {
        WeaponData weaponData = SideSlot.GetChild(0).GetComponent<WeaponData>();
        weaponData.PlayDrawClipAtPoint(this.transform);
    }

    public void Play1HSheatheClip()
    {
        WeaponData weaponData = MainHand.GetChild(0).GetComponent<WeaponData>();
        weaponData.PlaySheatheClipAtPoint(this.transform);
    }

    public void ActivateBlockingCollider()
    {
        Transform armorItemTransform = SecondHand.GetChild(0);
        ArmorData armorData = armorItemTransform.GetComponent<ArmorData>();
        armorData.ActivateBlockCollider();
    }

    public void DeactivateBlockingCollider()
    {
        Transform armorItemTransform = SecondHand.GetChild(0);
        ArmorData armorData = armorItemTransform.GetComponent<ArmorData>();
        armorData.DeactivateBlockCollider();
    }

    //TODO: Implement that when Weapon hits or shield gets hit, a sound plays
}
