using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] MainMenu menu;
    [SerializeField] HealthSystem hs;
    [SerializeField] FirstPersonController fpc;
    [SerializeField] InventoryItemList iil;
    [SerializeField] MagicMenuItemList mmil;
    [SerializeField] SimpleGrabSystem sgs;
    [SerializeField] CharacterController cc;
    [SerializeField] GameObject backgroundMM;
    [SerializeField] Transform EnemyEmpty;
    [SerializeField] Transform ItemEmpty;
    [SerializeField] Transform ChestEmpty;
    [SerializeField] GameObject settings;
    [SerializeField] GameObject player;
    [SerializeField] Image health;
    [SerializeField] Image stamina;
    [SerializeField] Image mana;
    [SerializeField] ExperienceManager xpMan;
    string sceneName;
    string weaponPath = "Assets/_ScriptableObjects/Items/Weapons/";
    string armorPath = "Assets/_ScriptableObjects/Items/Armor/";
    string potionPath = "Assets/_ScriptableObjects/Items/Potions/";
    string magicBooksPath = "Assets/_ScriptableObjects/Magic/Books/";
    string destructionSpellsPath = "Assets/_ScriptableObjects/Magic/Spells/Destruction/";
    string restorationSpellsPath = "Assets/_ScriptableObjects/Magic/Spells/Restoration/";
    PlayerData data;
    public GameData gameData;
    GameObject cocMarkerHeading;
    ChestInventoryList chestInventoryList;

    void Awake()
    {
        try { gameData = GameObject.Find("GameData").GetComponent<GameData>(); }
        catch { Debug.LogWarning("gameData not found, probably because testing!"); }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Level01");
        gameData.ngbPressed = true;
        sceneName = SceneManager.GetActiveScene().name;
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void SaveGame()
    {
        SaveSystem.SavePlayer(hs, fpc, iil, mmil, sgs, xpMan);
    }

    public void LoadGameButtonClick()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;

        if (sceneName != "MainMenuScene")
        {
            //passiert nur, wenn in game, also nicht mainmenu scene
            LoadGame();
        }
        else if (sceneName == "MainMenuScene")
        {
            gameData.lgbPressed = true;
            SceneManager.LoadScene("Level01");
        }
    }

    public void LoadGame()
    {
        //Load PlayerData
        data = SaveSystem.LoadPlayer();

        //Load correct Scene
        if (sceneName != data.sceneName && data.sceneName != "Level01")
        {
            SceneManager.LoadScene(data.sceneName);
        }
        if (sceneName != data.sceneName && data.sceneName == "Level01" && !gameData.lgbPressed)
        {
            //Only happens when first scene is loaded
            foreach (GameObject o in Object.FindObjectsOfType<GameObject>())
            {
                Destroy(o);
            }
            SceneManager.LoadScene(data.sceneName);
        }

        //Set Enemies
        for (int i = 0; i < data.enemyList.Count; i++)
        {
            Enemy enemy = EnemyEmpty.GetChild(i).GetComponent<Enemy>();
            if (!data.enemyList[i])
            {
                enemy.SwitchState(enemy.dead);
                Debug.Log(enemy.currentState + " is dead!");
            }
            enemy.transform.localPosition = new Vector3(data.enemyPosX[i], data.enemyPosY[i], data.enemyPosZ[i]);
        }

        //Set Picked Items
        for (int i = 0; i < data.pickedItems.Count; i++)
        {
            GameObject obj = ItemEmpty.GetChild(i).gameObject;
            if (!data.pickedItems[i])
            {
                obj.SetActive(false);
            }
        }

        //Set Chest Item Strings
        for (int i = 0; i < data.chestItemStringsPerChest.Count; i++)
        {
            chestInventoryList = ChestEmpty.GetChild(i).GetComponent<ChestInventoryList>();
            chestInventoryList._itemStrings.Clear();
            chestInventoryList._items.Clear();
            for (int j = 0; j < data.chestItemStringsPerChest[i].Count; j++)
            {
                string item = data.chestItemStringsPerChest[i][j];
                chestInventoryList._itemStrings.Add(item);
            }
        }

        //Set Chest Items
        StartCoroutine(WaitForAssetLoad());

        //Set Health, Stamina and Mana
        hs.currentHealth = data.health;
        hs.currentStamina = data.stamina;
        hs.currentMana = data.mana;

        health.fillAmount = data.health / hs.maxHealth;
        stamina.fillAmount = data.stamina / hs.maxStamina;
        mana.fillAmount = data.mana / hs.maxMana;

        //Set XP and level
        xpMan.level = data.curLvl;
        xpMan.CalculateEXP();
        xpMan.currentXP = data.curXP;

        //Set Player Position
        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        cc.enabled = false;
        player.transform.position = position;
        cc.enabled = true;


        // Clear Item Lists and load Items
        // TODO: erkennen ob Waffe/Schild gezogen oder nicht
        iil._weaponItems.Clear();
        iil._armorItems.Clear();
        iil._potionItems.Clear();
        iil._bookItems.Clear();
        mmil._DestructionSpells.Clear();
        mmil._RestorationSpells.Clear();
        iil._armorMenuItems.Clear();
        iil._weaponMenuItems.Clear();
        iil._potionMenuItems.Clear();
        iil._bookMenuItems.Clear();
        mmil._DestructionMenuItems.Clear();
        mmil._RestorationMenuItems.Clear();
        iil.RemoveWeaponItems();
        iil.RemoveArmorItems();
        iil.RemovePotionItems();
        iil.RemoveBookItems();
        mmil.RemoveDestructionSpells();

        foreach (string weaponItemString in data.weaponItemStrings)
        {
            Addressables.LoadAssetAsync<WeaponItem>(weaponPath + weaponItemString + ".asset").Completed += OnWeaponItemLoadDone;
        }

        foreach (string armorItemString in data.armorItemStrings)
        {
            Addressables.LoadAssetAsync<ArmorItem>(armorPath + armorItemString + ".asset").Completed += OnArmorItemLoadDone;
        }

        foreach (string potionItemString in data.potionItemStrings)
        {
            Addressables.LoadAssetAsync<PotionItem>(potionPath + potionItemString + ".asset").Completed += OnPotionItemLoadDone;
        }

        foreach (string magicBookItemString in data.magicBookItemStrings)
        {
            Addressables.LoadAssetAsync<MagicBookItem>(magicBooksPath + magicBookItemString + ".asset").Completed += OnMagicBookItemLoadDone;
        }

        foreach (string destructionSpellString in data.destructionSpellItemStrings)
        {
            Addressables.LoadAssetAsync<DestructionSpell>(destructionSpellsPath + destructionSpellString + ".asset").Completed += OnDestructionSpellLoadDone;
        }

        foreach (string restorationSpellString in data.restorationSpellItemStrings)
        {
            Addressables.LoadAssetAsync<RestorationSpell>(restorationSpellsPath + restorationSpellString + ".asset").Completed += OnRestorationSpellLoadDone;
        }
    }

    private void OnWeaponItemLoadDone(AsyncOperationHandle<WeaponItem> obj)
    {
        // In a production environment, you should add exception handling to catch scenarios such as a null result.
        WeaponItem weaponItem;
        weaponItem = obj.Result;
        iil.AddWeaponItem(weaponItem);
        iil.SetWeaponItem(weaponItem);
        if (data.pms && weaponItem.WeaponType == WeaponType.Sword)
        {
            sgs.EquipOneHanded(weaponItem.InstanciableItem);
        }
    }

    private void OnArmorItemLoadDone(AsyncOperationHandle<ArmorItem> obj)
    {
        // In a production environment, you should add exception handling to catch scenarios such as a null result.
        ArmorItem armorItem;
        armorItem = obj.Result;
        iil.AddArmorItem(armorItem);
        iil.SetArmorItem(armorItem);
        if (data.pss && armorItem.ArmorType == ArmorType.Shield)
        {
            sgs.EquipSecondHand(armorItem.InstanciableItem);
        }
    }

    private void OnPotionItemLoadDone(AsyncOperationHandle<PotionItem> obj)
    {
        // In a production environment, you should add exception handling to catch scenarios such as a null result.
        PotionItem potionItem;
        potionItem = obj.Result;
        iil.AddPotionItem(potionItem);
        iil.SetPotionItem(potionItem);
    }

    private void OnMagicBookItemLoadDone(AsyncOperationHandle<MagicBookItem> obj)
    {
        // In a production environment, you should add exception handling to catch scenarios such as a null result.
        MagicBookItem magicBookItem;
        magicBookItem = obj.Result;
        iil.AddMagicBookItem(magicBookItem);
        iil.SetBookItem(magicBookItem);
    }

    private void OnDestructionSpellLoadDone(AsyncOperationHandle<DestructionSpell> obj)
    {
        // In a production environment, you should add exception handling to catch scenarios such as a null result.
        DestructionSpell spell;
        spell = obj.Result;
        mmil.AddDestructionSpell(spell);
        mmil.SetDestructionSpell(spell);
        if (data.pmss && spell.SpellType == SpellType.Ranged)
        {
            sgs.EquipSpell(spell.InstanciableItem);
        }
    }

    private void OnRestorationSpellLoadDone(AsyncOperationHandle<RestorationSpell> obj)
    {
        // In a production environment, you should add exception handling to catch scenarios such as a null result.
        RestorationSpell spell;
        spell = obj.Result;
        mmil.AddRestorationSpell(spell);
        mmil.SetRestorationSpell(spell);
        if (data.pmss)
        {
            sgs.EquipSpell(spell.InstanciableItem);
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (SceneManager.GetActiveScene().name != "MainMenuScene")
        {
            cocMarkerHeading = null;
            cocMarkerHeading = GameObject.Find("COCMarkerHeading");
            EnemyEmpty = GameObject.Find("Enemies").GetComponent<Transform>();
            ItemEmpty = GameObject.Find("Items").GetComponent<Transform>();
            ChestEmpty = GameObject.Find("Chests").GetComponent<Transform>();
            // cc.enabled = false;
            // player.transform.position = cocMarkerHeading.transform.position;
            // cc.enabled = true;
        }

        if (gameData != null)
        {
            if (scene.name != "MainMenuScene" && gameData.lgbPressed)
            {
                LoadGame();
                gameData.lgbPressed = false;
                Destroy(gameData.gameObject);
            }
            if (gameData.ngbPressed)
            {
                hs.currentHealth = hs.maxHealth;
                hs.currentStamina = hs.maxStamina;
                hs.currentMana = hs.maxMana;
                hs.SetMaxHealth(hs.maxHealth);
                hs.SetMaxStamina(hs.maxStamina);
                hs.SetMaxMana(hs.maxMana);
                //gameData.ngbPressed = false;
                Destroy(gameData.gameObject);
            }
        }
        else if (scene.name == "Level01")
        {
            //only for testing
            hs.currentHealth = hs.maxHealth;
            hs.currentStamina = hs.maxStamina;
            hs.currentMana = hs.maxMana;
            hs.SetMaxHealth(hs.maxHealth);
            hs.SetMaxStamina(hs.maxStamina);
            hs.SetMaxMana(hs.maxMana);
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    IEnumerator WaitForAssetLoad()
    {
        int itemCount = 0;
        int typeCount = 0;
        string itemType = "";
        AsyncOperationHandle<ScriptableObject> armorAsset;
        AsyncOperationHandle<ScriptableObject> weaponAsset;
        AsyncOperationHandle<ScriptableObject> potionAsset;
        AsyncOperationHandle<ScriptableObject> magicBookAsset;

        for (int i = 0; i < data.chestItemStringsPerChest.Count;)
        {
            chestInventoryList = ChestEmpty.GetChild(i).GetComponent<ChestInventoryList>();
            foreach (string item in data.chestItemsPerChest[itemCount])
            {
                itemType = data.chestItemTypesPerChest[itemCount][typeCount];
                typeCount++;
                if (itemType == "WeaponItem")
                {
                    weaponAsset = Addressables.LoadAssetAsync<ScriptableObject>(weaponPath + item + ".asset");
                    yield return new WaitUntil(() => weaponAsset.IsDone);
                    if (weaponAsset.IsDone)
                    {
                        chestInventoryList._items.Add(weaponAsset.Result);
                    }
                }
                else if (itemType == "ArmorItem")
                {
                    armorAsset = Addressables.LoadAssetAsync<ScriptableObject>(armorPath + item + ".asset");
                    yield return new WaitUntil(() => armorAsset.IsDone);
                    if (armorAsset.IsDone)
                    {
                        chestInventoryList._items.Add(armorAsset.Result);
                    }
                }
                else if (itemType.Contains("PotionItem"))
                {
                    potionAsset = Addressables.LoadAssetAsync<ScriptableObject>(potionPath + item + ".asset");
                    yield return new WaitUntil(() => potionAsset.IsDone);
                    if (potionAsset.IsDone)
                    {
                        chestInventoryList._items.Add(potionAsset.Result);
                    }
                }
                else if (itemType == "MagicBookItem")
                {
                    magicBookAsset = Addressables.LoadAssetAsync<ScriptableObject>(magicBooksPath + item + ".asset");
                    yield return new WaitUntil(() => magicBookAsset.IsDone);
                    if (magicBookAsset.IsDone)
                    {
                        chestInventoryList._items.Add(magicBookAsset.Result);
                    }
                }
            }
            if (chestInventoryList._items.Count == data.chestItemsPerChest[itemCount].Count)
            {
                itemCount++;
                typeCount = 0;
                i++;
            }
        }
    }
}
