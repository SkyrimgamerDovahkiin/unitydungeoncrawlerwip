using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using TMPro;

public class GameSettings : MonoBehaviour
{
    static string path;
    [SerializeField] GameObject cgb;
    [SerializeField] InputActionAsset inputActions;
    Transform chestEmpty;
    List<ChestInventoryList> chestInventoryList = new List<ChestInventoryList>();
    GameData gameData;
    GameObject winText;

    void Awake()
    {
        path = Application.dataPath + "/StreamingAssets/Binary/Save.sav";
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;
        gameData = GameObject.Find("GameData").GetComponent<GameData>();
  

        if (File.Exists(path))
        {
            cgb.SetActive(true);
        }
        else
        {
            cgb.SetActive(false);
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        chestEmpty = GameObject.Find("Chests").transform;
        if (gameData.ngbPressed)
        {
            gameData.ngbPressed = false;
            foreach (Transform chest in chestEmpty)
            {
                chestInventoryList.Add(chest.GetComponent<ChestInventoryList>());
            }

            if (chestInventoryList.Count > 0)
            {
                for (int i = 0; i < chestInventoryList.Count; i++)
                {
                    chestInventoryList[i].AddRandomItem();
                }
            }
        }

        if (scene.name == "Level01")
            winText = GameObject.Find("YouWon");

        if (scene.name == "Level02")
        {
            winText.GetComponent<TextMeshProUGUI>().text = " Congratulations, you've beaten the demo. More content will come in the next Update.";
            Time.timeScale = 0f;
        }
        else if (scene.name != "Level02")
        {
            winText.GetComponent<TextMeshProUGUI>().text = "";
        }
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
