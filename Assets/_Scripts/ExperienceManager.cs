using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ExperienceManager : MonoBehaviour
{
    Image XPBar;
    TextMeshProUGUI curXPText;
    public int currentXP = 0;
    public int level = 0;
    int XPNeeded = 100;

    void Start()
    {
        XPBar = GetComponent<Image>();
        curXPText = GetComponentInChildren<TextMeshProUGUI>();
    }

    void Update()
    {
        curXPText.text = currentXP.ToString() + " / " + XPNeeded.ToString();
        XPBar.fillAmount = currentXP / (float) XPNeeded;
        if (currentXP == XPNeeded)
        {
            level++;
            currentXP = 0;
            CalculateEXP();
        }
    }

    public int CalculateEXP()
    {
        XPNeeded = 250*level*(level+1);
        return XPNeeded;
    }
}
