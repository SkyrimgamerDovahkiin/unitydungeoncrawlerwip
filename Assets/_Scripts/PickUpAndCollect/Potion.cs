using UnityEngine;

public class Potion : MonoBehaviour, Interactable
{
    public PotionItem potionItem;
    public void Interact(Player player)
    {
        player.AddPotionItemInfo(potionItem);
        DestroyWithSound();
    }

    void DestroyWithSound()
    {
        AudioManager.PlayClipAtPointWithoutPool(potionItem.PickupClipArray[Random.Range(0, potionItem.PickupClipArray.Length)], transform.position);
        //Destroy(gameObject);
        gameObject.SetActive(false);
    }
}

public enum PotionType
{
    Health, Stamina, Mana, Potion, Poison
}
