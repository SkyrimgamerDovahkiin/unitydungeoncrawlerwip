using UnityEngine;

public class Armor : MonoBehaviour, Interactable
{
    public ArmorItem armorItem;

    public void Interact(Player player)
    {
        player.AddArmorItemInfo(armorItem);
        DestroyWithSound();
    }

    void DestroyWithSound()
    {
        AudioManager.PlayClipAtPointWithoutPool(armorItem.PickupClipArray[Random.Range(0, armorItem.PickupClipArray.Length)], transform.position);
        //Destroy(gameObject);
        gameObject.SetActive(false);
    }
}

public enum ArmorClass
{
    Light, Heavy, Medium, Cloth, Jewelery
}

public enum ArmorType
{
    Body, Feet, Hands, Head, Shield
}
