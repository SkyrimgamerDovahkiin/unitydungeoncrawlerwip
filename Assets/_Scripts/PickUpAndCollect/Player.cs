using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
using Ilumisoft.VisualStateMachine;


public class Player : MonoBehaviour
{
    public MagicMenuItemDetail magicMenuItemDetail;
    public MagicMenuItemList magicMenuItemList;
    [SerializeField] StateMachine stateMachine = null;
    [SerializeField] ChestInventoryItemList chestInventoryItemList;
    [SerializeField] Image pickUpImage;
    [SerializeField] GameObject chestInventory;
    [SerializeField] InventoryItemList inventoryItemList;
    [SerializeField] TextMeshProUGUI pickUpText;
    [SerializeField] TextMeshProUGUI pickUpImageText;
    [SerializeField] InputActionAsset inputActions;
    InputAction InteractAction;
    InputAction InventoryCloseAction;
    [HideInInspector] public Camera cam;
    ChestInventoryList chestInventoryList;
    Animator chestAnim;
    Inventory inventory;
    RaycastHit hitInfo;
    [SerializeField] float rayLength = 5f;
    bool opened = false;

    void Start()
    {
        //cam = GetComponentInChildren<Camera>();
        cam = Camera.main;
        inventory = GameObject.Find("Canvas").GetComponent<Inventory>();
        var gameplayActionMap = inputActions.FindActionMap("Player");
        var uiGameActionMap = inputActions.FindActionMap("UI");
        InteractAction = gameplayActionMap.FindAction("Interact");
        InventoryCloseAction = uiGameActionMap.FindAction("InventoryClose");
        InteractAction.Enable();
        InventoryCloseAction.Enable();
    }

    void Update()
    {
        if (hitInfo.collider == null)
        {
            pickUpImageText.enabled = false;
            pickUpImage.enabled = false;
            pickUpText.enabled = false;
        }

        if (cam != null)
        {
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hitInfo, rayLength))
            {
                Interactable interactable = hitInfo.collider.GetComponent<Interactable>();
                if (InteractAction.triggered && interactable != null)
                {
                    interactable.Interact(this);
                }

                PickUpItem();

                if (hitInfo.transform.name.Contains("Truhe") && InteractAction.triggered && !opened)
                {
                    chestInventoryList = hitInfo.transform.gameObject.GetComponent<ChestInventoryList>();
                    chestAnim = hitInfo.transform.GetComponent<Animator>();
                    AudioManager.PlayClipAtPointWithoutPool(chestInventoryList._openClips[Random.Range(0, chestInventoryList._openClips.Length)], hitInfo.transform.position);
                    chestAnim.SetBool("canOpen", true);
                    chestAnim.SetBool("canClose", false);
                    chestInventory.SetActive(true);
                    stateMachine.Trigger("ShowInventoryChestMenu");
                    //chestInventoryItemList.
                    chestInventoryItemList.AddItems(chestInventoryList);
                    Cursor.lockState = CursorLockMode.Confined;
                    Cursor.visible = true;
                    opened = true;
                    Time.timeScale = 0f;
                }


                else if (opened && InventoryCloseAction.triggered)
                {
                    chestInventory.SetActive(false);
                    if (stateMachine.CurrentState == "ChestMenu")
                    {
                        stateMachine.Trigger("ExitInventoryChestMenuChest");
                    }
                    else if (stateMachine.CurrentState == "InventoryChestState")
                    {
                        stateMachine.Trigger("ExitInventoryChestMenuPlayer");
                    }
                    Cursor.visible = false;
                    opened = false;
                    Time.timeScale = 1f;
                    AudioManager.PlayClipAtPointWithoutPool(chestInventoryList._closeClips[Random.Range(0, chestInventoryList._closeClips.Length)], hitInfo.transform.position);
                    chestAnim.SetBool("canClose", true);
                    chestAnim.SetBool("canOpen", false);
                }
            }
        }
    }

    public void PickUpItem()
    {
        while (hitInfo.collider != null)
        {
            pickUpText.enabled = true;
            pickUpImage.enabled = true;
            pickUpImageText.enabled = true;
            
            var bindingIndex = InteractAction.GetBindingIndex(InputBinding.MaskByGroup("Keyboard&Mouse"));
            var textString = InteractAction.GetBindingDisplayString(bindingIndex, InputBinding.DisplayStringOptions.DontIncludeInteractions);
            textString = textString.Substring(0, textString.Length - 1);
            pickUpImageText.text = textString;
            //pickUpImageText.text = InteractAction.GetBindingDisplayString(bindingIndex);


            if (hitInfo.transform.GetComponent<Weapon>())
            {
                pickUpText.text = "      Nehmen \n" + hitInfo.transform.GetComponent<Weapon>().weaponItem.label;
            }
            else if (hitInfo.transform.GetComponent<Armor>())
            {
                pickUpText.text = "      Nehmen \n" + hitInfo.transform.GetComponent<Armor>().armorItem.label;
            }
            else if (hitInfo.transform.GetComponent<Potion>())
            {
                pickUpText.text = "      Nehmen \n" + hitInfo.transform.GetComponent<Potion>().potionItem.label;
            }
            else if (hitInfo.transform.GetComponent<MagicBook>())
            {
                pickUpText.text = "      Nehmen \n" + hitInfo.transform.GetComponent<MagicBook>().magicBookItem.label;
            }
            else if (hitInfo.transform.GetComponent<Portal>())
            {
                pickUpText.text = " " + hitInfo.transform.GetComponent<Portal>().SceneName;
            }
            else if (hitInfo.transform.name.Contains("Truhe"))
            {
                pickUpText.text = "     Öffnen \n      Truhe";
            }
            else
            {
                pickUpText.text = "";
                pickUpImageText.text = "";
                pickUpImage.enabled = false;
            }
            return;
        }
    }

    //Passiert, wenn Player aufsammelt
    public void AddWeaponItemInfo(WeaponItem weaponItem)
    {
        inventoryItemList.AddWeaponItem(weaponItem);
        inventoryItemList.SetWeaponItem(weaponItem);
    }

    public void AddArmorItemInfo(ArmorItem armorItem)
    {
        inventoryItemList.AddArmorItem(armorItem);
        inventoryItemList.SetArmorItem(armorItem);
    }

    public void AddPotionItemInfo(PotionItem potionItem)
    {
        inventoryItemList.AddPotionItem(potionItem);
        inventoryItemList.SetPotionItem(potionItem);
    }

    public void AddMagicBookItemInfo(MagicBookItem magicBookItem)
    {
        inventoryItemList.AddMagicBookItem(magicBookItem);
        inventoryItemList.SetBookItem(magicBookItem);
    }

    public void AddDestructionSpellInfo(DestructionSpell destructionSpell)
    {
        magicMenuItemList.AddDestructionSpell(destructionSpell);
        magicMenuItemList.SetDestructionSpell(destructionSpell);
    }

    public void AddRestorationSpellInfo(RestorationSpell restorationSpell)
    {
        magicMenuItemList.AddRestorationSpell(restorationSpell);
        magicMenuItemList.SetRestorationSpell(restorationSpell);
    }
}
