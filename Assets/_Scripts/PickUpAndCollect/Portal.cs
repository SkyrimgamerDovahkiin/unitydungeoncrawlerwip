using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour, Interactable
{
    public string SceneName;
    int currentSceneIndex;
    GameObject player;
    CharacterController cc;
    

    void Start()
    {
        player = GameObject.Find("Player");
        cc = player.GetComponent<CharacterController>();
    }

    public void Interact(Player player)
    {
        SceneManager.LoadScene(SceneName);
        cc.enabled = false;
        cc.enabled = true;
    }
}
