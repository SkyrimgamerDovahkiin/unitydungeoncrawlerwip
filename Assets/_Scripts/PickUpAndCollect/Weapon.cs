using UnityEngine;

public class Weapon : MonoBehaviour, Interactable
{
    public WeaponItem weaponItem;
    AudioSource source;

    public void Interact(Player player)
    {
        player.AddWeaponItemInfo(weaponItem);
        DestroyWithSound();
    }

    void DestroyWithSound()
    {
        AudioManager.PlayClipAtPointWithoutPool(weaponItem.PickupClipArray[Random.Range(0, weaponItem.PickupClipArray.Length)], transform.position);
        //Destroy(gameObject);
        gameObject.SetActive(false);
    }
}

public enum ItemType
{
    Apple, Gold, Mushroom, Shortsword, Shield
}

public enum WeaponClass
{
    Onehanded, Twohanded, Bow, Arrow, Crossbow, Bolt
}

public enum WeaponType
{
    Sword, Axe, Bow, Arrow, Crossbow, Bolt, Dagger, WarAxe
}
