using UnityEngine;

public class MagicBook : MonoBehaviour, Interactable
{
    public MagicBookItem magicBookItem;

    public void Interact(Player player)
    {
        player.AddMagicBookItemInfo(magicBookItem);
        DestroyWithSound();
    }

    void DestroyWithSound()
    {
        AudioManager.PlayClipAtPointWithoutPool(magicBookItem.PickupClipArray[Random.Range(0, magicBookItem.PickupClipArray.Length)], transform.position);
        //Destroy(gameObject);
        gameObject.SetActive(false);
    }
}

public enum SpellClass
{
    Destruction, Conjuration, Restoration
}
