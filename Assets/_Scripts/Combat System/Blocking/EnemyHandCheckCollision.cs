using UnityEngine;

public class EnemyHandCheckCollision : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] HealthSystem healthSystem;
    [SerializeField] Enemy enemy;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Player not blocking!");
            healthSystem.TakeDamage(enemy.damage);
        }
        if (other.CompareTag("Shield"))
        {
            Debug.Log("Player blocking!");
            int blockingValue = other.GetComponent<ArmorData>().armor.armor;
            healthSystem.TakeDamage(enemy.damage - blockingValue);
            animator.SetBool("canStagger", true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (animator.GetBool("canStagger"))
        {
            animator.SetBool("canStagger", false);
        }
    }
}
