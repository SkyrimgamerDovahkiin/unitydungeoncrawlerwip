using UnityEngine;

public class EnemyActivateColliders : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] Collider leftHand;
    [SerializeField] Collider rightHand;

    void ActivateLeftHandCollider()
    {
        leftHand.enabled = true;
    }

    void ActivateRightHandCollider()
    {
        rightHand.enabled = true;
    }

    void ActivateBothHandsCollider()
    {
        leftHand.enabled = true;
        rightHand.enabled = true;
    }

    void DeactivateLeftHandCollider()
    {
        leftHand.enabled = false;
    }

    void DeactivateRightHandCollider()
    {
        rightHand.enabled = false;
    }

    void DeactivateBothHandsCollider()
    {
        leftHand.enabled = false;
        rightHand.enabled = false;
    }
}
