using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthbar : MonoBehaviour
{
    [SerializeField] Image healthImage;
    public Enemy enemy;

    void Update()
    {
        if (enemy == null) { return; }

        healthImage.fillAmount = enemy.CalculateHealth(enemy.currentHealth, enemy.maxHealth);
        if (enemy.currentHealth <= 0 || enemy.currentState == enemy.dead || enemy.currentState == enemy.wander)
        {
            healthImage.transform.parent.gameObject.SetActive(false);
        }
        else if (enemy.currentHealth > 0 || enemy.currentState == enemy.attackState)
        {
            healthImage.transform.parent.gameObject.SetActive(true);
        }
    }
}
