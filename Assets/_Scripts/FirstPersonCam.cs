using UnityEngine;
using UnityEngine.InputSystem;

public class FirstPersonCam : MonoBehaviour
{
    [SerializeField] InputActionAsset inputActions;
    InputActionMap gameplayActionMap;
    InputAction ZoomAction;
    InputAction RotateAction;
    public Vector3 offset = Vector3.zero;
    public Vector3 rotationOffset = Vector3.zero;
    public Transform target;
    float yRot;
    public float yRotSensitivity = 0.5f;

    [Tooltip("Camera pitch limitations")]
    public LimitsInfo PitchLimits = new LimitsInfo { Minimum = -60.0f, Maximum = 60.0f };

    [System.Serializable]
    public struct LimitsInfo
    {
        [Tooltip("Minimum pitch angle, in the range [-90, Maximum]")]
        public float Minimum;

        [Tooltip("Maximum pitch angle, in the range [Minimum, 90]")]
        public float Maximum;
    }

    void Awake()
    {
        gameplayActionMap = gameplayActionMap = inputActions.FindActionMap("Player");
        ZoomAction = gameplayActionMap.FindAction("Zoom");
        RotateAction = gameplayActionMap.FindAction("Look");
        gameplayActionMap.Enable();
    }

    void LateUpdate()
    {
        if (target == null) { return; }

        if (Time.timeScale > 0f)
        {
            Rotate(target);
        }
        Move(target);
    }

    public bool Zoom()
    {
        // //GET ZOOM INPUT AND APPLY TO THE CURRENT DISTANCE
        if (ZoomAction != null)
        {
            float zoom = ZoomAction.ReadValue<Vector2>().y;
            if (Mathf.Sign(zoom) == -1f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    void Move(Transform targetTransform)
    {
        transform.position = targetTransform.position + offset;
    }

    void Rotate(Transform targetTransform)
    {
        //SET THE VALUES TO GET LATER. QUATERNION IS NEEDED BECAUSE THE ENTITY ROTATION IS A QUATERNION
        Vector3 MouseMoveVector = RotateAction.ReadValue<Vector2>();
        Vector3 pitchYawVector = Vector3.zero;
        Quaternion rotate = Quaternion.identity;
        yRot -= MouseMoveVector.y;
        
        //SET THE PITCH AND CLAMP IT, SO IT DOESEN'T MOVE AROUND THE PLAYER
        float _pitch = yRot * yRotSensitivity;
        _pitch = Mathf.Clamp(_pitch, PitchLimits.Minimum, PitchLimits.Maximum);

        if (yRot < PitchLimits.Minimum && Mathf.Sign(RotateAction.ReadValue<Vector2>().y) == -1f)
        {
            Debug.Log("is Min!");
            yRot = PitchLimits.Minimum;
        }
        else if (yRot > PitchLimits.Maximum && System.MathF.Sign(RotateAction.ReadValue<Vector2>().y) == 1f)
        {
            Debug.Log("is Max!");
            yRot = PitchLimits.Maximum;
        }

        //PUT THE VALUES IN THE ROTATE QUATERNION/THE VECTOR, THEN ROTATE THE CAMERA 
        Vector3 rotationTest = new Vector3(0f, targetTransform.rotation.eulerAngles.y, 0f);
        pitchYawVector = rotationTest;//transform.rotation.eulerAngles;//rotate.eulerAngles;
        pitchYawVector.x = _pitch;
        transform.localEulerAngles = pitchYawVector + rotationOffset;
    }
}
