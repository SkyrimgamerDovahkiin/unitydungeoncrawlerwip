using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public class BrightnessSettings : MonoBehaviour
{
    [SerializeField] Volume volume;
    [SerializeField] Slider brightnessSlider;

    public void ChangeMainLight()
    {
        VolumeProfile profile = volume.sharedProfile;
        if (profile.TryGet<GradientSky>(out GradientSky gradientSky))
        {
            gradientSky.exposure.value = brightnessSlider.value;
        }
    }
}
