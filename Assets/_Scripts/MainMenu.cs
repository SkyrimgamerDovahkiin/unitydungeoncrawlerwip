using UnityEngine;
using Ilumisoft.VisualStateMachine;
using UnityEngine.InputSystem;

public class MainMenu : MonoBehaviour
{
    [SerializeField] StateMachine stateMachine = null;
    [SerializeField] InputActionAsset inputActions;
    InputAction MainMenuAction;
    bool keyPressed = false;

    void Start()
    {
        var gameplayActionMap = inputActions.FindActionMap("UI");
        MainMenuAction = gameplayActionMap.FindAction("MainMenu");
        MainMenuAction.Enable();
    }

    void Update()
    {
        if (MainMenuAction.triggered && !keyPressed)
        {
            keyPressed = true;
            stateMachine.Trigger("ShowMainMenu");
        }

        else if (MainMenuAction.triggered && keyPressed)
        {
            keyPressed = false;
            stateMachine.Trigger("ExitMainMenu");
        }
    }

    public void MenuOnOnStateEnter()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        Time.timeScale = 0f;
    }

    public void MenuOffOnStateEnter()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1f;
    }

    public void MenuOffOnStateExit()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        Time.timeScale = 0f;
    }
}
