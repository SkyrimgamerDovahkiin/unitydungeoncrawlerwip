using UnityEngine;

public class MapCamera : MonoBehaviour
{
    public Transform player;

    public void OpenMap()
    {
        if (player != null)
        {
            Vector3 newPos = new Vector3(player.position.x, transform.position.y, player.position.z);
            transform.position = newPos;
        }
    }
}
