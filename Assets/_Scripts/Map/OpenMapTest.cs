using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;

public class OpenMapTest : MonoBehaviour
{
    public InputActionAsset inputActions;
    public InputAction MapAction;
    public MapCamera mapCamera;
    bool mapKeyPressed = false;
    public GameObject map;

    void Start()
    {
        var gameplayActionMap = inputActions.FindActionMap("UI");
        MapAction = gameplayActionMap.FindAction("Map");
        MapAction.Enable();
    }

    void Update()
    {
        if (MapAction.triggered && !mapKeyPressed)
        {
            mapCamera.OpenMap();
            map.SetActive(true);
            mapKeyPressed = true;
            Time.timeScale = 0f;
        }
        else if (MapAction.triggered && mapKeyPressed)
        {
            Time.timeScale = 1f;
            map.SetActive(false);
            mapKeyPressed = false;
        }
    }
}
