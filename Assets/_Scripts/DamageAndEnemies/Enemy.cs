using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    [SerializeField] protected int experience = 20;
    [SerializeField] protected ExperienceManager xpManager;
    public EnemyCheckCollision checkCollision;
    public Animator animator;
    public Transform playerTarget;
    public WalkWaypoints walkWaypoints;
    public NavMeshAgent agent;
    public EnemyState currentState;
    public EnemyState attackState;
    public EnemyState wander;
    public EnemyState dead;
    [HideInInspector] public Rigidbody[] rigidbodies;
    [HideInInspector] public Collider[] colliders;
    [HideInInspector] public Joint[] joints;
    [HideInInspector] public LayerMask playerMask;
    [Range(0, 360)]
    public float viewAngle = 45;
    public float viewDistance = 5f;
    public float followDistance = 40f;
    public float awareDistance = 1;
    public float rotationSpeed = 1f;
    public int maxHealth = 20;
    public int currentHealth;
    public int WeaponResistance = 4;
    public int damage = 5;
    [HideInInspector] public int playerLayer = 1 << 8;
    [HideInInspector] public bool giveXP = false;
    [HideInInspector] public bool interuptState = false;

    void Awake()
    {
        currentHealth = maxHealth;
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        colliders = GetComponentsInChildren<Collider>();
        joints = GetComponentsInChildren<Joint>();
        attackState = new AttackState(this);
        wander = new Wander(this);
        dead = new Dead(this);
        currentState = wander;
        currentState.OnStateEnter();
    }

    void Update()
    {
        currentState.OnUpdate();
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            currentState = dead;
            currentState.OnStateEnter();
        }
        else if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    public void SwitchState(EnemyState state)
    {
        currentState.OnStateExit();
        currentState = state;
        currentState.OnStateEnter();
    }

    public void EnableRagdoll()
    {
        foreach (Rigidbody rigidbody in rigidbodies)
        {
            rigidbody.isKinematic = false;
        }
        foreach (Collider collider in colliders)
        {
            if (collider.gameObject.CompareTag("EnemyDamage"))
            {
                collider.enabled = false;
            }
        }
        //eventuell joints disablen
        /*foreach (Joint joint in joints)
        {
        }*/
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0f, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    public float CalculateHealth(float currentHealth, float maxHealth)
    {
        return currentHealth / maxHealth;
    }

    //Method to override from each enemy to check the resistance/weakness and apply the correct magic damage
    public virtual void CheckMagicDamage(ProjectileCheckCollision projectile) {}
}
