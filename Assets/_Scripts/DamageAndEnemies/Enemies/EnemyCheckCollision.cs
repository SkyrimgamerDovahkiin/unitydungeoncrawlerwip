using UnityEngine;

//checks the enemy collision with a weapon/with magic and applies the correct damage
public class EnemyCheckCollision : MonoBehaviour
{
    [SerializeField] EnemyHealthbar enemyHealthbar;
    [SerializeField] Enemy enemy;
    public bool beenHit { get; private set; } = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Magic") && !beenHit)
        {
            //Debug.Log("been Hit with Magic!");
            var projectile = other.GetComponent<ProjectileCheckCollision>();
            enemyHealthbar.enemy = enemy;
            enemy.CheckMagicDamage(projectile);
            beenHit = true;
        }
        else if (other.CompareTag("PlayerWeapon") && !beenHit)
        {
            //Debug.Log("been Hit with a Weapon!");
            var weaponItem = other.GetComponentInChildren<WeaponData>().weaponItem;
            enemyHealthbar.enemy = enemy;
            enemy.currentHealth -= (weaponItem.damage - enemy.WeaponResistance);
            beenHit = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (beenHit)
        {
            beenHit = false;
        }
    }
}
