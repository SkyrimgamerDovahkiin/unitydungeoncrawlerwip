using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class WaterGolem : Enemy
{
    public int FrostResistance = 4;
    public int FireWeakness = 3;

    void Awake()
    {
        currentHealth = maxHealth;
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        colliders = GetComponentsInChildren<Collider>();
        joints = GetComponentsInChildren<Joint>();
        attackState = new AttackState(this);
        wander = new Wander(this);
        dead = new Dead(this);
        currentState = wander;
        currentState.OnStateEnter();
    }

    void Update()
    {
        currentState.OnUpdate();
        if (currentHealth <= 0)
        {
            if (!giveXP)
            {
                xpManager.currentXP += experience;
                giveXP = true;
            }
            currentHealth = 0;
            currentState = dead;
            currentState.OnStateEnter();
        }
        else if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    void OnDrawGizmosSelected()
    {

        var nav = agent;
        if (nav == null || nav.path == null)
            return;

        var line = gameObject.GetComponent<LineRenderer>();
        if (line == null)
        {
            line = gameObject.AddComponent<LineRenderer>();
            line.material = new Material(Shader.Find("Sprites/Default")) { color = Color.yellow };
            line.startWidth = 0.5f;
            line.endWidth = 0.5f;
            line.startColor = Color.yellow;
            line.endColor = Color.yellow;
        }

        var path = nav.path;

        //line.positionCount = path.corners.Length;
        line.SetPositions(path.corners);

        for (int i = 0; i < path.corners.Length; i++)
        {
            line.SetPosition(i, path.corners[i]);
        }

    }

    public override void CheckMagicDamage(ProjectileCheckCollision projectile)
    {
        Debug.Log("Water Golem!");
        if (projectile.projectile.destructionSpellClass == DestructionSpellClass.Fire)
        {
            currentHealth -= (projectile.projectile.damage + FireWeakness);
        }
        else if (projectile.projectile.destructionSpellClass == DestructionSpellClass.Frost)
        {
            currentHealth -= (projectile.projectile.damage - FrostResistance);
        }
    }
}
