using UnityEngine;

public class ArmorData : MonoBehaviour
{
    public ArmorItem armor;

    public void ActivateBlockCollider()
    {
        Collider collider = GetComponent<Collider>();
        collider.enabled = true;
    }

    public void DeactivateBlockCollider()
    {
        Collider collider = GetComponent<Collider>();
        collider.enabled = false;
    }
}
