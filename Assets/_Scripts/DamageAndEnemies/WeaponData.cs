using System.Collections;
using UnityEngine;

public class WeaponData : MonoBehaviour
{
    public WeaponItem weaponItem;
    AudioSource source;
    HealthSystem healthSystem;

    void Awake() 
    {
        healthSystem = GameObject.Find("Canvas").GetComponent<HealthSystem>();
    }

    public void PlayAttackClipAtPoint(Transform transform)
    {
        healthSystem.RemoveStamina(weaponItem.StaminaToRemove);
        int index = Random.Range (0, weaponItem.FightingSoundClipArray.Length);
        AudioClip randomClip = weaponItem.FightingSoundClipArray[index];
        source = AudioManager.PlayClipAtPoint(randomClip, transform.position);
        StartCoroutine(WaitForAttackSoundToFinish(source));
    }

    public void PlayDrawClipAtPoint(Transform transform)
    {
        int index = Random.Range (0, weaponItem.DrawSoundClipArray.Length);
        AudioClip randomClip = weaponItem.DrawSoundClipArray[index];
        source = AudioManager.PlayClipAtPoint(randomClip, transform.position);
        StartCoroutine(WaitForDrawSoundToFinish(source));
    }

    public void PlaySheatheClipAtPoint(Transform transform)
    {
        int index = Random.Range (0, weaponItem.SheatheSoundClipArray.Length);
        AudioClip randomClip = weaponItem.SheatheSoundClipArray[index];
        source = AudioManager.PlayClipAtPoint(randomClip, transform.position);
        StartCoroutine(WaitForSheatheSoundToFinish(source));
    }

    IEnumerator WaitForAttackSoundToFinish(AudioSource source)
    {
        yield return new WaitForSeconds(source.clip.length);
        AudioManager.ReturnSourceToPool(source);
    }

    IEnumerator WaitForDrawSoundToFinish(AudioSource source)
    {
        yield return new WaitForSeconds(source.clip.length);
        AudioManager.ReturnSourceToPool(source);
    }

    IEnumerator WaitForSheatheSoundToFinish(AudioSource source)
    {
        yield return new WaitForSeconds(source.clip.length);
        AudioManager.ReturnSourceToPool(source);
    }
}
