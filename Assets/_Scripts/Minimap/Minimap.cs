using UnityEngine;
using UnityEngine.UI;

public class Minimap : MonoBehaviour
{
    public Transform player;

    private void LateUpdate()
    {
        if (player == null) { return; }
        
        Vector3 newPos = new Vector3(player.position.x, transform.position.y, player.position.z);
        transform.position = newPos;
        transform.rotation = Quaternion.Euler(90f, player.eulerAngles.y, 0f);
    }
}
