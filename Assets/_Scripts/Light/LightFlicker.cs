using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    float minIntensity;
    public float maxIntensity = 1f;
    Light Light;

    [Range(1, 50)]
    public int smoothing = 5;
    Queue<float> smoothQueue;
    float lastSum = 0;


    public void Reset()
    {
        smoothQueue.Clear();
        lastSum = 0;
    }

    void Start()
    {
        Light = GetComponent<Light>();
        minIntensity = Light.intensity;
        smoothQueue = new Queue<float>(smoothing);
    }

    void Update()
    {
        if (Light == null)
            return;

        // pop off an item if too big
        while (smoothQueue.Count >= smoothing)
        {
            lastSum -= smoothQueue.Dequeue();
        }

        // Generate random new item, calculate new average
        float newVal = Random.Range(minIntensity, maxIntensity);
        smoothQueue.Enqueue(newVal);
        lastSum += newVal;

        // Calculate new smoothed average
        Light.intensity = lastSum / (float)smoothQueue.Count;
    }
}

