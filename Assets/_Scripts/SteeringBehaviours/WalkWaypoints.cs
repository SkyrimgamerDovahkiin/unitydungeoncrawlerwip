using System.Collections.Generic;
using UnityEngine;

public class WalkWaypoints : MonoBehaviour
{
    [SerializeField] Transform waypointParent;
    public float arriveDist = 0.3f;
    List<Vector3> waypoints;
    public Vector3 curWaypoint { get; private set;}
    int wpCounter;


    void Awake()
    {
        waypoints = new List<Vector3>(waypointParent.childCount);
        for (int i = 0; i < waypointParent.childCount; i++)
        {
            waypoints.Add(waypointParent.GetChild(i).position);
        }
        waypoints.Shuffle();
        curWaypoint = waypoints[0];
    }

    public void GetRandomWaypoint()
    {
        curWaypoint = waypoints[wpCounter];
        wpCounter++;
        if(wpCounter>= waypoints.Count)
        {
            waypoints.Shuffle();
            wpCounter = 0;
        }
    }
}
