using System.Collections;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    [SerializeField] float rotSpeed;
    [SerializeField] Transform camHolderTransform;
    [SerializeField] float camRotDuration = 0.5f;
    [SerializeField] Transform meshTransform;
    CharacterController cc;
    float gravity = 9.81f;
    float vSpeed;
    CharState charState;
    float camRotAmount;

    void Awake()
    {
        cc = GetComponent<CharacterController>();
    }

    void Update()
    {
        switch (charState)
        {
            case CharState.Normal:
                Move();
                Gravity();
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    camRotAmount = -90f;
                    charState = CharState.StartRotation;
                }
                else if (Input.GetKeyDown(KeyCode.E))
                {
                    camRotAmount = 90f;
                    charState = CharState.StartRotation;
                }
                break;
            case CharState.StartRotation:
                Gravity();
                StartCoroutine(RotateCamCO(camRotAmount));
                charState = CharState.Rotating;
                break;
            case CharState.Rotating:
                Gravity();
                break;
            default:
                break;
        }
    }

    void Gravity()
    {
        if (cc.isGrounded)
        {
            vSpeed = 0f;
        }
        vSpeed += gravity * Time.deltaTime;
        cc.Move(Vector3.down * vSpeed * Time.deltaTime);
    }

    void Move()
    {
        Vector3 forward = camHolderTransform.forward;
        Vector3 right = camHolderTransform.right;
        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 moveDir = forward * vertical + right * horizontal;
        cc.Move(moveDir * moveSpeed * Time.deltaTime);
        if (moveDir != Vector3.zero)
        {
            Quaternion toRot = Quaternion.LookRotation(moveDir, Vector3.up);
            meshTransform.rotation = Quaternion.RotateTowards(
                meshTransform.rotation,
                toRot,
                rotSpeed * Time.deltaTime);
        }
    }

    IEnumerator RotateCamCO(float amount)
    {
        float timer = 0f;
        float yRotStart = camHolderTransform.localEulerAngles.y;
        float yRotEnd = yRotStart + amount;
        while (timer < camRotDuration)
        {
            float yRotCur = Mathf.Lerp(yRotStart, yRotEnd, timer / camRotDuration);
            camHolderTransform.localEulerAngles = new Vector3(0f, yRotCur, 0f);
            timer += Time.deltaTime;
            yield return null;
        }
        camHolderTransform.localEulerAngles = new Vector3(0f, yRotEnd % 360, 0f);
        charState = CharState.Normal;
    }

}

public enum CharState
{
    Normal, StartRotation, Rotating
}
