using System.Collections.Generic;
using UnityEngine;

public class CharSound : MonoBehaviour
{
    [SerializeField] List<AudioClip> footsteps = new List<AudioClip>();
    [SerializeField] float minFsDelay = 0.2f;
    [SerializeField] float maxFsDelay = 1.5f;
    [SerializeField] AnimationCurve volumeTempoCurve;
    float footstepTimer;
    int footstepCounter;
    AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    public void UpdateFootsteps(float walkSpeed)
    {
        if (walkSpeed < 0.08f) { return; }

        source.volume = volumeTempoCurve.Evaluate(walkSpeed);
        float footstepDelay = Mathf.Lerp(maxFsDelay, minFsDelay, walkSpeed);

        if (footstepTimer == 0f)
        {
            source.clip = footsteps[footstepCounter];
            source.Play();
            footstepCounter++;
        }
        else if (footstepTimer >= footstepDelay)
        {
            footstepTimer = 0f;
            footstepCounter++;
            if (footstepCounter >= footsteps.Count)
            {
                footstepCounter = 0;
                footsteps.Shuffle();
            }
            source.clip = footsteps[footstepCounter];
            source.Play();
        }
        footstepTimer += Time.deltaTime;
    }
}
