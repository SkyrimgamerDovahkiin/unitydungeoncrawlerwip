using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PlayerData
{
    public float stamina;
    public float health;
    public float mana;
    public float[] position;
    public bool pms;
    public bool pmss;
    public bool pss;
    public List<string> weaponItemStrings = new List<string>();
    public List<string> armorItemStrings = new List<string>();
    public List<string> potionItemStrings = new List<string>();
    public List<string> magicBookItemStrings = new List<string>();
    public List<string> destructionSpellItemStrings = new List<string>();
    public List<string> restorationSpellItemStrings = new List<string>();
    public string sceneName;
    public List<bool> enemyList = new List<bool>();
    public List<float> enemyPosX = new List<float>();
    public List<float> enemyPosY = new List<float>();
    public List<float> enemyPosZ = new List<float>();
    public List<bool> pickedItems = new List<bool>();
    public List<List<string>> chestItemStringsPerChest = new List<List<string>>();
    public List<List<string>> chestItemsPerChest = new List<List<string>>();
    public List<List<string>> chestItemTypesPerChest = new List<List<string>>();
    public int curXP;
    public int curLvl;

    public PlayerData(HealthSystem hs, FirstPersonController fpc, InventoryItemList iil, MagicMenuItemList mmil, SimpleGrabSystem sgs, ExperienceManager xpMan)
    {
        sceneName = SceneManager.GetActiveScene().name;
        curXP = xpMan.currentXP;
        curLvl = xpMan.level;

        Transform enemyEmpty = GameObject.Find("Enemies").GetComponent<Transform>();
        foreach (Transform enemy in enemyEmpty)
        {
            if (enemy.GetComponent<Animator>().enabled)
            {
                enemyList.Add(true);
            }
            else if (!enemy.GetComponent<Animator>().enabled)
            {
                enemyList.Add(false);
            }
            enemyPosX.Add(enemy.transform.localPosition.x);
            enemyPosY.Add(enemy.transform.localPosition.y);
            enemyPosZ.Add(enemy.transform.localPosition.z);
        }

        Transform itemEmpty = GameObject.Find("Items").GetComponent<Transform>();
        foreach (Transform item in itemEmpty)
        {
            if (item.gameObject.activeSelf)
            {
                pickedItems.Add(true);
            }
            else if (!item.gameObject.activeSelf)
            {
                pickedItems.Add(false);
            }
        }

        //save chest Items
        Transform chestEmpty = GameObject.Find("Chests").GetComponent<Transform>();
        List<ChestInventoryList> chestInventoryLists = new List<ChestInventoryList>();
        foreach (Transform chest in chestEmpty)
        {
            chestInventoryLists.Add(chest.GetComponent<ChestInventoryList>());
        }
        for (int i = 0; i < chestInventoryLists.Count; i++)
        {
            List<string> chestItemStrings = new List<string>();
            List<string> chestItems = new List<string>();
            List<string> chestItemTypes = new List<string>();
            for (int j = 0; j < chestInventoryLists[i]._itemStrings.Count; j++)
            {
                chestItemStrings.Add(chestInventoryLists[i]._itemStrings[j]);
            }
            chestItemStringsPerChest.Add(chestItemStrings);
            for (int j = 0; j < chestInventoryLists[i]._items.Count; j++)
            {
                chestItems.Add(chestInventoryLists[i]._items[j].name);
                chestItemTypes.Add(chestInventoryLists[i]._items[j].GetType().ToString());
            }
            chestItemsPerChest.Add(chestItems);
            chestItemTypesPerChest.Add(chestItemTypes);
            //chestItemStrings.Clear();
            //chestItems.Clear();
            //Debug.Log(chestItemStringsPerChest[i].Count + "LOL!");
        }

        stamina = hs.currentStamina;
        health = hs.currentHealth;
        mana = hs.currentMana;

        position = new float[3];
        position[0] = fpc.transform.position.x;
        position[1] = fpc.transform.position.y;
        position[2] = fpc.transform.position.z;

        pms = sgs.isPickedMainSlot;
        pmss = sgs.isPickedMainSlotSpell;
        pss = sgs.isPickedSecondSlot;

        foreach (WeaponItem item in iil._weaponItems)
        {
            weaponItemStrings.Add(item.label);
        }
        foreach (ArmorItem item in iil._armorItems)
        {
            armorItemStrings.Add(item.label);
        }
        foreach (PotionItem item in iil._potionItems)
        {
            potionItemStrings.Add(item.label);
        }
        foreach (MagicBookItem item in iil._bookItems)
        {
            magicBookItemStrings.Add(item.label);
        }
        foreach (DestructionSpell item in mmil._DestructionSpells)
        {
            destructionSpellItemStrings.Add(item.label);
        }
        foreach (RestorationSpell item in mmil._RestorationSpells)
        {
            restorationSpellItemStrings.Add(item.label);
        }
    }
}
