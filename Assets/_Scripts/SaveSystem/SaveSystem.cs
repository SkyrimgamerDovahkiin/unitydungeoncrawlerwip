using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    static BinaryFormatter formatter = new BinaryFormatter();
    static string path = Application.dataPath + "/StreamingAssets/Binary/Save.sav";
    public static PlayerData data;

    public static void SavePlayer(HealthSystem hs, FirstPersonController fpc, InventoryItemList iil, MagicMenuItemList mmil, SimpleGrabSystem sgs, ExperienceManager xpMan)
    {
        data = new PlayerData(hs, fpc, iil, mmil, sgs, xpMan);

        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            formatter.Serialize(stream, data);
        }
    }

    public static PlayerData LoadPlayer()
    {
        if (File.Exists(path))
        {
            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                return formatter.Deserialize(stream) as PlayerData;
            }
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}
