using System.Collections;
using UnityEngine;
using TMPro;

public class TextFade : MonoBehaviour
{
    [SerializeField] float FadeSpeed = 1.0F;
    [SerializeField] int RolloverCharacterSpread = 10;
    [SerializeField] Color ColorTint;
    public bool isRunning {get; private set;}
    TextMeshProUGUI myText;

    void Awake()
    {
        myText = GetComponent<TextMeshProUGUI>();
    }


public IEnumerator AnimateVertexColors()
{
    isRunning = true;
    myText.enabled = true;
    yield return new WaitForSecondsRealtime(2f);
    // Need to force the text object to be generated so we have valid data to work with right from the start.
    myText.ForceMeshUpdate();
    TMP_TextInfo textInfo = myText.textInfo;
    Color32[] newVertexColors;
    int currentCharacter = 0;
    int startingCharacterRange = currentCharacter;
    bool isRangeMax = false;
    while (!isRangeMax)
    {
        int characterCount = textInfo.characterCount;
        // Spread should not exceed the number of characters.
        byte fadeSteps = (byte)Mathf.Max(1, 255 / RolloverCharacterSpread);
        for (int i = startingCharacterRange; i < currentCharacter + 1; i++)
        {
            // Skip characters that are not visible
            if (!textInfo.characterInfo[i].isVisible) continue;
            // Get the index of the material used by the current character.
            int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;
            // Get the vertex colors of the mesh used by this text element (character or sprite).
            newVertexColors = textInfo.meshInfo[materialIndex].colors32;
            // Get the index of the first vertex used by this text element.
            int vertexIndex = textInfo.characterInfo[i].vertexIndex;
            // Get the current character's alpha value.
            byte alpha = (byte)Mathf.Clamp(newVertexColors[vertexIndex + 0].a - fadeSteps, 0, 255);
            // Set new alpha values.
            newVertexColors[vertexIndex + 0].a = alpha;
            newVertexColors[vertexIndex + 1].a = alpha;
            newVertexColors[vertexIndex + 2].a = alpha;
            newVertexColors[vertexIndex + 3].a = alpha;
            // Tint vertex colors
            // Note: Vertex colors are Color32 so we need to cast to Color to multiply with tint which is Color.
            newVertexColors[vertexIndex + 0] = (Color)newVertexColors[vertexIndex + 0] * ColorTint;
            newVertexColors[vertexIndex + 1] = (Color)newVertexColors[vertexIndex + 1] * ColorTint;
            newVertexColors[vertexIndex + 2] = (Color)newVertexColors[vertexIndex + 2] * ColorTint;
            newVertexColors[vertexIndex + 3] = (Color)newVertexColors[vertexIndex + 3] * ColorTint;
            if (alpha == 0)
            {
                startingCharacterRange += 1;
                if (startingCharacterRange == characterCount)
                {
                    // Update mesh vertex data one last time.
                    myText.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
                    yield return new WaitForSecondsRealtime(1.0f);
                    // Reset the text object back to original state.
                    myText.ForceMeshUpdate();
                    myText.enabled = false;
                    yield return new WaitForSecondsRealtime(1.0f);
                    // Reset our counters.
                    currentCharacter = 0;
                    startingCharacterRange = 0;
                    isRangeMax = true; // Would end the coroutine.
                }
            }
        }
        // Upload the changed vertex colors to the Mesh.
        myText.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
        if (currentCharacter + 1 < characterCount) currentCharacter += 1;
        yield return new WaitForSecondsRealtime(0.25f - FadeSpeed * 0.01f);
    }
    isRunning = false;
}
}

