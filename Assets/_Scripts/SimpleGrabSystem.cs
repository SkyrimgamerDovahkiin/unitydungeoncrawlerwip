using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.InputSystem;

public class SimpleGrabSystem : MonoBehaviour
{
    public Transform mainSlot;
    [SerializeField] Transform weaponSide;
    [SerializeField] Transform weaponBack;
    [SerializeField] Transform secondSlot;
    //[SerializeField] BlockingCollider blockingCollider;
    [HideInInspector] public GameObject MainHand;
    [HideInInspector] public GameObject SecondHand;
    public bool isPickedMainSlot { get; private set; } = false;
    public bool isPickedMainSlotSpell { get; private set; } = false;
    public bool isPickedSecondSlot { get; private set; } = false;
    public bool isEquippedOneHanded { get; private set; } = false;
    public bool isEquippedSpellOneHanded { get; private set; } = false;
    public bool isEquippedSecondHand { get; private set; } = false;
    [HideInInspector] public bool isSheathedWeaponSide = true;
    [HideInInspector] public bool isSheathedSpellSide = true;
    [HideInInspector] public bool isSheathedWeaponBack = true;

    //Sword EquipOneHanded
    public void EquipOneHanded(GameObject item)
    {
        MainHand = Instantiate(item, weaponSide.transform.position, Quaternion.Euler(0f, 0f, -40f));
        MainHand.transform.SetParent(weaponSide);
        MainHand.transform.localRotation = Quaternion.Euler(0f, -90f, 230f);
        isEquippedOneHanded = true;
    }

    //Sword UnequipOneHanded
    public void UnequipOneHanded()
    {
        if (MainHand != null)
        {
            Destroy(MainHand);
        }
        isEquippedOneHanded = false;
        isEquippedSpellOneHanded = false;
    }

    public void EquipSpell(GameObject item)
    {
        MainHand = Instantiate(item, weaponSide.transform.position, Quaternion.identity);
        MainHand.transform.SetParent(weaponSide);
        MainHand.SetActive(false);
        //TODO: Wenn Spell 2H, dann bools für beide hände auf true
        isEquippedSpellOneHanded = true;
    }

    public void EquipSecondHand(GameObject item)
    {
        //TODO: umbenennen in equip shield
        SecondHand = Instantiate(item, weaponBack.transform.position, weaponBack.transform.rotation);
        //blockingCollider.SetColliderDamageAbsorption(SecondHand.GetComponent<ShieldData>().shield);
        SecondHand.transform.SetParent(weaponBack);
        SecondHand.transform.localRotation = Quaternion.Euler(-90f, 0f, -180f);
        isEquippedSecondHand = true;
    }

    public void UnequipSecondHand()
    {
        Destroy(SecondHand);
        isEquippedSecondHand = false;
    }


    //Draw and Sheathe onehanded Weapons
    public void DrawWeapon(GameObject item)
    {
        isPickedMainSlot = true;
        item.transform.SetParent(mainSlot);
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = Quaternion.Euler(90f, 0f, -90f);
        isSheathedWeaponSide = false;
    }

    public void SheatheWeapon(GameObject item)
    {
        isPickedMainSlot = false;
        item.transform.SetParent(weaponSide);
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = Quaternion.Euler(0f, -90f, 230f);
        isSheathedWeaponSide = true;
    }

    //Draw and Sheathe Shields
    public void DrawShield(GameObject item)
    {
        isPickedSecondSlot = true;
        item.transform.SetParent(secondSlot);
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = Quaternion.Euler(0f, -30f, -60f);
        isSheathedWeaponBack = false;
    }

    public void SheatheShield(GameObject item)
    {
        isPickedSecondSlot = false;
        item.transform.SetParent(weaponBack);
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = Quaternion.Euler(-90f, 0f, -180f);
        isSheathedWeaponBack = true;
    }

    //Draw and Sheathe Spells
    public void DrawSpell(GameObject item)
    {
        isPickedMainSlotSpell = true;
        item.transform.SetParent(mainSlot);
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = Quaternion.identity;
        MainHand.SetActive(true);
        isSheathedSpellSide = false;
    }

    public void SheatheSpell(GameObject item)
    {
        isPickedMainSlotSpell = false;
        item.transform.SetParent(weaponSide);
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = Quaternion.identity;
        MainHand.SetActive(false);
        isSheathedSpellSide = true;
    }

    //draw and sheathe functions for the animation so it looks correct.
    void Draw1HWeapon()
    {
        DrawWeapon(MainHand);
    }
    void Sheathe1HWeapon()
    {
        SheatheWeapon(MainHand);
    }
}