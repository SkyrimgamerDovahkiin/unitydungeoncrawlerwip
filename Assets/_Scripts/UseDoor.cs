using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class UseDoor : MonoBehaviour
{
    // [SerializeField] TextMeshProUGUI useLeverText;
    // [SerializeField] InputActionAsset inputActions;
    // InputAction UseDoorAction;
    // Camera cam;
    // GameObject player;
    // Linking link;

    // void Start()
    // {
    //     cam = Camera.main;
    //     player = this.gameObject;
    // }

    // void Update()
    // {
    //     useLeverText.enabled = false;
    //     RaycastHit hitInfo;

    //     //legt eine LayerMask fest, invertiert diese und bewegt den Player auf einen Layer, damit dieser vom Raycast ignoriert wird.
    //     player.layer = 8;
    //     var layerMask = 1 << 8;
    //     layerMask = ~layerMask;

    //     //findet das "Linking" Script auf dem anvisierten Objekt und spielt die Animation für Tür und Hebel ab.
    //     //Die Begrenzung der EulerAngles bei der CloseAnim ist notwendig, da Unity den Hebel nicht auf exakt 40 Grad rotiert
    //     if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hitInfo, 10f, layerMask) && hitInfo.transform.tag == "Lever")
    //     {
    //         link = hitInfo.transform.GetComponent<Linking>();
    //         if (UseDoorAction.triggered)
    //         {
    //             if (hitInfo.transform.GetChild(0).localRotation.eulerAngles.y > 39f && hitInfo.transform.GetChild(0).localRotation.eulerAngles.y < 41f)
    //             {
    //                 hitInfo.transform.GetChild(0).GetComponent<Animation>().Play("LeverCloseAnim");
    //                 link.secondLever.transform.GetChild(0).GetComponent<Animation>().Play("LeverCloseAnim");
    //                 link.door.GetComponent<Animation>().Play("GitterCloseAnim");
    //             }
    //             else if (hitInfo.transform.GetChild(0).localRotation.eulerAngles.y == 320f)
    //             {
    //                 hitInfo.transform.GetChild(0).GetComponent<Animation>().Play("LeverOpenAnim");
    //                 link.secondLever.transform.GetChild(0).GetComponent<Animation>().Play("LeverOpenAnim");
    //                 link.door.GetComponent<Animation>().Play("GitterOpenAnim");
    //             }
    //         }

    //         //Während der Hebel anvisiert wird, erscheint der Text zum benutzen
    //         while (hitInfo.collider.isTrigger)
    //         {
    //             useLeverText.enabled = true;
    //             var bindingIndex = UseDoorAction.GetBindingIndex(InputBinding.MaskByGroup("Keyboard&Mouse"));
    //             useLeverText.text = "Drücke " + UseDoorAction.GetBindingDisplayString(bindingIndex) + " um den Hebel zu benutzen";

    //             return;
    //         }

    //         useLeverText.enabled = false;
    //     }
    // }
}
