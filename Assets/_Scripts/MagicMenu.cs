using UnityEngine;
using UnityEngine.InputSystem;
using Ilumisoft.VisualStateMachine;

public class MagicMenu : MonoBehaviour
{
    [SerializeField] StateMachine stateMachine = null;
    [SerializeField] InputActionAsset inputActions;
    [SerializeField] GameObject magicMenuItemDetails;
    InputAction MagicMenuAction;
    CanvasRenderer magicMenuItemDetailsRend;
    bool magicMenuKeyPressed = false;
    //public GameObject magicMenuItemList;
    [SerializeField] Transform magicMenuInvMarker;

    void Start()
    {
        magicMenuItemDetailsRend = magicMenuItemDetails.GetComponent<CanvasRenderer>();
        var gameplayActionMap = inputActions.FindActionMap("UI");
        MagicMenuAction = gameplayActionMap.FindAction("MagicMenu");
        MagicMenuAction.Enable();
    }

    void Update()
    {

        if (MagicMenuAction.triggered && !magicMenuKeyPressed)
        {
            magicMenuKeyPressed = true;
            stateMachine.Trigger("ShowMagicMenu");
        }
        else if (MagicMenuAction.triggered && magicMenuKeyPressed)
        {
            magicMenuKeyPressed = false;
            stateMachine.Trigger("ExitMagicMenu");
            foreach (Transform child in magicMenuInvMarker)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
