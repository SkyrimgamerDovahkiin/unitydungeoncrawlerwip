using UnityEngine;
using UnityEngine.InputSystem;
using Ilumisoft.VisualStateMachine;

public class Inventory : MonoBehaviour
{
    [SerializeField] MainMenu menu;
    [SerializeField] StateMachine stateMachine = null;
    [SerializeField] InputActionAsset inputActions;
    [SerializeField] GameObject inventoryItemList;
    [SerializeField] GameObject itemDetails;
    [SerializeField] CanvasRenderer itemDetailsRend;
    InputAction InventoryAction;
    public bool invKeyPressed = false;
    //public GameObject invMarker;
    RectTransform scrollbarRect;
    RectTransform invItemListBorderRect;
    RectTransform viewportRect;
    Vector2 scrollbarRectOrigSize;
    Vector3 scrollbarRectOrigPos;
    Vector2 scrollbarRectNewSize;
    Vector3 scrollbarRectNewPos;
    Vector2 borderRectOrigSize;
    Vector3 borderRectOrigPos;
    Vector2 borderRectNewSize;
    Vector3 borderRectNewPos;
    Vector2 viewportRectOrigSize;
    Vector3 viewportRectOrigPos;
    Vector2 viewportRectNewSize;
    Vector3 viewportRectNewPos;

    void Start()
    {
        itemDetailsRend = itemDetails.GetComponent<CanvasRenderer>();
        var gameplayActionMap = inputActions.FindActionMap("UI");
        InventoryAction = gameplayActionMap.FindAction("Inventory");
        scrollbarRect = inventoryItemList.transform.Find("Scrollbar").GetComponent<RectTransform>();
        invItemListBorderRect = inventoryItemList.transform.Find("BorderInvItemList").GetComponent<RectTransform>();
        viewportRect = inventoryItemList.transform.Find("Viewport").GetComponent<RectTransform>();
        InventoryAction.Enable();
    }

    void Update()
    {
        if (InventoryAction.triggered && !invKeyPressed)
        {
            invKeyPressed = true;
            stateMachine.Trigger("ShowInventory");
        }
        else if (InventoryAction.triggered && invKeyPressed)
        {
            invKeyPressed = false;
            stateMachine.Trigger("ExitInventory");
        }
    }

    public void InventoryChestOnStateEnter()
    {
        scrollbarRectOrigSize = scrollbarRect.sizeDelta;
        scrollbarRectOrigPos = scrollbarRect.transform.localPosition;
        scrollbarRectNewSize = new Vector2(scrollbarRect.sizeDelta.x, 727.693f);
        scrollbarRectNewPos = new Vector3(scrollbarRect.transform.localPosition.x, 58f, scrollbarRect.transform.localPosition.z);
        scrollbarRect.sizeDelta = scrollbarRectNewSize;
        scrollbarRect.transform.localPosition = scrollbarRectNewPos;

        borderRectOrigSize = invItemListBorderRect.sizeDelta;
        borderRectOrigPos = invItemListBorderRect.transform.localPosition;
        borderRectNewSize = new Vector2(invItemListBorderRect.sizeDelta.x, 751f);
        borderRectNewPos = new Vector3(invItemListBorderRect.transform.localPosition.x, 57.5f, invItemListBorderRect.transform.localPosition.z);
        invItemListBorderRect.sizeDelta = borderRectNewSize;
        invItemListBorderRect.transform.localPosition = borderRectNewPos;

        viewportRectOrigSize = viewportRect.sizeDelta;
        viewportRectOrigPos = viewportRect.transform.localPosition;
        viewportRectNewSize = new Vector2(viewportRect.sizeDelta.x, 752.26f);
        viewportRectNewPos = new Vector3(viewportRect.transform.localPosition.x, 58.17f, viewportRect.transform.localPosition.z);
        viewportRect.sizeDelta = viewportRectNewSize;
        viewportRect.transform.localPosition = viewportRectNewPos;
    }

    public void InventoryChestOnStateExit()
    {
        scrollbarRect.transform.localPosition = scrollbarRectOrigPos;
        scrollbarRect.sizeDelta = scrollbarRectOrigSize;

        invItemListBorderRect.transform.localPosition = borderRectOrigPos;
        invItemListBorderRect.sizeDelta = borderRectOrigSize;

        viewportRect.transform.localPosition = viewportRectOrigPos;
        viewportRect.sizeDelta = viewportRectOrigSize;
    }
}
