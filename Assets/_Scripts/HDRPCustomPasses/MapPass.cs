    using UnityEngine;
    using UnityEngine.Rendering;
    using UnityEngine.Rendering.RendererUtils;
    using UnityEngine.Rendering.HighDefinition;
    
    public class MapPass : CustomPass
    {
        [Header("Output")]
        public RenderTexture outputRenderTexture;
        protected override bool executeInSceneView => true;
        protected override void Execute(CustomPassContext ctx)
        {
            if(outputRenderTexture != null)
            {
                var rtHandle = RTHandles.Alloc(outputRenderTexture);
                var scale = rtHandle.rtHandleProperties.rtHandleScale;
                
                //var scale = new Vector2(outputRenderTexture.width * 10f, outputRenderTexture.height * 10f);
                ctx.cmd.Blit(ctx.cameraColorBuffer, outputRenderTexture, new Vector2(scale.x, scale.y), Vector2.zero, 0, 0);
            }
        }
        protected override void Cleanup()
        {
            base.Cleanup();
        }
    }