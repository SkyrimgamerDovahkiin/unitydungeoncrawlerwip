using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShortSwordSounds : MonoBehaviour
{
    [SerializeField] AudioClip[] swordHitMetal;
    [SerializeField] AudioSource source;

    
    //source returnen!!!
    // void Update()
    // {
    //     if (source != null && !source.isPlaying)
    //     {
    //         AudioManager.ReturnSourceToPool(source);
    //     }
    // }
    void Start() 
    {
        source = GetComponent<AudioSource>();    
    }

    void OnCollisionEnter(Collision collision)
    {
        //funktioniert nur, wenn attack anim passiert ist!
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            Debug.Log(" I've just hit an enemy!");
            //source = AudioManager.PlayClipAtPoint(swordHitMetal[Random.Range(0, swordHitMetal.Length)], transform.position);
            source.PlayOneShot(swordHitMetal[Random.Range(0, swordHitMetal.Length)]);
        }
    }
}
