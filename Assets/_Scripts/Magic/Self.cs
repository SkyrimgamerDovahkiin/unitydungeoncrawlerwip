using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Self : MonoBehaviour
{
    [SerializeField] Spell spell;
    TextMeshProUGUI timerText;
    int sec = 0;
    float timer;

    void Start()
    {
        timer = spell.time;
        timerText = GameObject.Find("TimerText").GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timerText.text = "";
            Destroy(this.gameObject);
        }
        else if (timer > 0)
        {
            sec = Mathf.FloorToInt(timer % 60);
            if (sec >= 10)
            {
                timerText.text = spell.label + ": " + sec.ToString("00");
            }
            else if (sec < 10)
            {
                timerText.text = spell.label + ": " + sec.ToString("0");
            }
        }
    }
}
