using UnityEngine;

public class ProjectileCheckCollision : MonoBehaviour
{
    public LayerMask layerMask;
    Vector3 lastPos;
    public DestructionSpell projectile;

    void Update()
    {
        RaycastHit hit;
        Vector3 dir = lastPos - transform.position;

        if (lastPos != Vector3.zero)
        {
            if (Physics.Raycast(transform.position, dir, out hit, dir.magnitude, layerMask, QueryTriggerInteraction.Ignore))
            {
                Destroy(this.gameObject);
            }
            Debug.DrawRay(transform.position, dir, Color.red);
        }
        lastPos = transform.position;
    }
}
