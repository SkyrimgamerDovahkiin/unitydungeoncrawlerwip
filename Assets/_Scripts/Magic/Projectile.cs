using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float Speed = 30f;
    public float SpawnOffset = 0.1f;
    public float Lifetime = 2.5f;
    float timer;

    void Start()
    {

        RaycastHit hitInfo;

        //Create a ray, disable rigidbody gravity
        Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo);
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.useGravity = false;

        //Rotate the bullet towards the point where the ray hits a collider
        transform.LookAt(hitInfo.point);

        //It is better to add a force, not to modify velocity.
        rb.AddForce(transform.forward * Speed, ForceMode.VelocityChange);
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= Lifetime)
        {
            timer = 0f;
            Destroy(this.gameObject);
        }
    }
}
