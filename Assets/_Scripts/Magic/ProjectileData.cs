using UnityEngine;

public class ProjectileData : MonoBehaviour
{
    public AudioClip clip;
    public GameObject trail;
    public Spell spell;
}
