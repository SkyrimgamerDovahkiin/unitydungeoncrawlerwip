using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

public class HealthSystem : MonoBehaviour
{
    public float maxHealth = 100;
    public float maxStamina = 100;
    public float maxMana = 100;
    public float currentHealth;
    public float currentStamina;
    public float currentMana;
    [SerializeField] InputActionAsset inputActions;
    [SerializeField] float manaRegen = 1.5f;
    [SerializeField] Image healthbar;
    [SerializeField] Image staminabar;
    [SerializeField] Image manabar;
    [SerializeField] TextMeshProUGUI youLostText;
    [SerializeField] TextMeshProUGUI youWonText;
    [SerializeField] GameObject deathImage;
    InputActionMap gameplayActionMap;

    void Start()
    {
        gameplayActionMap = inputActions.FindActionMap("Player");
        healthbar = GameObject.Find("Healthbar").GetComponent<Image>();
        staminabar = GameObject.Find("Staminabar").GetComponent<Image>();
        manabar = GameObject.Find("Manabar").GetComponent<Image>();
    }
    void Update()
    {
        RestoreManaOverTime(manaRegen);

        if (currentHealth < 0f)
        {
            currentHealth = 0f;
        }
        else if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        if (currentHealth <= 0f)
        {
            youLostText.enabled = true;
            gameplayActionMap.Disable();
            deathImage.SetActive(true);
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
            Time.timeScale = 0f;
        }
        else if (currentHealth > 0f)
        {
            youLostText.enabled = false;
            gameplayActionMap.Enable();
            deathImage.SetActive(false);
        }

        if (currentStamina < 0f)
        {
            currentStamina = 0f;
        }
        else if (currentStamina > maxStamina)
        {
            currentStamina = maxStamina;
        }

        if (currentMana < 0f)
        {
            currentMana = 0f;
        }
        else if (currentMana > maxMana)
        {
            currentMana = maxMana;
        }
    }

    public void SetMaxHealth(float health)
    {
        healthbar.fillAmount = health / maxHealth;
    }

    public void SetHealth(float health)
    {
        healthbar.fillAmount = health / maxHealth;
    }

    public void SetMaxStamina(float stamina)
    {
        staminabar.fillAmount = stamina / maxStamina;
    }

    public void SetStamina(float stamina)
    {
        staminabar.fillAmount = stamina / maxStamina;
    }

    public void SetMaxMana(float mana)
    {
        manabar.fillAmount = mana / maxMana;
    }

    public void SetMana(float mana)
    {
        manabar.fillAmount = mana / maxMana;
    }

    public void TakeDamage(float damage)
    {
        if (damage > 0)
        {
            currentHealth -= damage;
        }
        SetHealth(currentHealth);
    }

    public void RemoveStamina(float staminaRem)
    {
        currentStamina -= staminaRem;
        SetStamina(currentStamina);
    }
    public void RemoveStaminaOverTime(float staminaRem)
    {
        currentStamina -= staminaRem * Time.deltaTime;
        SetStamina(currentStamina);
    }
    public void RemoveMana(float manaRem)
    {
        currentMana -= manaRem;
        SetMana(currentMana);
    }

    public void RestoreHealth(float health)
    {
        currentHealth += health;
        SetHealth(currentHealth);
    }

    public void RestoreStamina(float stamina)
    {
        currentStamina += stamina;
        SetStamina(currentStamina);
    }

    public void RestoreMana(float mana)
    {
        currentMana += mana;
        SetMana(currentMana);
    }

    public void RestoreStaminaOverTime(float stamina)
    {
        currentStamina += stamina * Time.deltaTime;
        SetStamina(currentStamina);
    }
    public void RestoreManaOverTime(float mana)
    {
        currentMana += mana * Time.deltaTime;
        SetMana(currentMana);
    }
}
