
public abstract class State
{
    public virtual void OnStateEnter()
    {
    }
    public virtual void OnStateExit()
    {
    }
    public virtual void OnUpdate()
    {
    }
}