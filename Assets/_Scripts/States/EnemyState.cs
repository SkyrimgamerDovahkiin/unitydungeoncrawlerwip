using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.AI;
using UnityEngine.UI;
using Unity.Mathematics;
using MathHelperFunctions;

public class EnemyState : State
{
    protected Enemy enemy;
    protected EnemyCheckCollision checkCollision;
    protected Rigidbody[] rigidbodies;
    protected Collider[] colliders;
    protected Joint[] joints;
    protected Animator animator;
    protected Transform playerTarget;
    protected WalkWaypoints walkWaypoints;
    protected NavMeshAgent agent;
    protected LayerMask playerMask;
    protected int playerLayer;
    protected float viewDistance;
    protected float followDistance;
    protected float rotTimer = 0f;
    protected float viewAngle;
    protected float awareDistance;
    protected float maxHealth;
    protected float currentHealth;
    protected float damage;
    protected float rotationSpeed;
    protected float maxSpeed = 5f;
    protected float rotAngle;
    protected bool interuptState;
    protected Vector3 playerToEnemy;
    protected Vector3 rotVector;
    protected Vector3 offset;
    protected Vector3 offsetVectorPlayerToEnemy;

    public EnemyState(Enemy enemy)
    {
        this.enemy = enemy;
        this.checkCollision = enemy.checkCollision;
        this.viewDistance = enemy.viewDistance;
        this.followDistance = enemy.followDistance;
        this.viewAngle = enemy.viewAngle;
        this.awareDistance = enemy.awareDistance;
        this.maxHealth = enemy.maxHealth;
        this.currentHealth = enemy.currentHealth;
        this.damage = enemy.damage;
        this.rotationSpeed = enemy.rotationSpeed;
        this.playerLayer = enemy.playerLayer;
        this.animator = enemy.animator;
        this.playerTarget = enemy.playerTarget;
        this.walkWaypoints = enemy.walkWaypoints;
        this.interuptState = enemy.interuptState;
        this.agent = enemy.agent;
        this.rigidbodies = enemy.rigidbodies;
        this.colliders = enemy.colliders;
        this.joints = enemy.joints;
        this.playerMask = enemy.playerMask;
    }
}

public class AttackState : EnemyState
{
    bool foundUI;
    Text debug;
    float timer = 0f;
    float staggerTime = 2f;
    bool staggerTimer = false;

    public AttackState(Enemy enemy) : base(enemy) { }


    public override void OnStateEnter()
    {
        //walkWaypoints.IsEnabled = false;
        agent.speed = 5f;
    }

    public override void OnStateExit()
    {
        Debug.Log("OnStateExit AttackState!");
    }

    public override void OnUpdate()
    {
        if (playerTarget == null)
        {
            enemy.SwitchState(enemy.wander);
        }

        if (agent.velocity.magnitude <= 0.001f)
        {
            animator.SetBool("canAttack", false);
            animator.SetBool("canWalk", false);
            animator.SetBool("canRun", false);
        }

        if (playerTarget != null)
        {
            //playerToEnemy = (enemy.transform.position - playerTarget.position).normalized;
            playerToEnemy = math.normalizesafe(enemy.transform.position - playerTarget.position);
            Vector3 enemyPos = new Vector3(enemy.transform.position.x, enemy.transform.position.y, enemy.transform.position.z);
            Vector3 playerPos = new Vector3(playerTarget.transform.position.x, playerTarget.transform.position.y, playerTarget.transform.position.z);
            offsetVectorPlayerToEnemy = new Vector3(0f, enemy.transform.position.y - enemy.playerTarget.position.y, 0f);
            float attackDistance = math.distance(enemyPos, playerPos);

            //if (Physics.Linecast(enemyPos, playerPos) && Vector3.Distance(enemyPos, playerPos) > viewDistance + 10f)
            //if (Physics.Linecast(enemyPos, playerPos) && attackDistance > viewDistance)
            if (Physics.Linecast(enemyPos, playerPos) && attackDistance > followDistance)
            {
                Debug.Log("playerNotFound!");
                enemy.SwitchState(enemy.wander);
            }

            //Rennen zum Player
            //if (Vector3.Distance(enemyPos, playerPos) > (awareDistance + 0.02f))
            if (attackDistance > awareDistance)
            {
                // TODO: delay für entscheidungen einbauen
                // TODO: richtig collision abfragen fürs hitten;
                offset = playerTarget.position + playerToEnemy * (awareDistance - 0.5f);
                agent.SetDestination(offset);

                if (agent.velocity.magnitude > 0.004f)
                {
                    animator.SetBool("canAttack", false);
                    animator.SetBool("canRun", true);
                    animator.SetBool("canWalk", false);
                }
            }

            //Attacken
            //else if (Vector3.Distance(enemyPos, playerPos) <= (awareDistance + 0.02f))
            else if (attackDistance <= awareDistance)
            {
                Debug.Log(" I can Attack!");

                if (animator.GetBool("canStagger"))
                {
                    staggerTimer = true;
                    animator.SetBool("canAttack", false);
                    animator.SetBool("isStaggered", true);
                }

                if (staggerTimer)
                {
                    timer += Time.deltaTime;
                    if (timer >= staggerTime)
                    {
                        timer = 0f;
                        staggerTimer = false;
                    }
                }

                if (timer <= 0f)
                {
                    animator.SetBool("canAttack", true);
                    animator.SetBool("isStaggered", false);
                }

                if (agent.velocity.magnitude > 0.004f)
                {
                    animator.SetBool("canRun", true);
                    animator.SetBool("canWalk", false);
                }
                else
                {
                    animator.SetBool("canRun", false);
                    animator.SetBool("canWalk", false);
                }
            }
        }
    }
}

public class Wander : EnemyState
{
    public enum WanderSubState
    {
        StartRotation, Rotating, Moving, Waiting
    }
    float yRot, yRotTarget;
    float waitTimer = 0f;
    Vector3 agentRot;
    WanderSubState subState = WanderSubState.StartRotation;
    bool foundUI;
    Text debug;
    //bool canMove = false;
    //bool canRotate = true;
    bool canMove;
    bool canRotate;

    public Wander(Enemy enemy) : base(enemy) { }

    public override void OnStateEnter()
    {
        canMove = false;
        canRotate = true;
        //walkWaypoints.IsEnabled = true;
        //subState = WanderSubState.StartRotation;
        //agent.SetDestination(walkWaypoints.curWaypoint);
    }

    public override void OnStateExit()
    {

    }

    public override void OnUpdate()
    {
        if (playerTarget != null)
        {
            //playerToEnemy = (enemy.transform.position - playerTarget.position).normalized;
            playerToEnemy = math.normalizesafe(enemy.transform.position - playerTarget.position);
            //Vector3 enemyPos = new Vector3(enemy.transform.position.x, enemy.transform.position.y, enemy.transform.position.z);
            //Vector3 playerPos = new Vector3(playerTarget.transform.position.x, playerTarget.transform.position.y, playerTarget.transform.position.z);
            float3 enemyPos = enemy.transform.position;
            float3 playerPos = playerTarget.transform.position;
            quaternion agentRot = agent.transform.rotation;
            offsetVectorPlayerToEnemy = new Vector3(0f, enemy.transform.position.y - enemy.playerTarget.position.y, 0f);

            float distance = math.distance(enemyPos, playerPos);
            var angle = Vector3.Angle(-enemy.transform.forward, playerToEnemy);

            //Wenn player im FOV, im view radius und nicht hinter einer collision, DANN player attacken
            // if ((Vector3.Angle(-enemy.transform.forward, playerTarget.position) < viewAngle / 2
            //     && Vector3.Distance(enemyPos, playerPos) <= viewDistance)
            //     && !Physics.Linecast(enemyPos, playerPos)
            //     || enemy.beenHit)

            if (angle < viewAngle / 2 && distance <= viewDistance
            && !Physics.Linecast(enemyPos, playerPos) || enemy.checkCollision.beenHit)
            {
                Debug.DrawLine(enemyPos, playerPos, Color.yellow, 3000000f);
                Debug.Log("playerFound!");
                enemy.SwitchState(enemy.attackState);
            }

            float3 ePos = agent.transform.position;
            float3 wPos = new float3(walkWaypoints.curWaypoint.x, agent.transform.position.y, walkWaypoints.curWaypoint.z);
            float waypointDistance = math.distance(ePos, wPos);

            float3 rotVector = (walkWaypoints.curWaypoint - (Vector3)agent.transform.position);
            if (canMove)
            {
                agent.speed = 4f;
                //if (Vector3.Distance(agent.destination, walkWaypoints.curWaypoint) > 1.0f)
                if (agent.remainingDistance < agent.stoppingDistance || agent.destination == Vector3.zero)
                {
                    agent.SetDestination(walkWaypoints.curWaypoint);
                }
                animator.SetBool("canWalk", true);
                animator.SetBool("canRun", false);
                animator.SetBool("canAttack", false);
            }

            if (canRotate)
            {
                quaternion targetRotation = quaternion.LookRotationSafe(rotVector, Vector3.up);
                quaternion smoothRotation = float3Extensions.RotateTowards(agentRot, targetRotation, rotationSpeed * Time.deltaTime);
                agent.transform.rotation = smoothRotation;

                float4 ownRotationFloats = new float4(agentRot.value.x, agentRot.value.y, agentRot.value.z, agentRot.value.w);
                float4 targetRotationFloats = new float4(targetRotation.value.x, targetRotation.value.y, targetRotation.value.z, targetRotation.value.w);

                if (math.distance(ownRotationFloats, targetRotationFloats) <= 0.2)
                {
                    canRotate = false;
                    canMove = true;
                }
            }

            //if (waypointDistance <= walkWaypoints.arriveDist)
            if (agent.remainingDistance <= walkWaypoints.arriveDist)
            {
                waitTimer += Time.deltaTime;
                animator.SetBool("canWalk", false);
                animator.SetBool("canRun", false);
                animator.SetBool("canAttack", false);
                if (waitTimer >= 2f)
                {
                    walkWaypoints.GetRandomWaypoint();
                    canMove = false;
                    canRotate = true;
                    waitTimer = 0f;
                }
            }
        }


        /*switch (subState)
        {
            //ROTATION START
            case WanderSubState.StartRotation:
                rotVector = (walkWaypoints.curWaypoint - agent.transform.position);
                Vector3 eulerRot = (Quaternion.LookRotation(rotVector, Vector3.up)).eulerAngles;
                yRotTarget = eulerRot.y;
                yRotTarget = Mathf.Abs(yRotTarget % 360);
                agentRot = agent.transform.eulerAngles;
                yRot = agentRot.y;
                subState = WanderSubState.Rotating;
                break;
            case WanderSubState.Rotating:
                rotTimer += Time.deltaTime;
                Quaternion agentQRot = Quaternion.Euler(Vector3.up * yRot);
                Quaternion targetQRot = Quaternion.Euler(Vector3.up * yRotTarget);
                Quaternion newQRot = Quaternion.Slerp(agentQRot, targetQRot, rotTimer);
                agent.transform.rotation = newQRot;
                if (rotTimer >= 0.5f)
                {
                    rotTimer = 0f;
                    agent.speed = 4f;
                    agent.SetDestination(walkWaypoints.curWaypoint);
                    animator.SetBool("canWalk", true);
                    animator.SetBool("canRun", false);
                    animator.SetBool("canAttack", false);
                    subState = WanderSubState.Moving;
                }
                break;
            //ROTATION END
            case WanderSubState.Moving:
                Vector3 ePos = new Vector3(enemy.transform.position.x, 0f, enemy.transform.position.z);
                Vector3 wPos = new Vector3(walkWaypoints.curWaypoint.x, 0f, walkWaypoints.curWaypoint.z);
                if (Vector3.Distance(ePos, wPos) < walkWaypoints.arriveDist)
                {
                    subState = WanderSubState.Waiting;
                }
                break;
            case WanderSubState.Waiting:
                waitTimer += Time.deltaTime;
                animator.SetBool("canWalk", false);
                animator.SetBool("canRun", false);
                animator.SetBool("canAttack", false);
                if (waitTimer >= 2f)
                {
                    walkWaypoints.GetRandomWaypoint();
                    subState = WanderSubState.StartRotation;
                    waitTimer = 0f;
                }
                break;
            default:
                break;
        }*/
    }
}

public class Dead : EnemyState
{
    public Dead(Enemy enemy) : base(enemy) { }

    public override void OnStateEnter()
    {
        animator.enabled = false;
        //walkWaypoints.IsEnabled = false;
        agent.isStopped = true;
        enemy.EnableRagdoll();
    }

    public override void OnStateExit() { }

    public override void OnUpdate() { }
}