using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    [SerializeField] AudioMixer mainVolumeMixer;
    [SerializeField] AnimationCurve backgroundMusicVolumeCurve;
    [SerializeField] AnimationCurve mainMenuMusicVolumeCurve;
    [SerializeField] AnimationCurve volumeCurve;
    [SerializeField] Slider volumeMainMenuSlider;
    [SerializeField] Slider volumeBackgroundMusicSlider;
    [SerializeField] Slider volumeSfxSlider;
    [SerializeField] AudioSource sfxPrefab;
    float volumeMainMenuSliderVolume;
    float volumeBackgroundMusicSliderVolume;
    float volumeSfxSliderVolume;
    bool muted = false;
    static AudioMixerGroup sfxMixerGroup;
    static Stack<AudioSource> sfxPool = new Stack<AudioSource>(20);

    void Start()
    {
        sfxMixerGroup = sfxPrefab.outputAudioMixerGroup;
        for (int i = 0; i < 20; i++)
        {
            AudioSource source = Instantiate<AudioSource>(sfxPrefab, transform);
            source.gameObject.SetActive(false);
            sfxPool.Push(source);
        }
    }


    public void SetMainMenuMusicVolume(float mainMenuMusicVolume)
    {
        float mainMenuVolume = mainMenuMusicVolumeCurve.Evaluate(mainMenuMusicVolume);
        mainVolumeMixer.SetFloat("MainMenuMusicVolume", mainMenuVolume);
    }

    public void SetBackgroundMusicVolume(float backgroundMusicVolume)
    {
        float backgroundVolume = backgroundMusicVolumeCurve.Evaluate(backgroundMusicVolume);
        mainVolumeMixer.SetFloat("BackgroundMusicVolume", backgroundVolume);
    }

    public void SetSFXVol(float sfxMusicVolume)
    {
        float sfxVolume = backgroundMusicVolumeCurve.Evaluate(sfxMusicVolume);
        mainVolumeMixer.SetFloat("SFXVolume", sfxVolume);
    }

    public void MuteMainMenuMusic()
    {
        if (!muted)
        {
            mainVolumeMixer.SetFloat("MainMenuMusicVolume", -80f);
            volumeMainMenuSliderVolume = volumeMainMenuSlider.value;
            volumeMainMenuSlider.value = -80f;
            muted = true;
        }
        else if (muted)
        {
            mainVolumeMixer.SetFloat("MainMenuMusicVolume", volumeMainMenuSliderVolume);
            volumeMainMenuSlider.value = volumeMainMenuSliderVolume;
            muted = false;
        }
    }

    public void MuteBGMusic()
    {
        if (!muted)
        {
            mainVolumeMixer.SetFloat("BackgroundMusicVolume", -80f);
            volumeBackgroundMusicSliderVolume = volumeBackgroundMusicSlider.value;
            volumeBackgroundMusicSlider.value = -80f;
            muted = true;
        }
        else if (muted)
        {
            mainVolumeMixer.SetFloat("BackgroundMusicVolume", volumeBackgroundMusicSliderVolume);
            volumeBackgroundMusicSlider.value = volumeBackgroundMusicSliderVolume;
            muted = false;
        }
    }

    public void MuteSFX()
    {
        if (!muted)
        {
            mainVolumeMixer.SetFloat("SFXVolume", -80f);
            volumeSfxSliderVolume = volumeSfxSlider.value;
            volumeSfxSlider.value = -80f;
            muted = true;
        }
        else if (muted)
        {
            mainVolumeMixer.SetFloat("SFXVolume", volumeSfxSliderVolume);
            volumeSfxSlider.value = volumeSfxSliderVolume;
            muted = false;
        }
    }

    public void FillStack()
    {
        if (sfxPool.Count == 0)
        {
            foreach (Transform item in transform)
            {
                AudioSource source = item.GetComponent<AudioSource>();
                item.gameObject.SetActive(false);
                sfxPool.Push(source);
            }
        }
    }

    public static AudioSource PlayClipAtPoint(AudioClip clip, Vector3 position)
    {
        AudioSource source = sfxPool.Pop();
        source.transform.position = position;
        source.clip = clip;
        source.gameObject.SetActive(true);
        source.Play();
        return source;
    }

    public static AudioSource PlayClipAtPointWithoutPool(AudioClip clip, Vector3 position)
    {
        GameObject tempGO = new GameObject("TempAudio"); // create the temp object
        tempGO.transform.position = position; // set its position
        AudioSource aSource = tempGO.AddComponent<AudioSource>(); // add an audio source
        aSource.clip = clip; // define the clip
        aSource.outputAudioMixerGroup = sfxMixerGroup;
        // set other aSource properties here, if desired
        aSource.Play(); // start the sound
        Destroy(tempGO, clip.length); // destroy object after clip duration
        return aSource; // return the AudioSource reference
    }

    public static void ReturnSourceToPool(AudioSource source)
    {
        source.gameObject.SetActive(false);
        sfxPool.Push(source);
    }
}
