using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//needed so things like the player, camera, etc. can transition into next scene
public class DontDestroyObjects : MonoBehaviour
{
    string mainMenuSceneName = "MainMenuScene";

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        //if in main menu scene, destroy the game object because it isn't needed anymore
        if (SceneManager.GetActiveScene().name == mainMenuSceneName)
        {
            Destroy(gameObject);
        }
    }
}
