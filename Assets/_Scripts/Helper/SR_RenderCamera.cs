using System.IO;
using UnityEngine;
using UnityEngine.InputSystem;

//used for capturing an image in game that can be used as the main menu background
public class SR_RenderCamera : MonoBehaviour
{

    public int FileCounter = 0;
    public InputActionAsset inputActions;
    InputAction CamShotAction;

    void Awake()
    {
        var gameplayActionMap = inputActions.FindActionMap("Player");
        CamShotAction = gameplayActionMap.FindAction("CamShot");
    }

    private void LateUpdate()
    {
        if (CamShotAction.triggered)
        {
            CamCapture();
        }
    }

    void CamCapture()
    {
        Camera Cam = GetComponent<Camera>();

        RenderTexture currentRT = RenderTexture.active;
        RenderTexture.active = Cam.targetTexture;

        Cam.Render();

        Texture2D Image = new Texture2D(Cam.targetTexture.width, Cam.targetTexture.height);
        Image.ReadPixels(new Rect(0, 0, Cam.targetTexture.width, Cam.targetTexture.height), 0, 0);
        Image.Apply();
        RenderTexture.active = currentRT;

        var Bytes = Image.EncodeToPNG();
        Destroy(Image);

        File.WriteAllBytes(Application.dataPath + "/Backgrounds/" + FileCounter + ".png", Bytes);
        FileCounter++;
    }

}

