using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//class for spawning a random dirt pile
#if (UNITY_EDITOR)
public class SpawnDirtPile : MonoBehaviour
{
    static DirtPileData dirtPileData;

    [MenuItem("Tools/Spawn DirtPile")]
    static void SpawnPrefab()
    {
        dirtPileData = Selection.activeGameObject.GetComponent<DirtPileData>();
        for (int i = 0; i < dirtPileData.spawnableItems; i++)
        {
            Vector2 randomCirclePos = new Vector2(Random.Range(dirtPileData.spawnArea.center.y, dirtPileData.spawnArea.radius), Random.Range(dirtPileData.spawnArea.center.y, dirtPileData.spawnArea.radius));
            GameObject prefab = dirtPileData.spawnablePrefabs[Random.Range(0, dirtPileData.spawnablePrefabs.Count)];
            GameObject result = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
            result.transform.SetParent(dirtPileData.gameObject.transform);
            if (prefab.name.Contains("Pile"))
            {
                result.transform.localPosition = new Vector3(randomCirclePos.x, Random.Range(-0.2f, dirtPileData.maxYValue), randomCirclePos.y);
                float scale = Random.Range(dirtPileData.minScaleDirtPile, dirtPileData.maxScaleDirtPile);
                result.transform.localScale = new Vector3(scale, scale, scale);
                result.transform.localEulerAngles = new Vector3(0f, Random.Range(0f, 360f), 0f);
            }
            else if (prefab.name.Contains("Brick"))
            {
                result.transform.localPosition = new Vector3(randomCirclePos.x, Random.Range(0f, dirtPileData.maxYValue), randomCirclePos.y);
                float scale = Random.Range(dirtPileData.minScaleBrick, dirtPileData.maxScaleBrick);
                result.transform.localScale = new Vector3(scale, scale, scale);
                result.transform.localEulerAngles = new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f));
            }
            else if (prefab.name.Contains("Head"))
            {
                result.transform.localPosition = new Vector3(randomCirclePos.x, Random.Range(0f, dirtPileData.maxYValue), randomCirclePos.y);
                float scale = Random.Range(dirtPileData.minScaleSkull, dirtPileData.maxScaleSkull);
                result.transform.localScale = new Vector3(scale, scale, scale);
                result.transform.localEulerAngles = new Vector3(Random.Range(-20f, 20f), Random.Range(0f, 360f), Random.Range(-10f, 10f));
            }
        }
    }
}
#endif
