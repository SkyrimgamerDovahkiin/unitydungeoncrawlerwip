    using UnityEngine;
    using TMPro;

    public class FPSCounter : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI textMeshPro;
        [SerializeField] private float _hudRefreshRate = 1f;
        private float timer;
        private void Update()
        {
            if (Time.unscaledTime > timer)
            {
                int fps = (int)(1f / Time.unscaledDeltaTime);
                textMeshPro.text = "FPS: " + fps;
                timer = Time.unscaledTime + _hudRefreshRate;
            }
        }
    }
