using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Data for the dirt Piles so that they can be randomly instanciated
public class DirtPileData : MonoBehaviour
{
    public List<GameObject> spawnablePrefabs = new List<GameObject>();
    public int spawnableItems = 6;
    public float maxYValue = 0.5f;
    public float minScaleDirtPile = 1.0f;
    public float maxScaleDirtPile = 2.0f;
    public float minScaleBrick = 0.3f;
    public float maxScaleBrick = 1f;
    public float minScaleSkull = 0.3f;
    public float maxScaleSkull = 0.7f;
    public SphereCollider spawnArea;
}
