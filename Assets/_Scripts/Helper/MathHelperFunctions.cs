using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

namespace MathHelperFunctions
{
    public struct GetAngleHelper
    {
        public static float GetAngle(float3 a, float3 b)
        {
            return math.degrees(GetRad(a, b));
        }

        public static float GetRad(float3 a, float3 b)
        {
            return math.acos(math.clamp(math.dot(a, b) / (math.length(a) * math.length(b)), -1f, 1f));
        }
    }

    public static class float3Extensions
    {
        public static float3 MoveTowards(float3 current, float3 target, float maxDistanceDelta)
        {
            float deltaX = target.x - current.x;
            float deltaY = target.y - current.y;
            float deltaZ = target.z - current.z;
            float sqdist = deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ;
            if (sqdist == 0 || sqdist <= maxDistanceDelta * maxDistanceDelta)
                return target;
            var dist = (float)math.sqrt(sqdist);
            return new float3(current.x + deltaX / dist * maxDistanceDelta,
                current.y + deltaY / dist * maxDistanceDelta,
                current.z + deltaZ / dist * maxDistanceDelta);
        }

        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static quaternion RotateTowards(
            quaternion from,
            quaternion to,
            float maxDegreesDelta)
        {
            float num = Angle(from, to);
            return num < float.Epsilon ? to : math.slerp(from, to, math.min(1f, maxDegreesDelta / num));
        }

        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Angle(this quaternion q1, quaternion q2)
        {
            var dot = math.dot(q1, q2);
            return !(dot > 0.999998986721039) ? (float)(math.acos(math.min(math.abs(dot), 1f)) * 2.0) : 0.0f;
        }

    }
}