using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class ThirdPersonController : MonoBehaviour
{
    [SerializeField] InputActionAsset inputActions;
    InputAction MoveAction;
    InputAction RotateAction;
    InputAction JumpAction;
    InputAction SprintAction;
    InputAction FireAction;
    InputAction SwordAttackAction;
    InputAction BlockAction;
    InputAction DrawAction;
    [SerializeField] GameObject camThirdPerson;
    [SerializeField] SimpleGrabSystem sgs;
    [SerializeField] HealthSystem healthSystem;
    [SerializeField] Transform playerHeadBone;
    [SerializeField] Transform SpawnEmpty;
    [SerializeField] Transform LightHolder;
    [SerializeField] AudioSource sfxSource;
    [SerializeField] AudioClip swordSlash;
    [SerializeField] AudioClip swordDraw;
    [SerializeField] AudioClip swordSheathe;
    [SerializeField] float MoveSpeed;
    [SerializeField] float vSpeed;
    [SerializeField] float jumpSpeed;
    [SerializeField] float MoveSpeedNormal = 3f;
    [SerializeField] float MoveSpeedSprint = 6f;
    [SerializeField] float gravity = 9.81f;
    [SerializeField] float friction;
    [SerializeField] float JumpHeight = 2f;
    [SerializeField] float FireRate = 1f;
    [SerializeField] float staminaDrainRun = 5f;
    [SerializeField] float staminaRegen = 2.5f;
    [SerializeField] float playerMaxFsDelayWalk = 0.6f;
    [SerializeField] float playerMaxFsDelayRun = 0.3f;
    [SerializeField] float pushPower = 2.0f;
    [SerializeField] Vector3 velocity;
    Animator playerAnimator;
    GameObject player;
    CharacterController cc;
    Transform camTransformThirdPerson;
    GameObject trail;
    GameObject magicLight;
    ProjectileData projectileData;
    PlayerSounds playerSounds;
    InputActionMap gameplayActionMap;
    bool isGrounded;
    bool isAttacking;
    bool drawWeapon;
    bool sheatheWeapon;
    float groundedTimer;
    public float xRot;
    float yRot;
    float nextFire;

    void Awake()
    {
        gameplayActionMap = inputActions.FindActionMap("Player");
        MoveAction = gameplayActionMap.FindAction("Move");
        JumpAction = gameplayActionMap.FindAction("Jump");
        RotateAction = gameplayActionMap.FindAction("Look");
        SprintAction = gameplayActionMap.FindAction("Sprint");
        FireAction = gameplayActionMap.FindAction("Fire");
        SwordAttackAction = gameplayActionMap.FindAction("SwordAttack");
        BlockAction = gameplayActionMap.FindAction("Block");
        DrawAction = gameplayActionMap.FindAction("Draw");
        playerSounds = GetComponent<PlayerSounds>();
        playerAnimator = gameObject.GetComponent<Animator>();
        cc = GetComponent<CharacterController>();
        camTransformThirdPerson = camThirdPerson.transform;
        MoveSpeed = MoveSpeedNormal;
        player = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != 1) { return; }

        CheckEquipStatus();

        isGrounded = cc.isGrounded;

        //Draw Weapon
        DrawAction.started += WeaponDrawAndSheathe;

        #region MagicBehaviour
        //Fire Magic
        FireAction.started += MagicReadying;
        FireAction.performed += MagicReady;
        FireAction.canceled += FireMagic;
        #endregion MagicBehaviour

        #region SwordBehaviour
        if (SwordAttackAction.triggered && !sgs.isSheathedWeaponSide)
        {
            OneHandWeaponAttack();
        }
        #endregion SwordBehaviour

        #region BlockBehaviour
        BlockAction.started += BlockReadying;
        BlockAction.performed += BlockReady;
        BlockAction.canceled += BlockStop;
        #endregion BlockBehaviour

        #region MovementBehaviour
        SprintAction.started += StartSprint;
        SprintAction.canceled += StopSprint;

        Move();

        if (healthSystem.currentStamina < healthSystem.maxStamina && MoveSpeed == MoveSpeedNormal)
        {
            healthSystem.RestoreStaminaOverTime(staminaRegen);
        }
        else if (healthSystem.currentStamina > 0f && MoveSpeed == MoveSpeedSprint)
        {
            healthSystem.RemoveStaminaOverTime(staminaDrainRun);
        }
        if (healthSystem.currentStamina <= 0f)
        {
            MoveSpeed = MoveSpeedNormal;
            playerSounds.maxFsDelay = playerMaxFsDelayWalk;
            playerAnimator.SetBool("isRunning", false);
        }

        if (velocity.magnitude < 0.004f)
        {
            playerAnimator.SetBool("isRunning", false);
            playerAnimator.SetBool("isWalking", false);
        }

        //GRAVITY
        Gravity();

        //JUMP
        if (JumpAction.triggered)
        {
            Jump();
        }

        //ROTATE CAMERA
        if (Time.timeScale != 0f)
        {
            Rotate();
        }
        #endregion MovementBehaviour
    }

    #region MovementFunctions
    // Sprint Functions
    void StartSprint(InputAction.CallbackContext context)
    {
        if (healthSystem.currentStamina > 0f && velocity.magnitude > 0.004f)
        {
            MoveSpeed = MoveSpeedSprint;
            playerSounds.maxFsDelay = playerMaxFsDelayRun;
            playerAnimator.SetBool("isRunning", true);
        }
    }

    void StopSprint(InputAction.CallbackContext context)
    {
        MoveSpeed = MoveSpeedNormal;
        playerSounds.maxFsDelay = playerMaxFsDelayWalk;
        playerAnimator.SetBool("isRunning", false);
    }

    void Rotate()
    {
        //player.transform.eulerAngles = new Vector3(0f, camTransformThirdPerson.rotation.eulerAngles.y, 0f);
        Vector2 test = RotateAction.ReadValue<Vector2>();
        xRot += test.x;
        player.transform.eulerAngles = new Vector3(0f, xRot, 0f);
    }

    void Move()
    {
        if (Time.timeScale == 0) { return; }
        Vector3 fwdDir = player.transform.forward;
        Vector3 sideDir = player.transform.right;
        Vector2 moveVector = MoveAction.ReadValue<Vector2>();
        Vector3 moveDir = fwdDir * moveVector.y + sideDir * moveVector.x;
        velocity = moveDir * MoveSpeed;
        velocity.y = vSpeed;
        cc.Move(velocity * Time.deltaTime);

        if (moveDir != Vector3.zero)
        {
            playerAnimator.SetBool("isWalking", true);
            playerSounds.UpdateFootsteps(velocity.magnitude);
        }
        else
        {
            playerAnimator.SetBool("isWalking", false);
        }
    }

    void Gravity()
    {
        if (isGrounded && vSpeed < 0)
        {
            vSpeed = 0f;
        }
        vSpeed -= gravity * Time.deltaTime;
    }

    void Jump()
    {
        if (isGrounded)
        {
            vSpeed += Mathf.Sqrt(JumpHeight * 2f * gravity);
        }
    }
    #endregion MovementFunctions

    #region Magic1HFunctions
    void FireMagic(InputAction.CallbackContext context)
    {
        if (Time.timeScale == 0f) { return; }
        if (!sgs.isSheathedSpellSide && sgs.isEquippedSpellOneHanded && Time.time > nextFire)
        {
            nextFire = Time.time + FireRate;
            if (healthSystem.currentMana > projectileData.spell.cost && projectileData.spell.SpellType == SpellType.Ranged)
            {
                healthSystem.RemoveMana(projectileData.spell.cost);
                trail = Instantiate(projectileData.trail, SpawnEmpty.position, Quaternion.Euler(0f, SpawnEmpty.rotation.eulerAngles.y, 0f));
                playerAnimator.SetBool("canFireSpell", true);
                playerAnimator.SetBool("spellReadying", false);
                playerAnimator.SetBool("spellReady", false);
                AudioManager.PlayClipAtPointWithoutPool(projectileData.clip, transform.position);
            }
            else if (healthSystem.currentMana > projectileData.spell.cost && projectileData.spell.SpellType == SpellType.Self)
            {
                healthSystem.RemoveMana(projectileData.spell.cost);
                if (projectileData.spell.label != "Light")
                {
                    trail = Instantiate(projectileData.trail, SpawnEmpty.position, Quaternion.Euler(0f, SpawnEmpty.rotation.eulerAngles.y, 0f));
                }
                else if (projectileData.spell.label == "Light")
                {
                    Destroy(magicLight);
                    magicLight = Instantiate(projectileData.trail, new Vector3(transform.position.x, transform.position.y + 4f, transform.position.z), Quaternion.identity);
                    magicLight.transform.SetParent(LightHolder);
                    magicLight.transform.localPosition = Vector3.zero;
                }
                //playerAnimator.SetBool("canFireSpell", true);
                //playerAnimator.SetBool("spellReadying", false);
                //playerAnimator.SetBool("spellReady", false);
                AudioManager.PlayClipAtPointWithoutPool(projectileData.clip, transform.position);
            }
        }
    }

    void MagicReadying(InputAction.CallbackContext context)
    {
        if (Time.timeScale == 0f) { return; }
        if (!sgs.isSheathedSpellSide && sgs.isEquippedSpellOneHanded)
        {
            GetProjectileData();
            if (healthSystem.currentMana > projectileData.spell.cost && projectileData.spell.SpellType == SpellType.Ranged)
            {
                playerAnimator.SetBool("canFireSpell", false);
                playerAnimator.SetBool("spellReadying", true);
            }
        }
    }

    void MagicReady(InputAction.CallbackContext context)
    {
        if (Time.timeScale == 0f) { return; }
        if (!sgs.isSheathedSpellSide && sgs.isEquippedSpellOneHanded)
        {
            if (healthSystem.currentMana > projectileData.spell.cost && projectileData.spell.SpellType == SpellType.Ranged)
            {
                playerAnimator.SetBool("spellReady", true);
            }
        }
    }
    #endregion Magic1HFunctions

    #region SwordFunctions
    void OneHandWeaponAttack()
    {
        WeaponData weaponData = sgs.MainHand.GetComponent<WeaponData>();
        WeaponItem weaponItem = weaponData.weaponItem;

        //set the animator trigger for doing a Onehand Attack
        playerAnimator.SetTrigger("1HAttack");

        //when the current stamina is too low, the player does not as much damage as normally
        if (healthSystem.currentStamina <= weaponItem.StaminaToRemove)
        {
            weaponItem.damage = weaponItem.damageLowStamina;
        }
    }
    #endregion SwordFunctions

    #region BlockFunctions
    void BlockReadying(InputAction.CallbackContext context)
    {
        if (Time.timeScale == 0f) { return; }
        if (!sgs.isSheathedWeaponBack && sgs.isEquippedSecondHand && Time.timeScale == 1)
        {
            playerAnimator.SetBool("canBlock", true);
            playerAnimator.SetBool("blockStopped", false);
            playerAnimator.SetBool("blockReadying", false);
        }
    }

    void BlockReady(InputAction.CallbackContext context)
    {
        if (Time.timeScale == 0f) { return; }
        if (!sgs.isSheathedWeaponBack && sgs.isEquippedSecondHand && Time.timeScale == 1)
        {
            playerAnimator.SetBool("blockReadying", true);
        }
    }

    void BlockStop(InputAction.CallbackContext context)
    {
        if (Time.timeScale == 0f) { return; }
        if (!sgs.isSheathedWeaponBack && sgs.isEquippedSecondHand && Time.timeScale == 1)
        {
            playerAnimator.SetBool("blockReadying", false);
            playerAnimator.SetBool("blockStopped", true);
            playerAnimator.SetBool("canBlock", false);
        }
    }
    #endregion BlockFunctions

    void WeaponDrawAndSheathe(InputAction.CallbackContext context)
    {
        if (Time.timeScale == 0f) { return; }
        //wird nur gemacht, wenn draw button gedrückt!
        if (sgs.isEquippedOneHanded)
        {
            if (sgs.isSheathedWeaponSide)
            {
                playerAnimator.SetBool("sheatheSword", false);
                playerAnimator.SetBool("drawSword", true);
                playerAnimator.SetBool("swordEquipped", true);
                playerAnimator.SetBool("spellEquipped", false);
            }
            else if (!sgs.isSheathedWeaponSide)
            {
                playerAnimator.SetBool("sheatheSword", true);
                playerAnimator.SetBool("drawSword", false);
                playerAnimator.SetBool("swordEquipped", false);
            }
        }
        else if (sgs.isEquippedSpellOneHanded)
        {
            if (sgs.isSheathedSpellSide)
            {
                playerAnimator.SetBool("spellEquipped", true);
                playerAnimator.SetBool("swordEquipped", false);
                sgs.DrawSpell(sgs.MainHand);
            }
            else if (!sgs.isSheathedSpellSide)
            {
                sgs.SheatheSpell(sgs.MainHand);
                playerAnimator.SetBool("spellEquipped", false);
            }
        }

        if (sgs.isEquippedSecondHand)
        {
            if (sgs.isSheathedWeaponBack)
            {
                //hier später anim bools einfügen, wenn anim vorhanden
                sgs.DrawShield(sgs.SecondHand);
            }
            else if (!sgs.isSheathedWeaponBack)
            {
                sgs.SheatheShield(sgs.SecondHand);
                //hier später anim bools einfügen, wenn anim vorhanden
            }
        }
    }

    void CheckEquipStatus()
    {
        if (sgs.isEquippedOneHanded)
        {
            if (!sgs.isSheathedWeaponSide)
            {
                playerAnimator.SetBool("swordEquipped", true);
                playerAnimator.SetBool("spellEquipped", false);
            }
            else if (sgs.isSheathedWeaponSide)
            {
                playerAnimator.SetBool("swordEquipped", false);
            }
        }
        else if (sgs.isEquippedSpellOneHanded)
        {
            if (!sgs.isSheathedSpellSide)
            {
                playerAnimator.SetBool("spellEquipped", true);
                playerAnimator.SetBool("swordEquipped", false);
            }
            else if (sgs.isSheathedSpellSide)
            {
                playerAnimator.SetBool("spellEquipped", false);
            }
        }
        else
        {
            playerAnimator.SetBool("drawSword", false);
            playerAnimator.SetBool("swordEquipped", false);
            playerAnimator.SetBool("spellEquipped", false);
        }
    }

    void GetProjectileData()
    {
        projectileData = sgs.mainSlot.transform.GetChild(0).GetComponent<ProjectileData>();
    }

    void OnEnable()
    {
        gameplayActionMap.Enable();
    }

    void OnDisable()
    {
        DrawAction.started -= WeaponDrawAndSheathe;
        FireAction.started -= MagicReadying;
        FireAction.performed -= MagicReady;
        FireAction.canceled -= FireMagic;
        BlockAction.started -= BlockReadying;
        BlockAction.performed -= BlockReady;
        BlockAction.canceled -= BlockStop;
        SprintAction.started -= StartSprint;
        SprintAction.canceled -= StopSprint;
        gameplayActionMap.Disable();
    }

    // void OnControllerColliderHit(ControllerColliderHit hit)
    // {
    //     Debug.Log("has hit something!");
    //     Rigidbody body = hit.collider.attachedRigidbody;
    //     Debug.Log("name: " + body + "is rigidbody!");

    //     if (body == null || body.isKinematic)
    //         return;

    //     Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

    //     //body.AddForce(pushDir * pushPower, ForceMode.Force);
    //     body.velocity = pushDir * pushPower;
    // }
}
